package io.ychescale9.flickrsearch;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.ychescale9.flickrsearch.data.api.ApiConstants;
import io.ychescale9.flickrsearch.presentation.helper.ImageLoader;
import io.ychescale9.flickrsearch.presentation.helper.PhotoUrlBuilder;
import io.ychescale9.flickrsearch.presentation.helper.PhotoUrlBuilderImpl;
import io.ychescale9.flickrsearch.util.Clock;
import io.ychescale9.flickrsearch.util.SchedulerProvider;
import io.ychescale9.flickrsearch.util.TestClock;
import io.ychescale9.flickrsearch.util.TestImageLoaderImpl;
import io.ychescale9.flickrsearch.util.UiTestSchedulerProvider;

/**
 * Created by yang on 8/4/17.
 */
@Module
public class TestAppModule {

    @Provides
    @Singleton
    SchedulerProvider provideSchedulerProvider() {
        return new UiTestSchedulerProvider();
    }

    @Provides
    @Singleton
    ImageLoader provideImageLoader() {
        return new TestImageLoaderImpl();
    }

    @Provides
    @Singleton
    PhotoUrlBuilder providePhotoUrlBuilder(ApiConstants apiConstants) {
        return new PhotoUrlBuilderImpl(
                apiConstants.getPhotoThumbnailUrlFormat(),
                apiConstants.getPhotoPageUrlFormat()
        );
    }

    @Provides
    @Singleton
    Clock provideClock() {
        return new TestClock();
    }
}