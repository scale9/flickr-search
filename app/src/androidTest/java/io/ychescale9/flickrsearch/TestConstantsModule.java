package io.ychescale9.flickrsearch;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.ychescale9.flickrsearch.data.api.ApiConstants;
import io.ychescale9.flickrsearch.data.api.TestApiConstants;
import io.ychescale9.flickrsearch.util.TestUtil;

/**
 * Created by yang on 6/4/17.
 */
@Module
public class TestConstantsModule {

    @Provides
    @Singleton
    ApiConstants provideApiConstants() {
        return new TestApiConstants(TestUtil.MOCK_SERVER_PORT);
    }
}