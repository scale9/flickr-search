package io.ychescale9.flickrsearch.util;

import android.os.AsyncTask;

import io.reactivex.Scheduler;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by yang on 6/4/17.
 * SchedulerProvider for android (UI) tests.
 */
public class UiTestSchedulerProvider extends SchedulerProvider {

    @Override
    public Scheduler io() {
        // override default Schedulers.io() with AsyncTask.THREAD_POOL_EXECUTOR
        // as Espresso by default waits for async tasks to complete
        return Schedulers.from(AsyncTask.THREAD_POOL_EXECUTOR);
    }
}
