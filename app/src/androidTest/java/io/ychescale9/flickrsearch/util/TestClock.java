package io.ychescale9.flickrsearch.util;

import org.joda.time.DateTime;

/**
 * Created by yang on 21/4/17.
 * A {@link Clock} implementation which supports overriding the return value of now() with a fixed value.
 * This can be used for timing-related tests.
 */
public class TestClock implements Clock {

    private long overriddenNow = -1;

    public void overrideNow(long overriddenNow) {
        this.overriddenNow = overriddenNow;
    }

    public void reset() {
        overriddenNow = -1;
    }

    @Override
    public long now() {
        // return real current time if overriddenCurrentTimeMillis < 0
        if (overriddenNow < 0) {
            return DateTime.now().getMillis();
        } else {
            return overriddenNow;
        }
    }
}
