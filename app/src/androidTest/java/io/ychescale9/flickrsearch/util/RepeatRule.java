package io.ychescale9.flickrsearch.util;

import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;

/**
 * Created by yang on 8/4/17.
 * TestRule to run tests multiple times.
 * Can be used to debug flaky tests.
 */
public final class RepeatRule implements TestRule {

    private final int iterations;

    public RepeatRule(int iterations) {
        if (iterations < 1) throw new IllegalArgumentException("iterations < 1: " + iterations);
        this.iterations = iterations;
    }

    @Override
    public Statement apply(final Statement base, Description description) {
        return new Statement() {
            @Override
            public void evaluate() throws Throwable {
                for (int i = 0; i < iterations; i++) {
                    base.evaluate();
                }
            }
        };
    }
}
