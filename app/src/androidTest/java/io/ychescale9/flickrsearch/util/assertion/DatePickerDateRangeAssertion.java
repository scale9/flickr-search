package io.ychescale9.flickrsearch.util.assertion;

import android.support.test.espresso.NoMatchingViewException;
import android.support.test.espresso.ViewAssertion;
import android.view.View;
import android.widget.DatePicker;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class DatePickerDateRangeAssertion implements ViewAssertion {

    private final long minDate;
    private final long maxDate;

    public static DatePickerDateRangeAssertion hasMinDate(long minDate) {
        return new DatePickerDateRangeAssertion(minDate, 0);
    }

    public static DatePickerDateRangeAssertion hasMaxDate(long maxDate) {
        return new DatePickerDateRangeAssertion(0, maxDate);
    }

    public DatePickerDateRangeAssertion(long minDate, long maxDate) {
        this.minDate = minDate;
        this.maxDate = maxDate;
    }

    @Override
    public void check(View view, NoMatchingViewException noViewFoundException) {
        if (noViewFoundException != null) {
            throw noViewFoundException;
        }

        DatePicker datePicker = (DatePicker) view;
        if (minDate > 0) {
            assertThat(datePicker.getMinDate(), is(minDate));
        }
        if (maxDate > 0) {
            assertThat(datePicker.getMaxDate(), is(maxDate));
        }
    }
}