package io.ychescale9.flickrsearch.util.matcher;

import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.TextView;

import org.hamcrest.Matcher;

import static android.support.test.espresso.matcher.ViewMatchers.withClassName;
import static android.support.test.espresso.matcher.ViewMatchers.withParent;
import static org.hamcrest.Matchers.anyOf;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.core.AllOf.allOf;
import static org.hamcrest.core.Is.is;

/**
 * Created by yang on 18/4/17.
 */
public final class CustomMatchers {

    /**
     * Returns a matcher that matches the navigation button in the v7.widget.Toolbar
     * @return
     */
    public static Matcher<View> withToolbarNavigationButton() {
        return allOf(
                withParent(withClassName(is(Toolbar.class.getName()))),
                withClassName(anyOf(
                        is(ImageButton.class.getName()),
                        is(AppCompatImageButton.class.getName())
                )));
    }

    /**
     * Returns a matcher that matches the title text view in the v7.widget.Toolbar
     * @return
     */
    public static Matcher<View> withToolbarTitleTextView() {
        return allOf(
                withParent(withClassName(is(Toolbar.class.getName()))),
                instanceOf(TextView.class));
    }

    /**
     * Returns a matcher that matches the currently visible DatePicker
     * @return
     */
    public static Matcher<View> withDatePicker() {
        return withClassName(equalTo(DatePicker.class.getName()));
    }
}
