package io.ychescale9.flickrsearch.util;

/**
 * Created by yang on 7/4/17.
 */
public class TestData {

    public static final String TEST_PHOTOS_RESPONSE_PAGE_1_JSON =
            "{\"photos\":{\"page\":1,\"pages\":2,\"perpage\":20,\"total\":\"30\",\"photo\":[" +
                    "{\"id\":\"1\",\"owner\":\"user1\",\"secret\":\"aaa\",\"server\":\"1000\",\"farm\":1,\"title\":\"Android Developer\",\"ispublic\":1,\"isfriend\":0,\"isfamily\":0}," +
                    "{\"id\":\"2\",\"owner\":\"user2\",\"secret\":\"aaa\",\"server\":\"1000\",\"farm\":1,\"title\":\"#android nougat\",\"ispublic\":1,\"isfriend\":0,\"isfamily\":0}," +
                    "{\"id\":\"3\",\"owner\":\"user3\",\"secret\":\"aaa\",\"server\":\"1000\",\"farm\":1,\"title\":\"#android O announced\",\"ispublic\":1,\"isfriend\":0,\"isfamily\":0}," +
                    "{\"id\":\"4\",\"owner\":\"user4\",\"secret\":\"aaa\",\"server\":\"1000\",\"farm\":1,\"title\":\"Mobile development\",\"ispublic\":1,\"isfriend\":0,\"isfamily\":0}," +
                    "{\"id\":\"5\",\"owner\":\"user5\",\"secret\":\"aaa\",\"server\":\"1000\",\"farm\":1,\"title\":\"Nexus\",\"ispublic\":1,\"isfriend\":0,\"isfamily\":0}," +
                    "{\"id\":\"6\",\"owner\":\"user6\",\"secret\":\"aaa\",\"server\":\"1000\",\"farm\":1,\"title\":\"Pixel\",\"ispublic\":1,\"isfriend\":0,\"isfamily\":0}," +
                    "{\"id\":\"7\",\"owner\":\"user7\",\"secret\":\"aaa\",\"server\":\"1000\",\"farm\":1,\"title\":\"#Android Studio\",\"ispublic\":1,\"isfriend\":0,\"isfamily\":0}," +
                    "{\"id\":\"8\",\"owner\":\"user8\",\"secret\":\"aaa\",\"server\":\"1000\",\"farm\":1,\"title\":\"material design\",\"ispublic\":1,\"isfriend\":0,\"isfamily\":0}," +
                    "{\"id\":\"9\",\"owner\":\"user9\",\"secret\":\"aaa\",\"server\":\"1000\",\"farm\":1,\"title\":\"#Android Auto\",\"ispublic\":1,\"isfriend\":0,\"isfamily\":0}," +
                    "{\"id\":\"10\",\"owner\":\"user10\",\"secret\":\"aaa\",\"server\":\"1000\",\"farm\":1,\"title\":\"#googleio #android\",\"ispublic\":1,\"isfriend\":0,\"isfamily\":0}," +
                    "{\"id\":\"11\",\"owner\":\"user11\",\"secret\":\"aaa\",\"server\":\"1000\",\"farm\":1,\"title\":\"title 11\",\"ispublic\":1,\"isfriend\":0,\"isfamily\":0}," +
                    "{\"id\":\"12\",\"owner\":\"user12\",\"secret\":\"aaa\",\"server\":\"1000\",\"farm\":1,\"title\":\"title 12\",\"ispublic\":1,\"isfriend\":0,\"isfamily\":0}," +
                    "{\"id\":\"13\",\"owner\":\"user13\",\"secret\":\"aaa\",\"server\":\"1000\",\"farm\":1,\"title\":\"title 13\",\"ispublic\":1,\"isfriend\":0,\"isfamily\":0}," +
                    "{\"id\":\"14\",\"owner\":\"user14\",\"secret\":\"aaa\",\"server\":\"1000\",\"farm\":1,\"title\":\"title 14\",\"ispublic\":1,\"isfriend\":0,\"isfamily\":0}," +
                    "{\"id\":\"15\",\"owner\":\"user15\",\"secret\":\"aaa\",\"server\":\"1000\",\"farm\":1,\"title\":\"title 15\",\"ispublic\":1,\"isfriend\":0,\"isfamily\":0}," +
                    "{\"id\":\"16\",\"owner\":\"user16\",\"secret\":\"aaa\",\"server\":\"1000\",\"farm\":1,\"title\":\"title 16\",\"ispublic\":1,\"isfriend\":0,\"isfamily\":0}," +
                    "{\"id\":\"17\",\"owner\":\"user17\",\"secret\":\"aaa\",\"server\":\"1000\",\"farm\":1,\"title\":\"title 17\",\"ispublic\":1,\"isfriend\":0,\"isfamily\":0}," +
                    "{\"id\":\"18\",\"owner\":\"user18\",\"secret\":\"aaa\",\"server\":\"1000\",\"farm\":1,\"title\":\"title 18\",\"ispublic\":1,\"isfriend\":0,\"isfamily\":0}," +
                    "{\"id\":\"19\",\"owner\":\"user19\",\"secret\":\"aaa\",\"server\":\"1000\",\"farm\":1,\"title\":\"title 19\",\"ispublic\":1,\"isfriend\":0,\"isfamily\":0}," +
                    "{\"id\":\"20\",\"owner\":\"user20\",\"secret\":\"aaa\",\"server\":\"1000\",\"farm\":1,\"title\":\"title 20\",\"ispublic\":1,\"isfriend\":0,\"isfamily\":0}" +
                    "]},\"stat\":\"ok\"}";

    public static final String TEST_PHOTOS_RESPONSE_PAGE_2_JSON =
            "{\"photos\":{\"page\":2,\"pages\":2,\"perpage\":20,\"total\":\"30\",\"photo\":[" +
                    "{\"id\":\"21\",\"owner\":\"user21\",\"secret\":\"aaa\",\"server\":\"1000\",\"farm\":1,\"title\":\"title 21\",\"ispublic\":1,\"isfriend\":0,\"isfamily\":0}," +
                    "{\"id\":\"22\",\"owner\":\"user22\",\"secret\":\"aaa\",\"server\":\"1000\",\"farm\":1,\"title\":\"title 22\",\"ispublic\":1,\"isfriend\":0,\"isfamily\":0}," +
                    "{\"id\":\"23\",\"owner\":\"user23\",\"secret\":\"aaa\",\"server\":\"1000\",\"farm\":1,\"title\":\"title 23\",\"ispublic\":1,\"isfriend\":0,\"isfamily\":0}," +
                    "{\"id\":\"24\",\"owner\":\"user24\",\"secret\":\"aaa\",\"server\":\"1000\",\"farm\":1,\"title\":\"title 24\",\"ispublic\":1,\"isfriend\":0,\"isfamily\":0}," +
                    "{\"id\":\"25\",\"owner\":\"user25\",\"secret\":\"aaa\",\"server\":\"1000\",\"farm\":1,\"title\":\"title 25\",\"ispublic\":1,\"isfriend\":0,\"isfamily\":0}," +
                    "{\"id\":\"26\",\"owner\":\"user26\",\"secret\":\"aaa\",\"server\":\"1000\",\"farm\":1,\"title\":\"title 26\",\"ispublic\":1,\"isfriend\":0,\"isfamily\":0}," +
                    "{\"id\":\"27\",\"owner\":\"user27\",\"secret\":\"aaa\",\"server\":\"1000\",\"farm\":1,\"title\":\"title 27\",\"ispublic\":1,\"isfriend\":0,\"isfamily\":0}," +
                    "{\"id\":\"28\",\"owner\":\"user28\",\"secret\":\"aaa\",\"server\":\"1000\",\"farm\":1,\"title\":\"title 28\",\"ispublic\":1,\"isfriend\":0,\"isfamily\":0}," +
                    "{\"id\":\"29\",\"owner\":\"user29\",\"secret\":\"aaa\",\"server\":\"1000\",\"farm\":1,\"title\":\"title 29\",\"ispublic\":1,\"isfriend\":0,\"isfamily\":0}," +
                    "{\"id\":\"30\",\"owner\":\"user30\",\"secret\":\"aaa\",\"server\":\"1000\",\"farm\":1,\"title\":\"title 30\",\"ispublic\":1,\"isfriend\":0,\"isfamily\":0}" +
                    "]},\"stat\":\"ok\"}";

    public static final String EMPTY_PHOTOS_RESPONSE_JSON = "{\"photos\":{\"page\":1,\"pages\":0,\"perpage\":20,\"total\":\"0\",\"photo\":[]},\"stat\":\"ok\"}";

}
