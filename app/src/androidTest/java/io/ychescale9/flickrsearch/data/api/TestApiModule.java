package io.ychescale9.flickrsearch.data.api;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.ychescale9.flickrsearch.data.deserializer.SearchPhotosResponseDeserializer;
import io.ychescale9.flickrsearch.data.model.SearchPhotosResponse;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by yang on 6/4/17.
 */
@Module
public class TestApiModule {

    @Provides
    @Singleton
    OkHttpClient provideOkHttpClient(ApiConstants apiConstants) {
        final OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.connectTimeout(apiConstants.getConnectTimeOutMilliseconds(), TimeUnit.MILLISECONDS)
                .readTimeout(apiConstants.getReadTimeOutMilliseconds(), TimeUnit.MILLISECONDS)
                .writeTimeout(apiConstants.getWriteTimeOutMilliseconds(), TimeUnit.MILLISECONDS);
        return builder.build();
    }

    @Provides
    @Singleton
    Retrofit provideRetrofit(OkHttpClient client,
                             ApiConstants apiConstants,
                             SearchPhotosResponseDeserializer searchPhotosResponseDeserializer) {
        // create Gson object using custom JsonDeserializers
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(SearchPhotosResponse.class, searchPhotosResponseDeserializer);
        Gson gson = gsonBuilder.create();

        return new Retrofit.Builder()
                .baseUrl(apiConstants.getApiBaseUrl())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
    }

    @Provides
    @Singleton
    FlickrSearchService provideFlickrSearchService(Retrofit retrofit) {
        return retrofit.create(FlickrSearchService.class);
    }
}
