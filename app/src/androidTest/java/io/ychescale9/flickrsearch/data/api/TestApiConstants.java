package io.ychescale9.flickrsearch.data.api;

/**
 * Created by yang on 6/4/17.
 */
public class TestApiConstants extends ApiConstants {

    private final int mockServerPort;

    public TestApiConstants(int mockServerPort) {
        this.mockServerPort = mockServerPort;
    }

    @Override
    public String getApiMethodSearchPhotos() {
        return "test.photos.search";
    }

    @Override
    public String getApiBaseUrl() {
        return "http://127.0.0.1:" + mockServerPort + "/";
    }

    @Override
    public int getConnectTimeOutMilliseconds() {
        return 0;
    }

    @Override
    public String getPhotoThumbnailUrlFormat() {
        return "http://localhost/flickrsearch/%s/%s_%s_q.jpg";
    }

    @Override
    public String getPhotoPageUrlFormat() {
        return "http://localhost/flickrsearch/photos/%s/%s";
    }

    @Override
    public int getReadTimeOutMilliseconds() {
        return 0;
    }

    @Override
    public int getWriteTimeOutMilliseconds() {
        return 0;
    }
}
