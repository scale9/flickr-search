package io.ychescale9.flickrsearch.ui;

import android.app.Instrumentation;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;

import javax.inject.Inject;

import io.ychescale9.flickrsearch.TestComponent;
import io.ychescale9.flickrsearch.TestFlickrSearchApplication;
import io.ychescale9.flickrsearch.TimeZoneUtcRule;
import io.ychescale9.flickrsearch.data.deserializer.SearchPhotosResponseDeserializer;
import io.ychescale9.flickrsearch.presentation.helper.PhotoUrlBuilder;
import io.ychescale9.flickrsearch.util.SystemAnimationsUtil;
import io.ychescale9.flickrsearch.util.TestUtil;
import okhttp3.mockwebserver.MockWebServer;

import static android.support.test.InstrumentationRegistry.getInstrumentation;

/**
 * Created by yang on 6/4/17.
 */
public abstract class BaseUiTest {

    @Rule
    public TimeZoneUtcRule timeZoneUtcRule = new TimeZoneUtcRule();

    @Inject
    PhotoUrlBuilder photoUrlBuilder;

    @Inject
    SearchPhotosResponseDeserializer searchPhotosResponseDeserializer;

    MockWebServer mockWebServer;

    TestFlickrSearchApplication testApp;

    @BeforeClass
    public static void setUpBeforeClass() throws Throwable {
        // disable animations before running tests
        SystemAnimationsUtil.disableAnimations(getInstrumentation().getTargetContext());
    }

    @AfterClass
    public static void tearDownAfterClass() throws Throwable {
        // re-enabled animations after running tests
        SystemAnimationsUtil.enableAnimations(getInstrumentation().getTargetContext());
    }

    @Before
    public void setUp() throws Throwable {
        Instrumentation instrumentation = getInstrumentation();
        testApp = (TestFlickrSearchApplication) instrumentation.getTargetContext().getApplicationContext();
        TestComponent component = (TestComponent) testApp.getComponent();
        component.inject(this);

        // start a new mock server
        mockWebServer = new MockWebServer();
        mockWebServer.start(TestUtil.MOCK_SERVER_PORT);
    }

    @After
    public void tearDown() throws Throwable {
        // shut down mock server
        mockWebServer.shutdown();
    }
}
