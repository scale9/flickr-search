package io.ychescale9.flickrsearch.ui;

import android.app.Instrumentation;
import android.content.ComponentName;
import android.content.Intent;
import android.net.Uri;
import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.espresso.intent.rule.IntentsTestRule;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.filters.LargeTest;
import android.support.test.runner.AndroidJUnit4;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.hamcrest.Matcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.ychescale9.flickrsearch.R;
import io.ychescale9.flickrsearch.TestComponent;
import io.ychescale9.flickrsearch.data.model.PhotoItem;
import io.ychescale9.flickrsearch.data.model.SearchPhotosResponse;
import io.ychescale9.flickrsearch.presentation.helper.ViewState;
import io.ychescale9.flickrsearch.presentation.home.HomeActivity;
import io.ychescale9.flickrsearch.presentation.searchfilters.SearchFilters;
import io.ychescale9.flickrsearch.presentation.searchfilters.SearchFiltersActivity;
import io.ychescale9.flickrsearch.util.TestData;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.RecordedRequest;

import static android.support.test.InstrumentationRegistry.getInstrumentation;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.Espresso.pressBack;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.pressImeActionButton;
import static android.support.test.espresso.action.ViewActions.replaceText;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.doesNotExist;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.contrib.RecyclerViewActions.scrollToPosition;
import static android.support.test.espresso.intent.Intents.intended;
import static android.support.test.espresso.intent.Intents.intending;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasAction;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasComponent;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasData;
import static android.support.test.espresso.matcher.ViewMatchers.hasFocus;
import static android.support.test.espresso.matcher.ViewMatchers.withEffectiveVisibility;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static io.ychescale9.flickrsearch.R.color.dummyPhoto1;
import static io.ychescale9.flickrsearch.R.color.dummyPhoto2;
import static io.ychescale9.flickrsearch.util.assertion.RecyclerViewItemCountAssertion.hasSize;
import static io.ychescale9.flickrsearch.util.matcher.DrawableMatcher.withDrawable;
import static io.ychescale9.flickrsearch.util.matcher.RecyclerViewMatcher.withRecyclerView;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.core.AllOf.allOf;
import static org.hamcrest.text.IsEmptyString.isEmptyString;
import static org.junit.Assert.assertThat;

/**
 * Created by yang on 6/4/17.
 */
@RunWith(AndroidJUnit4.class)
@LargeTest
public class HomeScreenTest extends BaseUiTest {

    @Inject
    ViewState<SearchFilters> searchFiltersViewState;

    @Rule
    public IntentsTestRule<HomeActivity> activityRule = new IntentsTestRule<>(
            HomeActivity.class,
            true,
            false); // launch activity manually

    private final List<PhotoItem> dummyPhotoItemsPage1 = new ArrayList<>();
    private final List<PhotoItem> dummyPhotoItemsPage2 = new ArrayList<>();

    @Override
    public void setUp() throws Throwable {
        super.setUp();

        TestComponent component = (TestComponent) testApp.getComponent();
        component.inject(this);

        // create Gson object using custom JsonDeserializers
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(SearchPhotosResponse.class, searchPhotosResponseDeserializer);
        Gson gson = gsonBuilder.create();

        // convert the dummy search photos response from json to SearchPhotosResponse
        SearchPhotosResponse response1 = gson.fromJson(TestData.TEST_PHOTOS_RESPONSE_PAGE_1_JSON, SearchPhotosResponse.class);
        dummyPhotoItemsPage1.clear();
        dummyPhotoItemsPage1.addAll(response1.getPhotoItems());

        SearchPhotosResponse response2 = gson.fromJson(TestData.TEST_PHOTOS_RESPONSE_PAGE_2_JSON, SearchPhotosResponse.class);
        dummyPhotoItemsPage2.clear();
        dummyPhotoItemsPage2.addAll(response2.getPhotoItems());
    }

    @Override
    public void tearDown() throws Throwable {
        super.tearDown();
        // reset search filters after each test
        searchFiltersViewState.update(new SearchFilters());
    }

    @Test
    public void openHomeScreen_initialStateDisplayed() throws Throwable {
        activityRule.launchActivity(null);

        // Verify that the search icon is displayed
        searchIconIsDisplayed();

        // Verify that the cancel button is NOT displayed
        cancelSearchButtonIsNotDisplayed();

        // Verify that the reset button is NOT displayed
        clearButtonIsNotDisplayed();

        // Verify that the search box has NO focus
        searchBoxHasNoFocus();

        // Verify that the search box is empty
        searchBoxIsEmpty();

        // Verify that the intro message is displayed
        introMessageIsDisplayed();

        // Verify that the search filters button is displayed
        searchFiltersButtonIsDisplayed();
    }

    @Test
    public void clickSearchBox_searchModeStarted() throws Throwable {
        activityRule.launchActivity(null);

        // click on the search box to start search mode
        onView(withId(R.id.edit_text_search_photos))
                .perform(click());

        // Verify that the search icon is NOT displayed
        searchIconIsNotDisplayed();

        // Verify that the cancel button is displayed
        cancelSearchButtonIsDisplayed();

        // Verify that the reset button is NOT displayed
        clearButtonIsNotDisplayed();

        // Verify that the search box has focus
        searchBoxHasFocus();

        // Verify that the search mode overlay is displayed
        searchModeOverlayIsDisplayed();
    }

    @Test
    public void enterSearchQuery_clearButtonVisibilityChanged() throws Throwable {
        activityRule.launchActivity(null);

        // type in some search query
        typeInSearchQuery("android");

        // reset the text box by replacing it with ""
        onView(withId(R.id.edit_text_search_photos))
                .perform(replaceText(""));

        // Verify that the search box is empty
        searchBoxIsEmpty();

        // Verify that the reset button is NOT displayed
        clearButtonIsNotDisplayed();
    }

    @Test
    public void submitSearchQueryWithResults_resultsDisplayed() throws Throwable {
        // given mock server returns valid response for the first page
        appendSearchPhotosResponsePage1();

        // given mock server returns valid response for the second page
        appendSearchPhotosResponsePage2();

        activityRule.launchActivity(null);

        // type in and submit some search query
        submitSearchQuery("android");

        // Verify that the search results list is displayed
        searchResultsListIsDisplayed();

        // Verify that the search results are correct
        searchResultsAreCorrect(dummyPhotoItemsPage1);

        // Verify that the intro message is NOT displayed
        introMessageIsNotDisplayed();

        // Verify that the no search results message is NOT displayed
        noSearchResultsMessageIsNotDisplayed();

        // Verify that the search mode overlay is NOT displayed
        searchModeOverlayIsNotDisplayed();

        // Verify that the search box does NOT have focus
        searchBoxHasNoFocus();
    }

    @Test
    public void submitSearchQueryWithNoResults_noResultsPromptMessageDisplayed() throws Throwable {
        // given mock server returns an empty response
        appendValidEmptyResponse();

        activityRule.launchActivity(null);

        // type in and submit some search query
        submitSearchQuery("android");

        // Verify that the search results are empty
        resultsListNumberOfItemsCorrect(0);

        // Verify that the intro message is NOT displayed
        introMessageIsNotDisplayed();

        // Verify that the no search results message is displayed
        noSearchResultsMessageIsDisplayed();

        // Verify that the search mode overlay is NOT displayed
        searchModeOverlayIsNotDisplayed();

        // Verify that the search box does NOT have focus
        searchBoxHasNoFocus();
    }

    @Test
    public void submitSearchQueryWithNoNetwork_searchPhotosErrorDisplayed() throws Throwable {
        // given mock server returns an error response
        appendErrorResponse();

        activityRule.launchActivity(null);

        // type in and submit some search query
        submitSearchQuery("android");

        // Verify that the search results are empty
        resultsListNumberOfItemsCorrect(0);

        // Verify that the no search results message is displayed
        noSearchResultsMessageIsDisplayed();

        // Verify that the search photos error snackbar is displayed
        searchPhotosErrorSnackbarIsDisplayed();
    }

    @Test
    public void clearSearchQuery_searchBoxEmpty() throws Throwable {
        activityRule.launchActivity(null);

        // type in some search query
        typeInSearchQuery("android");

        // Verify that the search box is NOT empty
        searchBoxIsNotEmpty();

        // Verify that the reset button is displayed
        clearButtonIsDisplayed();

        // click on the reset button
        onView(withId(R.id.button_clear))
                .perform(click());

        // Verify that the search box is empty
        searchBoxIsEmpty();

        // Verify that the reset button is NOT displayed
        clearButtonIsNotDisplayed();
    }

    @Test
    public void cancelSearch_searchModeEnded() throws Throwable {
        activityRule.launchActivity(null);

        // type in some search query
        typeInSearchQuery("android");

        // click on the cancel button
        onView(withId(R.id.button_cancel_search))
                .perform(click());

        // Verify that the search box is empty
        searchBoxIsEmpty();

        // Verify that the reset button is NOT displayed
        clearButtonIsNotDisplayed();

        // Verify that the search icon is displayed
        searchIconIsDisplayed();

        // Verify that the cancel button is NOT displayed
        cancelSearchButtonIsNotDisplayed();

        // Verify that the reset button is NOT displayed
        clearButtonIsNotDisplayed();

        // Verify that the search box has NO focus
        searchBoxHasNoFocus();

        // Verify that the search box is empty
        searchBoxIsEmpty();

        // Verify that the search mode overlay is NOT displayed
        searchModeOverlayIsNotDisplayed();

        // Verify that the intro message is displayed
        introMessageIsDisplayed();

        // Verify that the no search results message is NOT displayed
        noSearchResultsMessageIsNotDisplayed();

        // Verify that the search results list is NOT displayed
        searchResultsListIsNotDisplayed();
    }

    @Test
    public void cancelSearchByBackPressed_searchModeEnded() throws Throwable {
        // given mock server returns valid response for the first page
        appendSearchPhotosResponsePage1();

        // given mock server returns valid response for the second page
        appendSearchPhotosResponsePage2();

        activityRule.launchActivity(null);

        // type in some search query
        submitSearchQuery("android");

        // close softKeyboard first
        closeSoftKeyboard();

        // click on back button
        pressBack();

        // Verify that the search icon is displayed
        searchIconIsDisplayed();

        // Verify that the cancel button is NOT displayed
        cancelSearchButtonIsNotDisplayed();

        // Verify that the reset button is NOT displayed
        clearButtonIsNotDisplayed();

        // Verify that the search box has NO focus
        searchBoxHasNoFocus();

        // Verify that the search box is empty
        searchBoxIsEmpty();

        // Verify that the search mode overlay is NOT displayed
        searchModeOverlayIsNotDisplayed();

        // Verify that the intro message is displayed
        introMessageIsDisplayed();

        // Verify that the no search results message is NOT displayed
        noSearchResultsMessageIsNotDisplayed();

        // Verify that the search results list is NOT displayed
        searchResultsListIsNotDisplayed();
    }

    @Test
    public void cancelSearchWithResultsDisplayed_searchModeEnded() throws Throwable {
        // given mock server returns valid response for the first page
        appendSearchPhotosResponsePage1();

        // given mock server returns valid response for the second page
        appendSearchPhotosResponsePage2();

        activityRule.launchActivity(null);

        // type in and submit some search query
        submitSearchQuery("android");

        // click on the cancel button
        onView(withId(R.id.button_cancel_search))
                .perform(click());

        // Verify that the search box is empty
        searchBoxIsEmpty();

        // Verify that the reset button is NOT displayed
        clearButtonIsNotDisplayed();

        // Verify that the search icon is displayed
        searchIconIsDisplayed();

        // Verify that the cancel button is NOT displayed
        cancelSearchButtonIsNotDisplayed();

        // Verify that the reset button is NOT displayed
        clearButtonIsNotDisplayed();

        // Verify that the search box has NO focus
        searchBoxHasNoFocus();

        // Verify that the search box is empty
        searchBoxIsEmpty();

        // Verify that the search mode overlay is NOT displayed
        searchModeOverlayIsNotDisplayed();

        // Verify that the intro message is displayed
        introMessageIsDisplayed();

        // Verify that the no search results message is NOT displayed
        noSearchResultsMessageIsNotDisplayed();

        // Verify that the search results list is NOT displayed
        searchResultsListIsNotDisplayed();
    }

    @Test
    public void clickPhotoThumbnail_browserIntentLaunched() throws Throwable {
        // given mock server returns valid response for the first page
        appendSearchPhotosResponsePage1();

        // given mock server returns valid response for the second page
        appendSearchPhotosResponsePage2();

        activityRule.launchActivity(null);

        PhotoItem firstItem = dummyPhotoItemsPage1.get(0);
        String expectedUrl = photoUrlBuilder.buildPageUrl(firstItem.getOwner(), firstItem.getId());
        Uri expectedUri = Uri.parse(expectedUrl);

        // get expected intent
        Matcher<Intent> expectedIntent = allOf(hasAction(Intent.ACTION_VIEW), hasData(expectedUri));
        // intercept the intent
        intending(expectedIntent).respondWith(new Instrumentation.ActivityResult(0, null));

        // type in and submit some search query
        submitSearchQuery("android");

        // click on the first photo thumbnail
        onView(withId(R.id.recycler_view_photos))
                .perform(RecyclerViewActions.actionOnItemAtPosition(0, click()));

        // Verify that the correct intent has been launched
        intended(expectedIntent);
    }

    @Test
    public void scrollResultsListToBottom_nextPageOfResultsDisplayed() throws Throwable {
        // given mock server returns valid response for the first page
        appendSearchPhotosResponsePage1();

        // given mock server returns valid response for the second page
        appendSearchPhotosResponsePage2();

        activityRule.launchActivity(null);

        // type in and submit some search query
        submitSearchQuery("android");

        // Verify that the search results list is displayed
        searchResultsListIsDisplayed();

        // Verify that the search results are correct
        searchResultsAreCorrect(dummyPhotoItemsPage1);

        // scroll the list to the last item of page 1 to start pagination
        scrollResultsListToItem(dummyPhotoItemsPage1.size());

        // Verify that the search results for page 2 are correct
        List<PhotoItem> fullResults = new ArrayList<>(dummyPhotoItemsPage1);
        fullResults.addAll(dummyPhotoItemsPage2);
        searchResultsAreCorrect(fullResults);

        // verify that the total number of items in the 2 pages are correct
        resultsListNumberOfItemsCorrect(dummyPhotoItemsPage1.size() + dummyPhotoItemsPage2.size());
    }

    @Test
    public void clickOnSearchFiltersButton_searchFiltersActivityIntentLaunched() throws Throwable {
        activityRule.launchActivity(null);

        // get expected intent
        Matcher<Intent> expectedIntent = hasComponent(
                new ComponentName(
                        getInstrumentation().getTargetContext(),
                        SearchFiltersActivity.class)
        );

        // intercept the intent
        intending(expectedIntent).respondWith(new Instrumentation.ActivityResult(0, null));

        // click on the search filters button
        onView(withId(R.id.button_search_filters))
                .perform(click());

        // Verify that the correct intent has been launched
        intended(expectedIntent);
    }

    @Test
    public void searchFiltersUpdated_searchPhotosRequestWithFiltersMade() throws Throwable {
        // given mock server returns valid response for the first page
        appendSearchPhotosResponsePage1();

        // given mock server returns valid response for the second page
        appendSearchPhotosResponsePage2();

        activityRule.launchActivity(null);

        // type in and submit some search query
        submitSearchQuery("android");

        // scroll the list to the last item of page 1 to start pagination
        scrollResultsListToItem(dummyPhotoItemsPage1.size());

        // a search filters change has been detected
        SearchFilters searchFilters = new SearchFilters(1492000000000L, 1495000000000L);
        searchFiltersViewState.update(searchFilters);

        // Record the initial requests (page 1 and 2) before the search filters change
        mockWebServer.takeRequest();
        mockWebServer.takeRequest();

        // Verify that the search photos request has been re-submitted with the correct search filters
        RecordedRequest newRequestWithFilters = mockWebServer.takeRequest();
        assertThat(newRequestWithFilters.getPath(), containsString(
                "min_upload_date=" + searchFilters.getMinUploadDateMillis() / 1000));
        assertThat(newRequestWithFilters.getPath(), containsString(
                "max_upload_date=" + searchFilters.getMaxUploadDateMillis() / 1000));
    }

    //================================================================================
    // ACTIONS
    //================================================================================

    private void typeInSearchQuery(String queryString) {
        onView(withId(R.id.edit_text_search_photos))
                .perform(click())
                .perform(typeText(queryString));
    }

    private void submitSearchQuery(String queryString) {
        onView(withId(R.id.edit_text_search_photos))
                .perform(click())
                .perform(typeText(queryString))
                .perform(pressImeActionButton());
    }

    private void scrollResultsListToItem(int itemIndex) {
        onView(withId(R.id.recycler_view_photos))
                .perform(scrollToPosition(itemIndex));
    }

    //================================================================================
    // ASSERTIONS
    //================================================================================

    private void searchIconIsDisplayed() {
        onView(withId(R.id.image_view_search))
                .check(matches(withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE)));
    }

    private void searchIconIsNotDisplayed() {
        onView(withId(R.id.image_view_search))
                .check(matches(withEffectiveVisibility(ViewMatchers.Visibility.GONE)));
    }

    private void searchFiltersButtonIsDisplayed() {
        onView(withId(R.id.button_search_filters))
                .check(matches(withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE)));
    }

    private void cancelSearchButtonIsDisplayed() {
        onView(withId(R.id.button_cancel_search))
                .check(matches(withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE)));
    }

    private void cancelSearchButtonIsNotDisplayed() {
        onView(withId(R.id.button_cancel_search))
                .check(matches(withEffectiveVisibility(ViewMatchers.Visibility.GONE)));
    }

    private void clearButtonIsDisplayed() {
        onView(withId(R.id.button_clear))
                .check(matches(withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE)));
    }

    private void clearButtonIsNotDisplayed() {
        onView(withId(R.id.button_clear))
                .check(matches(withEffectiveVisibility(ViewMatchers.Visibility.GONE)));
    }

    private void searchBoxHasFocus() {
        onView(withId(R.id.edit_text_search_photos))
                .check(matches(hasFocus()));
    }

    private void searchBoxHasNoFocus() {
        onView(withId(R.id.edit_text_search_photos))
                .check(matches(not(hasFocus())));
    }

    private void searchBoxIsEmpty() {
        onView(withId(R.id.edit_text_search_photos))
                .check(matches(withText(isEmptyString())));
    }

    private void searchBoxIsNotEmpty() {
        onView(withId(R.id.edit_text_search_photos))
                .check(matches(withText(not(isEmptyString()))));
    }

    private void searchModeOverlayIsDisplayed() {
        onView(withId(R.id.search_mode_overlay))
                .check(matches(withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE)));
    }

    private void searchModeOverlayIsNotDisplayed() {
        onView(withId(R.id.search_mode_overlay))
                .check(matches(withEffectiveVisibility(ViewMatchers.Visibility.GONE)));
    }

    private void introMessageIsDisplayed() {
        onView(withId(R.id.text_view_intro_message))
                .check(matches(withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE)));
    }

    private void introMessageIsNotDisplayed() {
        onView(withId(R.id.text_view_intro_message))
                .check(matches(withEffectiveVisibility(ViewMatchers.Visibility.GONE)));
    }

    private void searchResultsListIsDisplayed() {
        onView(withId(R.id.recycler_view_photos))
                .check(matches(withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE)));
    }

    private void searchResultsListIsNotDisplayed() {
        onView(withId(R.id.recycler_view_photos))
                .check(matches(withEffectiveVisibility(ViewMatchers.Visibility.GONE)));
    }

    private void noSearchResultsMessageIsDisplayed() {
        onView(withId(R.id.text_view_no_photos_found_message))
                .check(matches(withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE)));
    }

    private void noSearchResultsMessageIsNotDisplayed() {
        onView(withId(R.id.text_view_no_photos_found_message))
                .check(matches(withEffectiveVisibility(ViewMatchers.Visibility.GONE)));
    }

    private void searchPhotosErrorSnackbarIsDisplayed() {
        onView(withText(R.string.error_message_could_not_search_photos))
                .check(matches(withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE)));
    }

    private void searchPhotosErrorSnackbarIsNotDisplayed() {
        onView(withText(R.string.error_message_could_not_search_photos))
                .check(doesNotExist());
    }

    private void searchResultsAreCorrect(List<PhotoItem> expectedResults) {
        for (int index = 0; index < expectedResults.size(); index++) {
            int colorResId;
            if (index % 2 == 0) {
                colorResId = dummyPhoto1;
            } else {
                colorResId = dummyPhoto2;
            }

            // make sure the item is visible in the screen
            scrollResultsListToItem(index);

            onView(withRecyclerView(R.id.recycler_view_photos)
                    .atPositionOnView(index, R.id.image_view_photo))
                    .check(matches(withDrawable(colorResId)));
        }
    }

    private void resultsListNumberOfItemsCorrect(int expectedNumberOfItems) {
        // note expectedNumberOfItems is for number of data items
        // which does not include the loading progress bar at the bottom
        onView(withId(R.id.recycler_view_photos))
                .check(hasSize(expectedNumberOfItems + 1)); // plus the loading progress bar
    }

    //================================================================================
    // HELPERS
    //================================================================================

    private void appendSearchPhotosResponsePage1() {
        mockWebServer.enqueue(new MockResponse()
                .setResponseCode(200)
                .setBody(TestData.TEST_PHOTOS_RESPONSE_PAGE_1_JSON)
        );
    }

    private void appendSearchPhotosResponsePage2() {
        mockWebServer.enqueue(new MockResponse()
                .setResponseCode(200)
                .setBody(TestData.TEST_PHOTOS_RESPONSE_PAGE_2_JSON)
        );
    }

    private void appendValidEmptyResponse() {
        mockWebServer.enqueue(new MockResponse()
                .setResponseCode(200)
                .setBody(TestData.EMPTY_PHOTOS_RESPONSE_JSON)
        );
    }

    private void appendErrorResponse() {
        mockWebServer.enqueue(new MockResponse()
                .setResponseCode(404)
        );
    }
}
