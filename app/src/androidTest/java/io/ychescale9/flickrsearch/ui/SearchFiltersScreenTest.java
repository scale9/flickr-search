package io.ychescale9.flickrsearch.ui;

import android.support.annotation.NonNull;
import android.support.test.espresso.contrib.PickerActions;
import android.support.test.espresso.intent.rule.IntentsTestRule;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.filters.LargeTest;
import android.support.test.runner.AndroidJUnit4;

import org.joda.time.DateTime;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;

import io.reactivex.subscribers.TestSubscriber;
import io.ychescale9.flickrsearch.R;
import io.ychescale9.flickrsearch.TestComponent;
import io.ychescale9.flickrsearch.presentation.helper.DateUtil;
import io.ychescale9.flickrsearch.presentation.helper.ViewState;
import io.ychescale9.flickrsearch.presentation.searchfilters.SearchFilters;
import io.ychescale9.flickrsearch.presentation.searchfilters.SearchFiltersActivity;

import static android.support.test.InstrumentationRegistry.getInstrumentation;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.pressBack;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.withEffectiveVisibility;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static io.ychescale9.flickrsearch.util.assertion.DatePickerDateRangeAssertion.hasMaxDate;
import static io.ychescale9.flickrsearch.util.assertion.DatePickerDateRangeAssertion.hasMinDate;
import static io.ychescale9.flickrsearch.util.matcher.CustomMatchers.withDatePicker;
import static io.ychescale9.flickrsearch.util.matcher.CustomMatchers.withToolbarNavigationButton;
import static io.ychescale9.flickrsearch.util.matcher.CustomMatchers.withToolbarTitleTextView;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 * Created by yang on 18/4/17.
 */
@RunWith(AndroidJUnit4.class)
@LargeTest
public class SearchFiltersScreenTest extends BaseUiTest {

    @Inject
    ViewState<SearchFilters> searchFiltersViewState;

    @Rule
    public IntentsTestRule<SearchFiltersActivity> activityRule = new IntentsTestRule<>(
            SearchFiltersActivity.class,
            true,
            false); // launch activity manually

    @Override
    public void setUp() throws Throwable {
        super.setUp();

        TestComponent component = (TestComponent) testApp.getComponent();
        component.inject(this);
    }

    @Override
    public void tearDown() throws Throwable {
        super.tearDown();
        // reset search filters after each test
        searchFiltersViewState.update(new SearchFilters());
    }

    @Test
    public void openSearchFiltersScreen_defaultFiltersDisplayed() throws Throwable {
        activityRule.launchActivity(null);

        // Verify that the cancel button is displayed
        cancelButtonIsDisplayed();

        // Verify that the activity title is displayed
        activityTitleIsCorrect(
                getInstrumentation().getTargetContext().getResources().getString(
                        R.string.activity_title_search_filters)
        );

        // Verify that the reset button is displayed
        clearButtonIsDisplayed();

        // Verify that the min upload date filter is populated with default value
        minUploadDateFilterHasDefaultValue();

        // Verify that the max upload date filter is populated with default value
        maxUploadDateFilterHasDefaultValue();

        // Verify that the apply button is displayed
        applyButtonIsDisplayed();
    }

    @Test
    public void openSearchFiltersScreenWithConfiguredFilters_configuredFiltersDisplayed() throws Throwable {
        // given that some search filters have already been configured
        SearchFilters searchFilters = new SearchFilters(1492000000000L, 1495000000000L);
        triggerSearchFiltersChange(searchFilters);

        activityRule.launchActivity(null);

        // Verify that the min upload date filter is populated with configured value
        minUploadDateFilterHasConfiguredValue(searchFilters.getMinUploadDateMillis());

        // Verify that the max upload date filter is populated with configured value
        maxUploadDateFilterHasConfiguredValue(searchFilters.getMaxUploadDateMillis());
    }

    @Test
    public void clickOnClearButton_filtersResetToDefaultValues() throws Throwable {
        // given that some search filters have already been configured
        SearchFilters searchFilters = new SearchFilters(1492000000000L, 1495000000000L);
        triggerSearchFiltersChange(searchFilters);

        activityRule.launchActivity(null);

        // click on the reset button
        clearFilters();

        // Verify that the min upload date filter is populated with default value
        minUploadDateFilterHasDefaultValue();

        // Verify that the max upload date filter is populated with default value
        maxUploadDateFilterHasDefaultValue();
    }

    @Test
    public void clickOnCancelButton_activityFinished() throws Throwable {
        activityRule.launchActivity(null);

        // click on the cancel button (navigation button)
        onView(withToolbarNavigationButton())
                .perform(click());

        // Verify that the activity is finishing
        assertThat(activityRule.getActivity().isFinishing(), is(true));
    }

    @Test
    public void changeMinUploadDate_updatedMinUploadDateFilterDisplayed() throws Throwable {
        activityRule.launchActivity(null);

        // set a new min upload date via the date picker
        DateTime newMinUploadDate = new DateTime(2017, 1, 20, 0, 0);
        updateMinUploadDate(newMinUploadDate);

        // Verify that the updated min upload date is populated
        minUploadDateFilterHasConfiguredValue(newMinUploadDate.getMillis());

        // Verify that min date of the max upload date picker has been updated
        maxUploadDatePickerHasCorrectMinDateAllowed(newMinUploadDate.getMillis());
    }

    @Test
    public void changeMaxUploadDate_updatedMaxUploadDateFilterDisplayed() throws Throwable {
        activityRule.launchActivity(null);

        // set a new max upload date via the date picker
        DateTime newMaxUploadDate = new DateTime(2017, 1, 20, 0, 0);
        updateMaxUploadDate(newMaxUploadDate);

        // Verify that the updated max upload date is populated
        maxUploadDateFilterHasConfiguredValue(newMaxUploadDate.getMillis());

        // Verify that max date the min upload date picker has been updated
        minUploadDatePickerHasCorrectMaxDateAllowed(newMaxUploadDate.getMillis());
    }

    @Test
    public void clickOnApplyButton_searchFiltersChangeNotified() throws Throwable {
        activityRule.launchActivity(null);

        // set a new min upload date via the date picker
        DateTime newMinUploadDate = new DateTime(2017, 1, 20, 0, 0);
        updateMinUploadDate(newMinUploadDate);

        // set a new max upload date via the date picker
        DateTime newMaxUploadDate = new DateTime(2017, 4, 20, 0, 0);
        updateMaxUploadDate(newMaxUploadDate);

        // apply the filters
        applyFilters();

        // Verify that the search filters change has been notified
        SearchFilters expectedSearchFilters = new SearchFilters(
                newMinUploadDate.getMillis(), newMaxUploadDate.getMillis());
        TestSubscriber<SearchFilters> testSubscriber = searchFiltersViewState.get().test();
        testSubscriber.assertValue(expectedSearchFilters);

        // Verify that the activity is finishing
        assertThat(activityRule.getActivity().isFinishing(), is(true));
    }

    //================================================================================
    // ACTIONS
    //================================================================================

    private void clearFilters() {
        onView(withId(R.id.action_clear_filters))
                .perform(click());
    }

    private void updateMinUploadDate(DateTime newMinUploadDate) {
        onView(withId(R.id.button_min_upload_date_filter))
                .perform(click());

        onView(withDatePicker())
                .perform(PickerActions.setDate(
                        newMinUploadDate.getYear(),
                        newMinUploadDate.getMonthOfYear(),
                        newMinUploadDate.getDayOfMonth()
                ));

        onView(withId(android.R.id.button1)).perform(click());
    }

    private void updateMaxUploadDate(DateTime newMaxUploadDate) {
        onView(withId(R.id.button_max_upload_date_filter))
                .perform(click());

        onView(withDatePicker())
                .perform(PickerActions.setDate(
                        newMaxUploadDate.getYear(),
                        newMaxUploadDate.getMonthOfYear(),
                        newMaxUploadDate.getDayOfMonth()
                ));

        onView(withId(android.R.id.button1)).perform(click());
    }

    private void applyFilters() {
        onView(withId(R.id.button_apply_filters))
                .perform(click());
    }

    //================================================================================
    // ASSERTIONS
    //================================================================================

    private void activityTitleIsCorrect(@NonNull String expectedTitle) {
        onView(withToolbarTitleTextView())
                .check(matches(withText(expectedTitle)));
    }

    private void cancelButtonIsDisplayed() {
        onView(withToolbarNavigationButton())
                .check(matches(withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE)));
    }

    private void clearButtonIsDisplayed() {
        onView(withId(R.id.action_clear_filters))
                .check(matches(withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE)));
    }

    private void applyButtonIsDisplayed() {
        onView(withId(R.id.button_apply_filters))
                .check(matches(withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE)));
    }

    private void minUploadDateFilterHasDefaultValue() {
        onView(withId(R.id.text_view_min_upload_date))
                .check(matches(withText(
                        getInstrumentation().getTargetContext().getResources().getString(
                                R.string.prompt_message_min_upload_date_filter)
                )));
    }

    private void maxUploadDateFilterHasDefaultValue() {
        onView(withId(R.id.text_view_max_upload_date))
                .check(matches(withText(
                        getInstrumentation().getTargetContext().getResources().getString(
                                R.string.prompt_message_max_upload_date_filter)
                )));
    }

    private void minUploadDateFilterHasConfiguredValue(long expectedDate) {
        String formattedTimestamp = DateUtil.formatDateTime(expectedDate);
        onView(withId(R.id.text_view_min_upload_date))
                .check(matches(withText(formattedTimestamp)));
    }

    private void maxUploadDateFilterHasConfiguredValue(long expectedDate) {
        String formattedTimestamp = DateUtil.formatDateTime(expectedDate);
        onView(withId(R.id.text_view_max_upload_date))
                .check(matches(withText(formattedTimestamp)));
    }

    private void minUploadDatePickerHasCorrectMaxDateAllowed(long expectedMaxDate) {
        // assume no date picker dialog is currently opened
        onView(withId(R.id.button_min_upload_date_filter))
                .perform(click());

        onView(withDatePicker())
                .check(hasMaxDate(expectedMaxDate));

        onView(withDatePicker())
                .perform(pressBack());
    }

    private void maxUploadDatePickerHasCorrectMinDateAllowed(long expectedMinDate) {
        // assume no date picker dialog is currently opened
        onView(withId(R.id.button_max_upload_date_filter))
                .perform(click());

        onView(withDatePicker())
                .check(hasMinDate(expectedMinDate));

        onView(withDatePicker())
                .perform(pressBack());
    }

    //================================================================================
    // HELPERS
    //================================================================================

    private void triggerSearchFiltersChange(SearchFilters newSearchFilters) {
        searchFiltersViewState.update(newSearchFilters);
    }
}
