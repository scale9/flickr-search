package io.ychescale9.flickrsearch;

import android.app.Application;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import io.ychescale9.flickrsearch.data.DataModule;
import io.ychescale9.flickrsearch.data.api.TestApiModule;
import io.ychescale9.flickrsearch.data.repository.RepositoryModule;
import io.ychescale9.flickrsearch.presentation.ViewStateModule;
import io.ychescale9.flickrsearch.presentation.base.ActivityModule;
import io.ychescale9.flickrsearch.ui.BaseUiTest;
import io.ychescale9.flickrsearch.ui.HomeScreenTest;
import io.ychescale9.flickrsearch.ui.SearchFiltersScreenTest;

/**
 * Created by yang on 6/4/17.
 */
@Singleton
@Component(
        modules = {
                TestAppModule.class,
                ViewStateModule.class,
                TestConstantsModule.class,
                DataModule.class,
                RepositoryModule.class,
                TestApiModule.class,
                ActivityModule.class, // all activity sub-components
        }
)
public interface TestComponent extends AppComponent {

    void inject(BaseUiTest test);

    void inject(HomeScreenTest test);

    void inject(SearchFiltersScreenTest test);

    @Component.Builder
    interface Builder {

        @BindsInstance
        Builder app(Application application);

        TestComponent build();
    }
}
