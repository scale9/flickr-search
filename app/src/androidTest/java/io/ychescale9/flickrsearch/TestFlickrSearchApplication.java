package io.ychescale9.flickrsearch;

/**
 * Created by yang on 6/4/17.
 */
public class TestFlickrSearchApplication extends FlickrSearchApplication {

    /**
     * Setup Dagger component for test.
     */
    @Override
    protected AppComponent createComponent() {
        return DaggerTestComponent.builder()
                .app(this)
                .build();
    }
}
