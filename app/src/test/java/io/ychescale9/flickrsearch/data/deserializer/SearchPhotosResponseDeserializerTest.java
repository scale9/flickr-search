package io.ychescale9.flickrsearch.data.deserializer;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.junit.Before;
import org.junit.Test;

import io.ychescale9.flickrsearch.data.model.BaseResponse;
import io.ychescale9.flickrsearch.data.model.SearchPhotosResponse;
import io.ychescale9.flickrsearch.presentation.helper.PhotoUrlBuilderImpl;

import static junit.framework.Assert.assertNotNull;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 * Created by yang on 7/4/17.
 * Unit tests for the implementation of {@link SearchPhotosResponseDeserializer}
 */
public class SearchPhotosResponseDeserializerTest {

    final String DUMMY_PHOTO_RESPONSE_JSON = "{\"photos\":{\"page\":1,\"pages\":100,\"perpage\":3,\"total\":\"300\",\"photo\":[" +
            "{\"id\":\"1\",\"owner\":\"user1\",\"secret\":\"aaa\",\"server\":\"1000\",\"farm\":1,\"title\":\"Android Developer\",\"ispublic\":1,\"isfriend\":0,\"isfamily\":0}," +
            "{\"id\":\"2\",\"owner\":\"user2\",\"secret\":\"aaa\",\"server\":\"1000\",\"farm\":1,\"title\":\"#android nougat\",\"ispublic\":1,\"isfriend\":0,\"isfamily\":0}," +
            "{\"id\":\"3\",\"owner\":\"user3\",\"secret\":\"aaa\",\"server\":\"1000\",\"farm\":1,\"title\":\"#android O announced\",\"ispublic\":1,\"isfriend\":0,\"isfamily\":0}" +
            "]},\"stat\":\"ok\"}";

    final String DUMMY_FAILED_RESPONSE_JSON = "{\"stat\":\"failed\", \"code\":100, \"message\":\"invalid API key\"}";

    private final String thumbnailUrlFormat = "https://farm%s.staticflickr.com/%s/%s_%s_q.jpg";
    private final String originalUrlFormat = "https://farm%s.staticflickr.com/%s/%s_%s_o.jpg";

    Gson gson;

    @Before
    public void setUp() throws Exception {
        // create Gson object using custom JsonDeserializers
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(
                SearchPhotosResponse.class,
                new SearchPhotosResponseDeserializer(
                        new PhotoUrlBuilderImpl(thumbnailUrlFormat, originalUrlFormat)
                ));
        gson = gsonBuilder.create();
    }

    @Test
    public void deserialize() throws Exception {
        // deserialize dummy photo object in json
        SearchPhotosResponse searchPhotosResponse = gson.fromJson(DUMMY_PHOTO_RESPONSE_JSON, SearchPhotosResponse.class);

        // verify values of the deserialized object
        assertNotNull(searchPhotosResponse);
        assertThat(searchPhotosResponse.isValid(), is(true));
        assertThat(searchPhotosResponse.getPageNumber(), is(1));
        assertThat(searchPhotosResponse.getItemsPerPage(), is(3));
        assertThat(searchPhotosResponse.getTotalNumPages(), is(100));
        assertThat(searchPhotosResponse.getTotalNumItems(), is(300));
        // verify values for the nested photoItems list
        assertNotNull(searchPhotosResponse.getPhotoItems());
        // photo 1
        assertThat(searchPhotosResponse.getPhotoItems().get(0).getId(), is("1"));
        assertThat(searchPhotosResponse.getPhotoItems().get(0).getOwner(), is("user1"));
        assertThat(searchPhotosResponse.getPhotoItems().get(0).getSecret(), is("aaa"));
        assertThat(searchPhotosResponse.getPhotoItems().get(0).getServerId(), is("1000"));
        assertThat(searchPhotosResponse.getPhotoItems().get(0).getFarmId(), is(1));
        assertThat(searchPhotosResponse.getPhotoItems().get(0).getTitle(), is("Android Developer"));
        assertThat(searchPhotosResponse.getPhotoItems().get(0).getIsPublic(), is(1));
        assertThat(searchPhotosResponse.getPhotoItems().get(0).getIsFriend(), is(0));
        assertThat(searchPhotosResponse.getPhotoItems().get(0).getIsFamily(), is(0));
        assertThat(searchPhotosResponse.getPhotoItems().get(0).getThumbnailUrl(), is("https://farm1.staticflickr.com/1000/1_aaa_q.jpg"));
        // photo 2
        assertThat(searchPhotosResponse.getPhotoItems().get(1).getId(), is("2"));
        assertThat(searchPhotosResponse.getPhotoItems().get(1).getOwner(), is("user2"));
        assertThat(searchPhotosResponse.getPhotoItems().get(1).getSecret(), is("aaa"));
        assertThat(searchPhotosResponse.getPhotoItems().get(1).getServerId(), is("1000"));
        assertThat(searchPhotosResponse.getPhotoItems().get(1).getFarmId(), is(1));
        assertThat(searchPhotosResponse.getPhotoItems().get(1).getTitle(), is("#android nougat"));
        assertThat(searchPhotosResponse.getPhotoItems().get(1).getIsPublic(), is(1));
        assertThat(searchPhotosResponse.getPhotoItems().get(1).getIsFriend(), is(0));
        assertThat(searchPhotosResponse.getPhotoItems().get(1).getIsFamily(), is(0));
        assertThat(searchPhotosResponse.getPhotoItems().get(1).getThumbnailUrl(), is("https://farm1.staticflickr.com/1000/2_aaa_q.jpg"));
        // photo 3
        assertThat(searchPhotosResponse.getPhotoItems().get(2).getId(), is("3"));
        assertThat(searchPhotosResponse.getPhotoItems().get(2).getOwner(), is("user3"));
        assertThat(searchPhotosResponse.getPhotoItems().get(2).getSecret(), is("aaa"));
        assertThat(searchPhotosResponse.getPhotoItems().get(2).getServerId(), is("1000"));
        assertThat(searchPhotosResponse.getPhotoItems().get(2).getFarmId(), is(1));
        assertThat(searchPhotosResponse.getPhotoItems().get(2).getTitle(), is("#android O announced"));
        assertThat(searchPhotosResponse.getPhotoItems().get(2).getIsPublic(), is(1));
        assertThat(searchPhotosResponse.getPhotoItems().get(2).getIsFriend(), is(0));
        assertThat(searchPhotosResponse.getPhotoItems().get(2).getIsFamily(), is(0));
        assertThat(searchPhotosResponse.getPhotoItems().get(2).getThumbnailUrl(), is("https://farm1.staticflickr.com/1000/3_aaa_q.jpg"));
    }

    @Test
    public void deserialize_failedResponse() throws Exception {
        // deserialize failed response object in json
        SearchPhotosResponse searchPhotosResponse = gson.fromJson(DUMMY_FAILED_RESPONSE_JSON, SearchPhotosResponse.class);

        // verify values of the deserialized object
        assertNotNull(searchPhotosResponse);
        assertThat(searchPhotosResponse.isValid(), is(false));
        assertThat(searchPhotosResponse.getErrorCode(), is(100));
        assertThat(searchPhotosResponse.getStatus(), is(BaseResponse.RESPONSE_STATUS_FAIL));
        assertThat(searchPhotosResponse.getErrorMessage(), is("invalid API key"));
    }
}