package io.ychescale9.flickrsearch.data.repository;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;
import io.ychescale9.flickrsearch.data.api.ApiConstants;
import io.ychescale9.flickrsearch.data.api.FlickrSearchService;
import io.ychescale9.flickrsearch.data.model.PhotoItem;
import io.ychescale9.flickrsearch.data.model.SearchPhotosResponse;

import static io.ychescale9.flickrsearch.RxUtils.getOnNextValues;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.nullable;
import static org.mockito.Mockito.when;

/**
 * Created by yang on 7/4/17.
 * Unit tests for the implementation of {@link FlickrSearchRepositoryImpl}
 */
public class FlickrSearchRepositoryImplTest {

    SearchPhotosResponse dummySearchPhotosResponse;

    SearchPhotosResponse dummyFailedResponse;

    final String dummyApiMethodSearchPhotos = "photos.search";

    final String dummyQueryString = "mobile";

    private final String urlFormat = "https://farm%s.staticflickr.com/%s/%s_%s_q.jpg";

    @Mock
    FlickrSearchService mockFlickrSearchService;

    @Mock
    ApiConstants mockApiConstants;

    private TestObserver<SearchPhotosResponse> testObserver;

    private FlickrSearchRepositoryImpl flickrSearchRepository;

    @Before
    public void setUp() throws Exception {
        // mock annotated objects
        MockitoAnnotations.initMocks(this);

        // initialize / configure dummy objects
        List<PhotoItem> photoItems = new ArrayList<>();
        photoItems.add(new PhotoItem("1", "user1", "aaa", "1000", 1, "Android", 1, 0, 0, urlFormat));
        photoItems.add(new PhotoItem("2", "user2", "aaa", "1000", 1, "iOS", 1, 0, 0, urlFormat));
        photoItems.add(new PhotoItem("3", "user3", "aaa", "1000", 1, "Windows", 1, 0, 0, urlFormat));
        dummySearchPhotosResponse = new SearchPhotosResponse(
                1, 100, 3, 300, photoItems
        );

        dummyFailedResponse = new SearchPhotosResponse(
            100, "Invalid API Key"
        );

        testObserver = new TestObserver<>();
        flickrSearchRepository = new FlickrSearchRepositoryImpl(
                mockFlickrSearchService,
                mockApiConstants);

        // given the mockApiConstants returns the corresponding API methods
        when(mockApiConstants.getApiMethodSearchPhotos()).thenReturn(dummyApiMethodSearchPhotos);
    }

    @Test
    public void searchPhotos_validResponse() throws Exception {
        // given valid response
        when(mockFlickrSearchService.searchPhotos(
                eq(dummyApiMethodSearchPhotos),
                anyInt(), anyInt(), eq(dummyQueryString),
                nullable(Long.class), nullable(Long.class))
        ).thenReturn(Observable.just(dummySearchPhotosResponse));

        flickrSearchRepository.searchPhotos(1, 10, dummyQueryString, null, null).subscribe(testObserver);
        testObserver.assertNoErrors();

        // verify that the correct data has been loaded from server
        SearchPhotosResponse response = (SearchPhotosResponse) getOnNextValues(testObserver).get(0);
        assertThat(response, is(dummySearchPhotosResponse));
        assertThat(response.isValid(), is(true));
    }

    @Test
    public void searchPhotos_failedResponse() throws Exception {
        // given a failed response
        when(mockFlickrSearchService.searchPhotos(
                eq(dummyApiMethodSearchPhotos),
                anyInt(), anyInt(), eq(dummyQueryString),
                nullable(Long.class), nullable(Long.class))
        ).thenReturn(Observable.just(dummyFailedResponse));

        flickrSearchRepository.searchPhotos(1, 10, dummyQueryString, null, null).subscribe(testObserver);
        // onError should be called with the correct error message
        testObserver.assertErrorMessage(dummyFailedResponse.getErrorMessage());
    }

    @Test
    public void searchPhotos_serverError() throws Exception {
        // given remote service returns a generic error
        when(mockFlickrSearchService.searchPhotos(
                eq(dummyApiMethodSearchPhotos),
                anyInt(), anyInt(), eq(dummyQueryString),
                nullable(Long.class), nullable(Long.class))
        ).thenReturn(Observable.<SearchPhotosResponse>error(new Exception("server error")));

        flickrSearchRepository.searchPhotos(1, 10, dummyQueryString, null, null).subscribe(testObserver);

        // onError should be called with the correct error message
        testObserver.assertErrorMessage("server error");
    }
}