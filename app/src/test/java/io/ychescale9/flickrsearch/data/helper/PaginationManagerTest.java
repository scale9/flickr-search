package io.ychescale9.flickrsearch.data.helper;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.verify;

/**
 * Created by yang on 9/4/17.
 * Unit tests for the implementation of {@link PaginationManager}
 */
public class PaginationManagerTest {

    final String dummyQueryString = "mobile";

    final int numItemsPerPage = 3;

    @Mock
    PaginationManager.PaginationListener<String> mockPaginationListener;

    PaginationManager<String> paginationManager;

    @Before
    public void setUp() throws Exception {
        // mock annotated objects
        MockitoAnnotations.initMocks(this);
        paginationManager = new PaginationManager<>();
        paginationManager.setPaginationListener(mockPaginationListener);
    }

    @Test
    public void initialize() throws Exception {
        // initialize pagination manager
        paginationManager.initialize(dummyQueryString);

        // verify values
        assertThat(paginationManager.getQueryString(), is(dummyQueryString));
        assertThat(paginationManager.getItemsPerPage(), is(0));
        assertThat(paginationManager.getTotalNumPages(), is(0));
        assertThat(paginationManager.getLatestPage(), is(0));
        assertThat(paginationManager.getDataItems().size(), is(0));
    }

    @Test
    public void appendPage() throws Exception {
        // initialize pagination manager
        paginationManager.initialize(dummyQueryString);
        // append first page of items (total number of pages is 2)
        List<String> newDataItems = Arrays.asList("a", "b", "c");
        paginationManager.appendPage(1, numItemsPerPage, 2, newDataItems);

        // should invoke onNewPageAdded()
        verify(mockPaginationListener).onNewPageAdded(newDataItems);
        // verify values
        assertThat(paginationManager.getQueryString(), is(dummyQueryString));
        assertThat(paginationManager.getItemsPerPage(), is(numItemsPerPage));
        assertThat(paginationManager.getTotalNumPages(), is(2));
        assertThat(paginationManager.getLatestPage(), is(1));
        assertThat(paginationManager.getDataItems().size(), is(3));

        // append second page of items (total number of pages is 2)
        newDataItems = Arrays.asList("d", "e", "f");
        paginationManager.appendPage(2, numItemsPerPage, 2, newDataItems);

        // should invoke onNewPageAdded()
        verify(mockPaginationListener).onNewPageAdded(newDataItems);
        // verify values
        assertThat(paginationManager.getQueryString(), is(dummyQueryString));
        assertThat(paginationManager.getItemsPerPage(), is(numItemsPerPage));
        assertThat(paginationManager.getTotalNumPages(), is(2));
        assertThat(paginationManager.getLatestPage(), is(2));
        assertThat(paginationManager.getDataItems().size(), is(6));
    }

    @Test
    public void appendPage_invalidNewDataItems() throws Exception {
        // initialize pagination manager
        paginationManager.initialize(dummyQueryString);
        // append first page of items (total number of pages is 2)
        List<String> newDataItems = Arrays.asList("a", "b", "c");
        paginationManager.appendPage(1, numItemsPerPage, 2, newDataItems);

        // append second page of items with invalid parameter - empty newDataItems
        newDataItems = Collections.emptyList();
        paginationManager.appendPage(2, numItemsPerPage, 2, newDataItems);

        // should invoke onInvalidNewPage()
        verify(mockPaginationListener).onInvalidNewPage();
        // verify values
        assertThat(paginationManager.getQueryString(), is(dummyQueryString));
        assertThat(paginationManager.getItemsPerPage(), is(numItemsPerPage));
        assertThat(paginationManager.getTotalNumPages(), is(2));
        assertThat(paginationManager.getLatestPage(), is(1));
        assertThat(paginationManager.getDataItems().size(), is(3));
    }

    @Test
    public void appendPage_invalidPageNumber() throws Exception {
        // initialize pagination manager
        paginationManager.initialize(dummyQueryString);
        // append first page of items (total number of pages is 2)
        List<String> newDataItems = Arrays.asList("a", "b", "c");
        paginationManager.appendPage(1, numItemsPerPage, 2, newDataItems);

        // append second page of items with invalid parameter - invalid page number
        newDataItems = Arrays.asList("d", "e", "f");
        paginationManager.appendPage(3, numItemsPerPage, 2, newDataItems);

        // should invoke onInvalidNewPage()
        verify(mockPaginationListener).onInvalidNewPage();
        // verify values
        assertThat(paginationManager.getQueryString(), is(dummyQueryString));
        assertThat(paginationManager.getItemsPerPage(), is(numItemsPerPage));
        assertThat(paginationManager.getTotalNumPages(), is(2));
        assertThat(paginationManager.getLatestPage(), is(1));
        assertThat(paginationManager.getDataItems().size(), is(3));
    }

    @Test
    public void appendPage_invalidItemsPerPage() throws Exception {
        // initialize pagination manager
        paginationManager.initialize(dummyQueryString);
        // append first page of items (total number of pages is 2)
        List<String> newDataItems = Arrays.asList("a", "b", "c");
        paginationManager.appendPage(1, numItemsPerPage, 2, newDataItems);

        // append second page of items with invalid parameter - invalid items per page
        newDataItems = Arrays.asList("d", "e", "f");
        paginationManager.appendPage(2, 10, 2, newDataItems);

        // should invoke onInvalidNewPage()
        verify(mockPaginationListener).onInvalidNewPage();
        // verify values
        assertThat(paginationManager.getQueryString(), is(dummyQueryString));
        assertThat(paginationManager.getItemsPerPage(), is(numItemsPerPage));
        assertThat(paginationManager.getTotalNumPages(), is(2));
        assertThat(paginationManager.getLatestPage(), is(1));
        assertThat(paginationManager.getDataItems().size(), is(3));
    }

    @Test
    public void appendPage_invalidTotalNumPages() throws Exception {
        // initialize pagination manager
        paginationManager.initialize(dummyQueryString);
        // append first page of items (total number of pages is 2)
        List<String> newDataItems = Arrays.asList("a", "b", "c");
        paginationManager.appendPage(1, numItemsPerPage, 2, newDataItems);

        // append second page of items with invalid parameter - total number of pages is less than the latest page loaded
        newDataItems = Arrays.asList("d", "e", "f");
        paginationManager.appendPage(2, numItemsPerPage, 1, newDataItems);

        // should invoke onInvalidNewPage()
        verify(mockPaginationListener).onInvalidNewPage();
        // verify values
        assertThat(paginationManager.getQueryString(), is(dummyQueryString));
        assertThat(paginationManager.getItemsPerPage(), is(numItemsPerPage));
        assertThat(paginationManager.getTotalNumPages(), is(1));
        assertThat(paginationManager.getLatestPage(), is(1));
        assertThat(paginationManager.getDataItems().size(), is(3));
    }

    @Test
    public void clear() throws Exception {
        // initialize pagination manager
        paginationManager.initialize(dummyQueryString);
        // append first page of items (total number of pages is 2)
        List<String> newDataItems = Arrays.asList("a", "b", "c");
        paginationManager.appendPage(1, numItemsPerPage, 2, newDataItems);

        // reset pagination manager
        paginationManager.clear();

        // verify values
        assertThat(paginationManager.getQueryString(), is(""));
        assertThat(paginationManager.getItemsPerPage(), is(0));
        assertThat(paginationManager.getTotalNumPages(), is(0));
        assertThat(paginationManager.getLatestPage(), is(0));
        assertThat(paginationManager.getDataItems().size(), is(0));
    }
}