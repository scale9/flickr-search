package io.ychescale9.flickrsearch.data.model;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 * Created by yang on 7/4/17.
 * Unit tests for the implementation of {@link BaseResponse}
 */
public class BaseResponseTest {

    BaseResponseImpl baseResponse;

    @Before
    public void setUp() throws Exception {

    }

    @Test
    public void isValid_true() {
        // given a valid response
        baseResponse = new BaseResponseImpl(BaseResponseImpl.RESPONSE_STATUS_OK, 0, null);
        // verify that response is valid
        assertThat(baseResponse.isValid(), is(true));
    }

    @Test
    public void isValid_false() {
        // given a failed response
        baseResponse = new BaseResponseImpl(BaseResponseImpl.RESPONSE_STATUS_FAIL, 100, "Invalid API Key");
        // verify that response is invalid
        assertThat(baseResponse.isValid(), is(false));
    }

    private static class BaseResponseImpl extends BaseResponse {

        public BaseResponseImpl(String status, int errorCode, String errorMessage) {
            super(status, errorCode, errorMessage);
        }
    }

}