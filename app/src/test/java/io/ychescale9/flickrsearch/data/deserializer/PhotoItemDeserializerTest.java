package io.ychescale9.flickrsearch.data.deserializer;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.junit.Before;
import org.junit.Test;

import io.ychescale9.flickrsearch.data.model.PhotoItem;
import io.ychescale9.flickrsearch.presentation.helper.PhotoUrlBuilderImpl;

import static junit.framework.Assert.assertNotNull;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 * Created by yang on 7/4/17.
 * Unit tests for the implementation of {@link PhotoItemDeserializer}
 */
public class PhotoItemDeserializerTest {

    final String DUMMY_PHOTO_ITEM_JSON =
            "{\"id\":\"1\",\"owner\":\"user1\",\"secret\":\"aaa\",\"server\":\"1000\",\"farm\":1,\"title\":\"Android Developer\",\"ispublic\":1,\"isfriend\":0,\"isfamily\":0}";

    private final String thumbnailUrlFormat = "https://farm%s.staticflickr.com/%s/%s_%s_q.jpg";
    private final String originalUrlFormat = "https://farm%s.staticflickr.com/%s/%s_%s_o.jpg";

    Gson gson;

    @Before
    public void setUp() throws Exception {
        // create Gson object using custom JsonDeserializers
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(PhotoItem.class, new PhotoItemDeserializer(
                new PhotoUrlBuilderImpl(thumbnailUrlFormat, originalUrlFormat)
        ));
        gson = gsonBuilder.create();
    }

    @Test
    public void deserialize() throws Exception {
        // deserialize dummy photo object in json
        PhotoItem photoItem = gson.fromJson(DUMMY_PHOTO_ITEM_JSON, PhotoItem.class);

        // verify values of the deserialized object
        assertNotNull(photoItem);
        assertThat(photoItem.getId(), is("1"));
        assertThat(photoItem.getOwner(), is("user1"));
        assertThat(photoItem.getSecret(), is("aaa"));
        assertThat(photoItem.getServerId(), is("1000"));
        assertThat(photoItem.getFarmId(), is(1));
        assertThat(photoItem.getTitle(), is("Android Developer"));
        assertThat(photoItem.getIsPublic(), is(1));
        assertThat(photoItem.getIsFriend(), is(0));
        assertThat(photoItem.getIsFamily(), is(0));
        assertThat(photoItem.getThumbnailUrl(), is("https://farm1.staticflickr.com/1000/1_aaa_q.jpg"));

    }
}