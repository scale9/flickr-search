package io.ychescale9.flickrsearch;

import io.reactivex.Scheduler;
import io.reactivex.schedulers.Schedulers;
import io.ychescale9.flickrsearch.util.SchedulerProvider;

/**
 * Created by yang on 6/4/17.
 * SchedulerProvider for unit tests.
 */
public class TestSchedulerProvider extends SchedulerProvider {

    @Override
    public Scheduler ui() {
        return Schedulers.trampoline();
    }

    @Override
    public Scheduler io() {
        return Schedulers.trampoline();
    }

    @Override
    public Scheduler computation() {
        return Schedulers.trampoline();
    }
}
