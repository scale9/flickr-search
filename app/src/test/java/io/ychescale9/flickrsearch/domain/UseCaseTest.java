package io.ychescale9.flickrsearch.domain;

import org.junit.Before;
import org.junit.Test;

import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;
import io.ychescale9.flickrsearch.TestSchedulerProvider;
import io.ychescale9.flickrsearch.util.SchedulerProvider;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 * Created by yang on 7/4/17.
 * Unit tests for the implementation of {@link UseCase}
 */
public class UseCaseTest {

    final static String DUMMY_RESULT_1 = "result1";
    final static String DUMMY_RESULT_2 = "result2";

    TestObserver<String> testObserver;

    UseCaseImpl useCase;

    @Before
    public void setUp() {
        testObserver = new TestObserver<>();
        useCase = new UseCaseImpl(new TestSchedulerProvider());
    }

    @Test
    public void getStream() {
        useCase.getStream(new UseCaseImpl.RequestValues(true)).subscribeWith(testObserver);
        testObserver.awaitTerminalEvent();

        testObserver.assertValue(DUMMY_RESULT_1);
        testObserver.assertNoErrors();
    }

    @Test
    public void getRequestValues() {
        useCase.getStream(new UseCaseImpl.RequestValues(true)).subscribeWith(testObserver);

        assertThat(useCase.getRequestValues().getFlag(), is(true));
    }

    private static class UseCaseImpl extends UseCase<UseCaseImpl.RequestValues, String> {

        UseCaseImpl(SchedulerProvider schedulerProvider) {
            super(schedulerProvider);
        }

        @Override
        protected Observable<String> createUseCase() {
            // return different results based on request values
            if (getRequestValues().getFlag()) {
                return Observable.just(DUMMY_RESULT_1);
            } else {
                return Observable.just(DUMMY_RESULT_2);
            }
        }

        static final class RequestValues implements UseCase.RequestValues {

            private final boolean flag;

            RequestValues(boolean flag) {
                this.flag = flag;
            }

            boolean getFlag() {
                return flag;
            }
        }
    }
}
