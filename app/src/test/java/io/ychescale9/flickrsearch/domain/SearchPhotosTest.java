package io.ychescale9.flickrsearch.domain;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;
import io.ychescale9.flickrsearch.TestSchedulerProvider;
import io.ychescale9.flickrsearch.data.model.PhotoItem;
import io.ychescale9.flickrsearch.data.model.SearchPhotosResponse;
import io.ychescale9.flickrsearch.data.repository.FlickrSearchRepository;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.nullable;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by yang on 7/4/17.
 * Unit tests for the implementation of {@link SearchPhotos}
 */
public class SearchPhotosTest {

    SearchPhotosResponse dummySearchPhotosResponse;

    final String dummyQueryString = "mobile";

    final Long dummyMinUploadDateMillis = 1492000000000L;

    final Long dummyMaxUploadDateMillis = 1495000000000L;

    final String urlFormat = "https://farm%s.staticflickr.com/%s/%s_%s_q.jpg";

    @Mock
    FlickrSearchRepository mockFlickrSearchRepository;

    TestObserver<SearchPhotosResponse> testObserver;

    SearchPhotos searchPhotos;

    @Before
    public void setUp() throws Exception {
        // mock annotated objects
        MockitoAnnotations.initMocks(this);

        // initialize / configure dummy objects
        List<PhotoItem> photoItems = new ArrayList<>();
        photoItems.add(new PhotoItem("1", "user1", "aaa", "1000", 1, "Android", 1, 0, 0, urlFormat));
        photoItems.add(new PhotoItem("2", "user2", "aaa", "1000", 1, "iOS", 1, 0, 0, urlFormat));
        photoItems.add(new PhotoItem("3", "user3", "aaa", "1000", 1, "Windows", 1, 0, 0, urlFormat));
        dummySearchPhotosResponse = new SearchPhotosResponse(
                1, 100, 3, 300, photoItems
        );

        testObserver = new TestObserver<>();
        searchPhotos = new SearchPhotos(
                new TestSchedulerProvider(),
                mockFlickrSearchRepository);

        // stub mockFlickrSearchRepository
        // given that searchPhotos successfully returns a SearchPhotosResponse observable
        when(mockFlickrSearchRepository.searchPhotos(
                anyInt(), anyInt(), eq(dummyQueryString),
                nullable(Long.class), nullable(Long.class))
        ).thenReturn(Observable.just(dummySearchPhotosResponse));
    }

    @Test
    public void searchPhotos() {
        // when the use case is executed
        searchPhotos.getStream(new SearchPhotos.RequestValues(
                1, 10, dummyQueryString, dummyMinUploadDateMillis, dummyMaxUploadDateMillis))
                .subscribeWith(testObserver);
        testObserver.awaitTerminalEvent();

        // should search photos with the query string
        verify(mockFlickrSearchRepository).searchPhotos(
                1, 10, dummyQueryString, dummyMinUploadDateMillis / 1000, dummyMaxUploadDateMillis / 1000);

        // should return the correct search results
        testObserver.assertValue(dummySearchPhotosResponse);
    }

    @Test
    public void searchPhotos_minUploadDateIsZero() {
        // when the use case is executed with minUploadDate = 0
        searchPhotos.getStream(new SearchPhotos.RequestValues(
                1, 10, dummyQueryString, 0, dummyMaxUploadDateMillis))
                .subscribeWith(testObserver);
        testObserver.awaitTerminalEvent();

        // should use null for minUploadDate
        verify(mockFlickrSearchRepository).searchPhotos(
                1, 10, dummyQueryString, null, dummyMaxUploadDateMillis / 1000);
    }

    @Test
    public void searchPhotos_maxUploadDateIsZero() {
        // when the use case is executed with minUploadDate = 0
        searchPhotos.getStream(new SearchPhotos.RequestValues(
                1, 10, dummyQueryString, dummyMinUploadDateMillis, 0))
                .subscribeWith(testObserver);
        testObserver.awaitTerminalEvent();

        // should use null for maxUploadDate
        verify(mockFlickrSearchRepository).searchPhotos(
                1, 10, dummyQueryString, dummyMinUploadDateMillis / 1000, null);
    }
}
