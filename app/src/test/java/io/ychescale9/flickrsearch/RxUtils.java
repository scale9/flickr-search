package io.ychescale9.flickrsearch;

import android.support.annotation.NonNull;

import java.util.List;

import io.reactivex.Scheduler;
import io.reactivex.android.plugins.RxAndroidPlugins;
import io.reactivex.functions.Function;
import io.reactivex.observers.TestObserver;
import io.reactivex.plugins.RxJavaPlugins;

/**
 * Created by yang on 6/4/17.
 */
public final class RxUtils {

    /**
     * Override all schedulers with the scheduler passed in.
     * @param newScheduler
     */
    public static void overrideSchedulers(@NonNull final Scheduler newScheduler) {
        RxJavaPlugins.reset();
        RxJavaPlugins.setIoSchedulerHandler(new Function<Scheduler, Scheduler>() {
            @Override
            public Scheduler apply(Scheduler scheduler) {
                return newScheduler;
            }
        });

        RxJavaPlugins.setComputationSchedulerHandler(new Function<Scheduler, Scheduler>() {
            @Override
            public Scheduler apply(Scheduler scheduler) {
                return newScheduler;
            }
        });

        RxJavaPlugins.setNewThreadSchedulerHandler(new Function<Scheduler, Scheduler>() {
            @Override
            public Scheduler apply(Scheduler scheduler) {
                return newScheduler;
            }
        });

        RxAndroidPlugins.reset();
        RxAndroidPlugins.setMainThreadSchedulerHandler(new Function<Scheduler, Scheduler>() {
            @Override
            public Scheduler apply(Scheduler scheduler) throws Exception {
                return newScheduler;
            }
        });
    }

    /**
     * Reset all schedulers
     */
    public static void resetSchedulers() {
        RxJavaPlugins.reset();
        RxAndroidPlugins.reset();
    }

    /**
     * Get the onNext values received by the test observer
     * @param testObserver
     * @return
     */
    public static List<?> getOnNextValues(TestObserver testObserver) {
        return (List<?>) testObserver.getEvents().get(0);
    }
}
