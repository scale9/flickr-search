package io.ychescale9.flickrsearch.presentation.helper;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import io.ychescale9.flickrsearch.presentation.base.IPresenter;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;

/**
 * Created by yang on 6/4/17.
 * Unit tests for the implementation of {@link PresenterManager}
 */
public class PresenterManagerTest {

    @Mock
    private IPresenter mockPresenter1;

    @Mock
    private IPresenter mockPresenter2;

    @Mock
    private IPresenter mockPresenter3;

    private final class Class1 {}

    private final class Class2 {}

    private final class Class3 {}

    @Before
    public void setUp() {
        // mock annotated objects
        MockitoAnnotations.initMocks(this);
    }

    @After
    public void tearDown() {
        // remove all presenters.
        PresenterManager.getInstance().clear();
    }

    @Test
    public void test_savePresenter() {
        PresenterManager.getInstance().savePresenter(Class1.class, mockPresenter1);
        assertThat(PresenterManager.getInstance().getAllPresenters().contains(mockPresenter1), is(true));
    }

    @Test
    public void test_savePresenter_with_same_presenter_and_same_key_multiple_times() {
        PresenterManager.getInstance().savePresenter(Class1.class, mockPresenter1);
        PresenterManager.getInstance().savePresenter(Class1.class, mockPresenter1);
        PresenterManager.getInstance().savePresenter(Class1.class, mockPresenter1);
        assertThat(PresenterManager.getInstance().getAllPresenters().contains(mockPresenter1), is(true));
        assertThat(PresenterManager.getInstance().getAllPresenters().size(), is(1));
    }

    @Test
    public void test_savePresenter_with_multiple_presenters() {
        PresenterManager.getInstance().savePresenter(Class1.class, mockPresenter1);
        PresenterManager.getInstance().savePresenter(Class2.class, mockPresenter2);
        PresenterManager.getInstance().savePresenter(Class3.class, mockPresenter3);
        assertThat(PresenterManager.getInstance().getAllPresenters().contains(mockPresenter1), is(true));
        assertThat(PresenterManager.getInstance().getAllPresenters().contains(mockPresenter2), is(true));
        assertThat(PresenterManager.getInstance().getAllPresenters().contains(mockPresenter3), is(true));
    }

    @Test
    public void test_savePresenter_with_multiple_presenters_and_same_key() {
        PresenterManager.getInstance().savePresenter(Class1.class, mockPresenter1);
        PresenterManager.getInstance().savePresenter(Class1.class, mockPresenter2);
        PresenterManager.getInstance().savePresenter(Class1.class, mockPresenter3);
        assertThat(PresenterManager.getInstance().getAllPresenters().contains(mockPresenter1), is(false));
        assertThat(PresenterManager.getInstance().getAllPresenters().contains(mockPresenter2), is(false));
        assertThat(PresenterManager.getInstance().getAllPresenters().contains(mockPresenter3), is(true));
    }

    @Test
    public void test_restorePresenter() {
        assertThat(PresenterManager.getInstance().restorePresenter(Class1.class), is(nullValue()));
        PresenterManager.getInstance().savePresenter(Class1.class, mockPresenter1);
        assertThat(PresenterManager.getInstance().restorePresenter(Class1.class), is(mockPresenter1));
    }

    @Test
    public void test_removePresenter() {
        assertThat(PresenterManager.getInstance().restorePresenter(Class1.class), is(nullValue()));
        PresenterManager.getInstance().savePresenter(Class1.class, mockPresenter1);
        assertThat(PresenterManager.getInstance().restorePresenter(Class1.class), is(mockPresenter1));
        PresenterManager.getInstance().removePresenter(Class1.class);
        assertThat(PresenterManager.getInstance().restorePresenter(Class1.class), is(nullValue()));
    }
}
