package io.ychescale9.flickrsearch.presentation.searchfilters;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import io.reactivex.subscribers.TestSubscriber;
import io.ychescale9.flickrsearch.TestSchedulerProvider;
import io.ychescale9.flickrsearch.presentation.helper.ViewState;

import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;

/**
 * Created by yang on 18/4/17.
 * Unit tests for the implementation of {@link SearchFiltersPresenter}
 */
public class SearchFiltersPresenterTest {

    @Mock
    SearchFiltersContract.View mockView;

    @Spy
    ViewState<SearchFilters> spySearchFiltersViewState;

    SearchFiltersPresenter searchFiltersPresenter;

    @Before
    public void setUp() throws Exception {
        // mock annotated objects
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void searchFiltersChangeDetected() throws Exception {
        // given there are some search filters change available
        SearchFilters newSearchFilters = new SearchFilters(1492000000000L, 1495000000000L);
        spySearchFiltersViewState.get().test(); // first subscriber subscribes
        spySearchFiltersViewState.update(newSearchFilters);

        // create the presenter
        createPresenter();

        // should detect search filters change and update UI with the current search filters
        verify(mockView).showSelectedMinUploadDate(newSearchFilters.getMinUploadDateMillis());
        verify(mockView).showSelectedMaxUploadDate(newSearchFilters.getMaxUploadDateMillis());
    }

    @Test
    public void searchFiltersChange_subscriptionDisposedAfterReceivingFirstItem() throws Exception {
        // given there are some search filters change available
        SearchFilters newSearchFilters = new SearchFilters(1492000000000L, 1495000000000L);
        spySearchFiltersViewState.get().test(); // first subscriber subscribes
        spySearchFiltersViewState.update(newSearchFilters);

        // create the presenter
        createPresenter();

        // should detect search filters change and update UI with the selected search filters
        verify(mockView).showSelectedMinUploadDate(newSearchFilters.getMinUploadDateMillis());
        verify(mockView).showSelectedMaxUploadDate(newSearchFilters.getMaxUploadDateMillis());

        reset(mockView);

        // new search filters change detected
        newSearchFilters = new SearchFilters(1497000000000L, 1498000000000L);
        spySearchFiltersViewState.update(newSearchFilters);

        // should NOT detect search filters change as the subscription should have already been disposed
        // after receiving the first value
        verify(mockView, never()).showSelectedMinUploadDate(anyLong());
        verify(mockView, never()).showSelectedMaxUploadDate(anyLong());
    }

    @Test
    public void startMinUploadDateSelection() throws Exception {
        // given there are some search filters change available
        SearchFilters newSearchFilters = new SearchFilters(1492000000000L, 1495000000000L);
        spySearchFiltersViewState.get().test(); // first subscriber subscribes
        spySearchFiltersViewState.update(newSearchFilters);

        // create the presenter
        createPresenter();

        // start selecting min upload date
        searchFiltersPresenter.startMinUploadDateSelection();

        // should update UI to show the min upload date selection screen
        verify(mockView).showMinUploadDateSelectionScreen(
                newSearchFilters.getMinUploadDateMillis(),
                newSearchFilters.getMaxUploadDateMillis()
        );
    }

    @Test
    public void startMaxUploadDateSelection() throws Exception {
        // given there are some search filters change available
        SearchFilters newSearchFilters = new SearchFilters(1492000000000L, 1495000000000L);
        spySearchFiltersViewState.get().test(); // first subscriber subscribes
        spySearchFiltersViewState.update(newSearchFilters);

        // create the presenter
        createPresenter();

        // start selecting max upload date
        searchFiltersPresenter.startMaxUploadDateSelection();

        // should update UI to show the max upload date selection screen
        verify(mockView).showMaxUploadDateSelectionScreen(
                newSearchFilters.getMaxUploadDateMillis(),
                newSearchFilters.getMinUploadDateMillis()
        );
    }

    @Test
    public void setMinUploadDate() throws Exception {
        // create the presenter
        createPresenter();

        // set the min upload date
        long minUploadDate = 1497000000000L;
        searchFiltersPresenter.setMinUploadDate(1497000000000L);

        // should update UI with the selected min upload date
        verify(mockView).showSelectedMinUploadDate(minUploadDate);
    }

    @Test
    public void setMaxUploadDate() throws Exception {
        // create the presenter
        createPresenter();

        // set the max upload date
        long maxUploadDate = 1497000000000L;
        searchFiltersPresenter.setMaxUploadDate(1497000000000L);

        // should update UI with the selected max upload date
        verify(mockView).showSelectedMaxUploadDate(maxUploadDate);
    }

    @Test
    public void applyFilters() throws Exception {
        // create the presenter
        createPresenter();

        // set a new min upload date
        long newMinUploadTimestamp = 1492000000000L;
        searchFiltersPresenter.setMinUploadDate(newMinUploadTimestamp);

        // set a new max upload date
        long newMaxUploadTimestamp = 1495000000000L;
        searchFiltersPresenter.setMaxUploadDate(newMaxUploadTimestamp);

        // apply the new filters
        searchFiltersPresenter.applyFilters();

        // should detect the new search filters change
        SearchFilters appliedSearchFilters = new SearchFilters(newMinUploadTimestamp, newMaxUploadTimestamp);
        TestSubscriber<SearchFilters> testSubscriber = spySearchFiltersViewState.get().test();
        testSubscriber.assertValue(appliedSearchFilters);
    }

    @Test
    public void applyFilters_noChanged() throws Exception {
        // create the presenter
        createPresenter();

        // apply the filters (with no changes)
        searchFiltersPresenter.applyFilters();

        // should NOT detect the new search filters change
        TestSubscriber<SearchFilters> testSubscriber = spySearchFiltersViewState.get().test();
        testSubscriber.assertNoValues();
    }

    @Test
    public void clearFilters() throws Exception {
        // given there are some search filters change available
        SearchFilters newSearchFilters = new SearchFilters(1492000000000L, 1495000000000L);
        spySearchFiltersViewState.update(newSearchFilters);

        // create the presenter
        createPresenter();

        // reset the filters
        searchFiltersPresenter.clearFilters();

        // should update UI with the default min upload date
        verify(mockView).showDefaultMaxUploadDate();

        // should update UI with the default max upload date
        verify(mockView).showDefaultMinUploadDate();
    }

    @Test
    public void syncState_defaultSearchFilters() {
        // create the presenter
        createPresenter();

        // sync state
        searchFiltersPresenter.syncState();

        // should update UI with default min upload date
        verify(mockView).showDefaultMinUploadDate();

        // should update UI with default max upload date
        verify(mockView).showDefaultMaxUploadDate();
    }

    @Test
    public void syncState_updatedSearchFilters() {
        // create the presenter
        createPresenter();

        // set a new min upload date
        long newMinUploadTimestamp = 1492000000000L;
        searchFiltersPresenter.setMinUploadDate(newMinUploadTimestamp);

        // set a new max upload date
        long newMaxUploadTimestamp = 1495000000000L;
        searchFiltersPresenter.setMaxUploadDate(newMaxUploadTimestamp);

        reset(mockView);

        // sync state
        searchFiltersPresenter.syncState();

        // should update UI with currently selected min upload date
        verify(mockView).showSelectedMinUploadDate(newMinUploadTimestamp);

        // should update UI with currently selected max upload date
        verify(mockView).showSelectedMaxUploadDate(newMaxUploadTimestamp);
    }

    /**
     * Instantiate a new instance of the SearchFiltersPresenter
     */
    private void createPresenter() {
        searchFiltersPresenter = new SearchFiltersPresenter(
                mockView,
                spySearchFiltersViewState,
                new TestSchedulerProvider()
        );
    }
}