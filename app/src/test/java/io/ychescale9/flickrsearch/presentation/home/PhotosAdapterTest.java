package io.ychescale9.flickrsearch.presentation.home;

import android.content.Context;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import io.ychescale9.flickrsearch.data.model.PhotoItem;
import io.ychescale9.flickrsearch.presentation.helper.ImageLoader;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.spy;

/**
 * Created by yang on 16/4/17.
 * Unit tests for the implementation of {@link PhotosAdapter}
 */
public class PhotosAdapterTest {

    List<PhotoItem> dummyItemsPage1;

    List<PhotoItem> dummyItemsPage2;

    final String thumbnailUrlFormat = "https://farm%s.staticflickr.com/%s/%s_%s_q.jpg";

    @Mock
    Context mockContext;

    @Mock
    ImageLoader mockImageLoader;

    @Mock
    PhotosAdapter.ActionListener mockActionListener;

    @Before
    public void setUp() throws Exception {
        // mock annotated objects
        MockitoAnnotations.initMocks(this);

        // initialize / configure dummy objects
        dummyItemsPage1 = new ArrayList<>();
        dummyItemsPage1.add(new PhotoItem("1", "user1", "aaa", "1000", 1, "Android", 1, 0, 0, thumbnailUrlFormat));
        dummyItemsPage1.add(new PhotoItem("2", "user2", "aaa", "1000", 1, "iOS", 1, 0, 0, thumbnailUrlFormat));
        dummyItemsPage1.add(new PhotoItem("3", "user3", "aaa", "1000", 1, "Windows", 1, 0, 0, thumbnailUrlFormat));
        dummyItemsPage1.add(new PhotoItem("4", "user4", "aaa", "1000", 1, "Android", 1, 0, 0, thumbnailUrlFormat));

        dummyItemsPage2 = new ArrayList<>();
        dummyItemsPage2.add(new PhotoItem("5", "user5", "aaa", "1000", 1, "iOS", 1, 0, 0, thumbnailUrlFormat));
        dummyItemsPage2.add(new PhotoItem("6", "user6", "aaa", "1000", 1, "Windows", 1, 0, 0, thumbnailUrlFormat));
        dummyItemsPage2.add(new PhotoItem("7", "user7", "aaa", "1000", 1, "Windows", 1, 0, 0, thumbnailUrlFormat));
        dummyItemsPage2.add(new PhotoItem("8", "user8", "aaa", "1000", 1, "Windows", 1, 0, 0, thumbnailUrlFormat));
    }

    @Test
    public void addItems() throws Exception {
        // given a PhotosAdaptor with span count = 3
        int spanCount = 3;
        PhotosAdapter photosAdapter = spy(new PhotosAdapter(mockContext, mockImageLoader, spanCount, mockActionListener));
        stubPhotosAdapter(photosAdapter);

        // when items are added
        photosAdapter.addItems(dummyItemsPage1);

        // should have correct number of data items
        assertThat(photosAdapter.getDataItemCount(), is(dummyItemsPage1.size()));
        // should have correct number of items (including loading view at the bottom)
        assertThat(photosAdapter.getItemCount(), is(dummyItemsPage1.size() + 1));

        // when another page of items are added
        photosAdapter.addItems(dummyItemsPage2);

        // should have correct number of data items
        assertThat(photosAdapter.getDataItemCount(), is(dummyItemsPage1.size() + dummyItemsPage2.size()));
        // should have correct number of items (including loading view at the bottom)
        assertThat(photosAdapter.getItemCount(), is(dummyItemsPage1.size() + dummyItemsPage2.size() + 1));
    }

    @Test
    public void clear() throws Exception {
        // given a PhotosAdaptor with span count = 3
        int spanCount = 3;
        PhotosAdapter photosAdapter = spy(new PhotosAdapter(mockContext, mockImageLoader, spanCount, mockActionListener));
        stubPhotosAdapter(photosAdapter);

        // add some items
        photosAdapter.addItems(dummyItemsPage1);

        // when photosAdapter is cleared
        photosAdapter.clear();

        // should have 0 data items
        assertThat(photosAdapter.getDataItemCount(), is(0));
        // should have 1 items (including loading view at the bottom)
        assertThat(photosAdapter.getItemCount(), is(1));
    }

    @Test
    public void getItemViewType() throws Exception {
        // given a PhotosAdaptor with span count = 3
        int spanCount = 3;
        PhotosAdapter photosAdapter = spy(new PhotosAdapter(mockContext, mockImageLoader, spanCount, mockActionListener));
        stubPhotosAdapter(photosAdapter);

        // when items are added
        photosAdapter.addItems(dummyItemsPage1);

        // view type of first item should be data
        assertThat(photosAdapter.getItemViewType(0), is(PhotosAdapter.TYPE_DATA_ITEM));
        // view type of second last item should be data
        assertThat(photosAdapter.getItemViewType(dummyItemsPage1.size() - 1), is(PhotosAdapter.TYPE_DATA_ITEM));
        // view type of last item should be loading view
        assertThat(photosAdapter.getItemViewType(dummyItemsPage1.size()), is(PhotosAdapter.TYPE_LOADING_MORE));
    }

    @Test
    public void getItemCount() throws Exception {
        // given a PhotosAdaptor with span count = 3
        int spanCount = 3;
        PhotosAdapter photosAdapter = spy(new PhotosAdapter(mockContext, mockImageLoader, spanCount, mockActionListener));
        stubPhotosAdapter(photosAdapter);

        // should have 1 items (including loading view at the bottom) initially
        assertThat(photosAdapter.getItemCount(), is(1));

        // when items are added
        photosAdapter.addItems(dummyItemsPage1);

        // should have correct number of items (including loading view at the bottom)
        assertThat(photosAdapter.getItemCount(), is(dummyItemsPage1.size() + 1));
    }

    @Test
    public void getDataItemCount() throws Exception {
        // given a PhotosAdaptor with span count = 3
        int spanCount = 3;
        PhotosAdapter photosAdapter = spy(new PhotosAdapter(mockContext, mockImageLoader, spanCount, mockActionListener));
        stubPhotosAdapter(photosAdapter);

        // should have 0 data items initially
        assertThat(photosAdapter.getDataItemCount(), is(0));

        // when items are added
        photosAdapter.addItems(dummyItemsPage1);

        // should have correct number of items (including loading view at the bottom)
        assertThat(photosAdapter.getItemCount(), is(dummyItemsPage1.size() + 1));
    }

    @Test
    public void getItemSpanSize() throws Exception {
        // given a PhotosAdaptor with span count = 3
        int spanCount = 3;
        PhotosAdapter photosAdapter = spy(new PhotosAdapter(mockContext, mockImageLoader, spanCount, mockActionListener));
        stubPhotosAdapter(photosAdapter);

        // add some items
        photosAdapter.addItems(dummyItemsPage1);

        // items should have correct item column span
        assertThat(photosAdapter.getItemSpanSize(0), is(1));
        assertThat(photosAdapter.getItemSpanSize(1), is(1));
        assertThat(photosAdapter.getItemSpanSize(2), is(1));
        assertThat(photosAdapter.getItemSpanSize(3), is(1));
        assertThat(photosAdapter.getItemSpanSize(4), is(spanCount)); // loading view
    }

    private void stubPhotosAdapter(PhotosAdapter photosAdapter) {
        doNothing().when(photosAdapter).notifyItemRangeInserted(anyInt(), anyInt());
        doNothing().when(photosAdapter).notifyDataSetChanged();
    }
}