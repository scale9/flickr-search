package io.ychescale9.flickrsearch.presentation.searchfilters;

import org.junit.Test;

import static org.hamcrest.Matchers.not;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 * Created by yang on 18/4/17.
 * Unit tests for the implementation of {@link SearchFilters}
 */
public class SearchFiltersTest {

    @Test
    public void equals_true() throws Exception {
        SearchFilters searchFilters1 = new SearchFilters(1492000000000L, 1495000000000L);
        SearchFilters searchFilters2 = new SearchFilters(1492000000000L, 1495000000000L);

        assertThat(searchFilters1, is(searchFilters2));
    }

    @Test
    public void equals_false() throws Exception {
        SearchFilters searchFilters1 = new SearchFilters(1492000000000L, 1495000000000L);
        SearchFilters searchFilters2 = new SearchFilters(1492000000000L, 1498000000000L);

        assertThat(searchFilters1, not(searchFilters2));
    }

    @Test
    public void reset() throws Exception {
        SearchFilters searchFilters = new SearchFilters(1492000000000L, 1495000000000L);
        searchFilters.setMinUploadDateMillis(1492000000000L);
        searchFilters.setMaxUploadDateMillis(1495000000000L);

        // reset search filters
        searchFilters.reset();

        // search filters should have been reset
        assertThat(searchFilters.getMinUploadDateMillis(), is(0L));
        assertThat(searchFilters.getMaxUploadDateMillis(), is(0L));
    }
}