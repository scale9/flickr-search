package io.ychescale9.flickrsearch.presentation.home;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.schedulers.TestScheduler;
import io.ychescale9.flickrsearch.TestSchedulerProvider;
import io.ychescale9.flickrsearch.data.helper.PaginationManager;
import io.ychescale9.flickrsearch.data.model.PhotoItem;
import io.ychescale9.flickrsearch.data.model.SearchPhotosResponse;
import io.ychescale9.flickrsearch.domain.SearchPhotos;
import io.ychescale9.flickrsearch.presentation.helper.ViewState;
import io.ychescale9.flickrsearch.presentation.searchfilters.SearchFilters;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

/**
 * Created by yang on 9/4/17.
 * Unit tests for the implementation of {@link HomePresenter}
 */
public class HomePresenterTest {

    SearchPhotosResponse dummySearchPhotosResponsePage1;
    SearchPhotosResponse dummySearchPhotosResponsePage2;
    SearchPhotosResponse dummySearchPhotosEmptyResponse;

    final String dummyQueryString = "mobile";

    final String urlFormat = "https://farm%s.staticflickr.com/%s/%s_%s_q.jpg";

    final int numItemsPerPage = 3;

    @Mock
    HomeContract.View mockView;

    @Mock
    SearchPhotos mockSearchPhotos;

    @Captor
    ArgumentCaptor<SearchPhotos.RequestValues> requestValuesCaptor;

    @Spy
    ViewState<SearchFilters> spySearchFiltersViewState;

    HomePresenter homePresenter;

    @Before
    public void setUp() throws Exception {
        // mock annotated objects
        MockitoAnnotations.initMocks(this);

        // initialize / configure dummy objects
        // page 1
        List<PhotoItem> photoItems = new ArrayList<>();
        photoItems.add(new PhotoItem("1", "user1", "aaa", "1000", 1, "Android", 1, 0, 0, urlFormat));
        photoItems.add(new PhotoItem("2", "user2", "aaa", "1000", 1, "iOS", 1, 0, 0, urlFormat));
        photoItems.add(new PhotoItem("3", "user3", "aaa", "1000", 1, "Windows", 1, 0, 0, urlFormat));
        dummySearchPhotosResponsePage1 = new SearchPhotosResponse(
                1, 2, numItemsPerPage, numItemsPerPage * 2, photoItems
        );

        dummySearchPhotosEmptyResponse = new SearchPhotosResponse(
                1, 0, numItemsPerPage, 0, Collections.<PhotoItem>emptyList());

        // page 2
        photoItems = new ArrayList<>();
        photoItems.add(new PhotoItem("4", "user4", "aaa", "1000", 1, "title 4", 1, 0, 0, urlFormat));
        photoItems.add(new PhotoItem("5", "user5", "aaa", "1000", 1, "title 5", 1, 0, 0, urlFormat));
        photoItems.add(new PhotoItem("6", "user6", "aaa", "1000", 1, "title 6", 1, 0, 0, urlFormat));
        dummySearchPhotosResponsePage2 = new SearchPhotosResponse(
                2, 2, numItemsPerPage, numItemsPerPage * 2, photoItems
        );

        // empty response
        dummySearchPhotosEmptyResponse = new SearchPhotosResponse(
                1, 0, numItemsPerPage, 0, Collections.<PhotoItem>emptyList());

        homePresenter = new HomePresenter(
                mockView,
                mockSearchPhotos,
                new PaginationManager<PhotoItem>(),
                numItemsPerPage,
                spySearchFiltersViewState,
                new TestSchedulerProvider());
    }

    @Test
    public void enterSearchQuery() throws Exception {
        // start entering query string (empty first)
        homePresenter.enterSearchQuery("");

        // should update UI with correct state
        verify(mockView).updateViewState(
                HomeContract.SearchViewState.ENTERING_QUERY,
                HomeContract.ContentViewState.INTRO);
    }

    @Test
    public void enterSearchQuery_multipleTimes() throws Exception {
        // start entering query string (empty first)
        homePresenter.enterSearchQuery("");

        // enter some more query string
        homePresenter.enterSearchQuery(dummyQueryString);

        // should only update UI with states once
        verify(mockView, times(1)).updateViewState(
                HomeContract.SearchViewState.ENTERING_QUERY,
                HomeContract.ContentViewState.INTRO);
    }

    @Test
    public void submitSearchQuery() throws Exception {
        // given that mockSearchPhotos.getStream() returns a dummy page 1 result observable
        when(mockSearchPhotos.getStream(any(SearchPhotos.RequestValues.class))).thenReturn(Observable.just(dummySearchPhotosResponsePage1));

        // start entering query string
        homePresenter.enterSearchQuery(dummyQueryString);

        // submit
        homePresenter.submitSearchQuery(dummyQueryString);

        // should get stream
        verify(mockSearchPhotos).getStream(any(SearchPhotos.RequestValues.class));

        // should update UI with results
        verify(mockView).showSearchResults(dummySearchPhotosResponsePage1.getPhotoItems());

        // should update UI with correct state
        verify(mockView).updateViewState(
                HomeContract.SearchViewState.SUBMITTED_QUERY, HomeContract.ContentViewState.RESULTS_DISPLAYED);
    }

    @Test
    public void submitSearchQuery_empty() throws Exception {
        // start entering query string (empty)
        homePresenter.enterSearchQuery("");

        reset(mockView);

        // submit
        homePresenter.submitSearchQuery("");

        // should NOT get stream
        verify(mockSearchPhotos, never()).getStream(any(SearchPhotos.RequestValues.class));

        // should have no more interaction with the view
        verifyNoMoreInteractions(mockView);
    }

    @Test
    public void submitSearchQuery_error() throws Exception {
        // given that mockSearchPhotos.getStream() returns an error observable
        when(mockSearchPhotos.getStream(any(SearchPhotos.RequestValues.class))).thenReturn(Observable.<SearchPhotosResponse>error(new Exception()));

        // start entering query string
        homePresenter.enterSearchQuery(dummyQueryString);

        // submit
        homePresenter.submitSearchQuery(dummyQueryString);

        // should get stream
        verify(mockSearchPhotos).getStream(any(SearchPhotos.RequestValues.class));

        // should update UI with error message
        verify(mockView).showCannotSearchPhotosError();

        // should update UI with correct state
        verify(mockView).updateViewState(
                HomeContract.SearchViewState.SUBMITTED_QUERY, HomeContract.ContentViewState.RESULTS_DISPLAYED);
    }

    @Test
    public void submitSearchQuery_noMatch() throws Exception {
        // given that mockSearchPhotos.getStream() returns an empty result observable
        when(mockSearchPhotos.getStream(any(SearchPhotos.RequestValues.class))).thenReturn(Observable.just(dummySearchPhotosEmptyResponse));

        // start entering query string
        homePresenter.enterSearchQuery(dummyQueryString);

        // submit
        homePresenter.submitSearchQuery(dummyQueryString);

        // should get stream
        verify(mockSearchPhotos).getStream(any(SearchPhotos.RequestValues.class));

        // should update UI with results
        verify(mockView).showSearchResults(dummySearchPhotosEmptyResponse.getPhotoItems());

        // should update UI with correct state
        verify(mockView).updateViewState(
                HomeContract.SearchViewState.SUBMITTED_QUERY, HomeContract.ContentViewState.RESULTS_DISPLAYED);
    }

    @Test
    public void loadNextPage() throws Exception {
        // given that mockSearchPhotos.getStream() returns a dummy page 1 result observable
        when(mockSearchPhotos.getStream(any(SearchPhotos.RequestValues.class))).thenReturn(Observable.just(dummySearchPhotosResponsePage1));

        // start entering query string
        homePresenter.enterSearchQuery(dummyQueryString);

        // submit
        homePresenter.submitSearchQuery(dummyQueryString);

        // should get stream
        verify(mockSearchPhotos).getStream(any(SearchPhotos.RequestValues.class));

        reset(mockView, mockSearchPhotos);

        // given that mockSearchPhotos.getStream() returns a dummy page 2 result observable
        when(mockSearchPhotos.getStream(any(SearchPhotos.RequestValues.class))).thenReturn(Observable.just(dummySearchPhotosResponsePage2));

        // load next page
        homePresenter.loadNextPage();

        // should get stream
        verify(mockSearchPhotos).getStream(any(SearchPhotos.RequestValues.class));

        // should update UI with more results (page 2)
        verify(mockView).showMoreSearchResults(dummySearchPhotosResponsePage2.getPhotoItems());
    }

    @Test
    public void loadNextPage_error() throws Exception {
        // given that mockSearchPhotos.getStream() returns a dummy page 1 result observable
        when(mockSearchPhotos.getStream(any(SearchPhotos.RequestValues.class))).thenReturn(Observable.just(dummySearchPhotosResponsePage1));

        // start entering query string
        homePresenter.enterSearchQuery(dummyQueryString);

        // submit
        homePresenter.submitSearchQuery(dummyQueryString);

        reset(mockView, mockSearchPhotos);

        // given that mockSearchPhotos.getStream() returns an error observable
        when(mockSearchPhotos.getStream(any(SearchPhotos.RequestValues.class))).thenReturn(Observable.<SearchPhotosResponse>error(new Exception()));

        // load next page
        homePresenter.loadNextPage();

        // should update UI with error message
        verify(mockView).showCannotSearchPhotosError();
    }

    @Test
    public void loadNextPage_invalidPageReturned() throws Exception {
        // given that mockSearchPhotos.getStream() returns a dummy page 1 result observable
        when(mockSearchPhotos.getStream(any(SearchPhotos.RequestValues.class))).thenReturn(Observable.just(dummySearchPhotosResponsePage1));

        // start entering query string
        homePresenter.enterSearchQuery(dummyQueryString);

        // submit
        homePresenter.submitSearchQuery(dummyQueryString);

        reset(mockView, mockSearchPhotos);

        // given that mockSearchPhotos.getStream() returns an invalid page observable
        when(mockSearchPhotos.getStream(any(SearchPhotos.RequestValues.class))).thenReturn(
                Observable.just(
                        new SearchPhotosResponse(10, 2, numItemsPerPage, numItemsPerPage * 2, Collections.<PhotoItem>emptyList())
                )
        );

        // load next page
        homePresenter.loadNextPage();

        // should update UI with error message
        verify(mockView).showCannotSearchPhotosError();
    }

    @Test
    public void cancelSearch() throws Exception {
        // start entering query string
        homePresenter.enterSearchQuery(dummyQueryString);

        // cancel search
        homePresenter.cancelSearch();

        // should update UI with correct state
        verify(mockView).updateViewState(
                HomeContract.SearchViewState.INITIAL,
                HomeContract.ContentViewState.INTRO);
    }

    @Test
    public void cancelSearch_notSearching() throws Exception {
        // cancel search
        homePresenter.cancelSearch();

        // should NOT update UI with states
        verify(mockView, never()).updateViewState(
                any(HomeContract.SearchViewState.class),
                any(HomeContract.ContentViewState.class)
        );
    }

    @Test
    public void isInSearchMode() throws Exception {
        // given that mockSearchPhotos.getStream() returns a dummy page 1 result observable
        when(mockSearchPhotos.getStream(any(SearchPhotos.RequestValues.class))).thenReturn(Observable.just(dummySearchPhotosResponsePage1));

        // initially should NOT be in search mode
        assertThat(homePresenter.isInSearchMode(), is(false));

        // start entering query string
        homePresenter.enterSearchQuery(dummyQueryString);

        // should be in search mode
        assertThat(homePresenter.isInSearchMode(), is(true));

        // submit
        homePresenter.submitSearchQuery(dummyQueryString);

        // should still be in search mode
        assertThat(homePresenter.isInSearchMode(), is(true));

        // cancel search
        homePresenter.cancelSearch();

        // should no longer be in search mode
        assertThat(homePresenter.isInSearchMode(), is(false));
    }

    @Test
    public void areAllPagesLoaded() throws Exception {
        // given that mockSearchPhotos.getStream() returns a dummy page 1 result observable
        when(mockSearchPhotos.getStream(any(SearchPhotos.RequestValues.class))).thenReturn(Observable.just(dummySearchPhotosResponsePage1));

        // start entering query string
        homePresenter.enterSearchQuery(dummyQueryString);

        // submit
        homePresenter.submitSearchQuery(dummyQueryString);

        // should NOT have loaded all pages
        assertThat(homePresenter.areAllPagesLoaded(), is(false));

        reset(mockView, mockSearchPhotos);

        // given that mockSearchPhotos.getStream() returns a dummy page 2 result observable
        when(mockSearchPhotos.getStream(any(SearchPhotos.RequestValues.class))).thenReturn(Observable.just(dummySearchPhotosResponsePage2));

        // load next page
        homePresenter.loadNextPage();

        // should have loaded all pages
        assertThat(homePresenter.areAllPagesLoaded(), is(true));
    }

    @Test
    public void isLoading() throws Exception {
        // given that mockSearchPhotos.getStream() returns a dummy page 1 result observable with some delay
        int delay = 5000;
        TestScheduler testScheduler = new TestScheduler();
        when(mockSearchPhotos.getStream(any(SearchPhotos.RequestValues.class))).thenReturn(
                Observable.just(dummySearchPhotosResponsePage1).delay(
                        delay, TimeUnit.MILLISECONDS, testScheduler
                )
        );

        // start entering query string
        homePresenter.enterSearchQuery(dummyQueryString);

        // should NOT be loading
        assertThat(homePresenter.isLoading(), is(false));

        // submit
        homePresenter.submitSearchQuery(dummyQueryString);

        // should be loading
        assertThat(homePresenter.isLoading(), is(true));

        // advance time
        testScheduler.advanceTimeBy(delay, TimeUnit.MILLISECONDS);

        // should NOT be loading
        assertThat(homePresenter.isLoading(), is(false));

        reset(mockView, mockSearchPhotos);

        // given that mockSearchPhotos.getStream() returns a dummy page 1 result observable with some delay
        when(mockSearchPhotos.getStream(any(SearchPhotos.RequestValues.class))).thenReturn(
                Observable.just(dummySearchPhotosResponsePage1).delay(
                        delay, TimeUnit.MILLISECONDS, testScheduler
                )
        );

        // load next page
        homePresenter.loadNextPage();

        // should be loading
        assertThat(homePresenter.isLoading(), is(true));

        // advance time
        testScheduler.advanceTimeBy(delay, TimeUnit.MILLISECONDS);

        // should NOT be loading
        assertThat(homePresenter.isLoading(), is(false));
    }

    @Test
    public void syncState() throws Exception {
        // given that mockSearchPhotos.getStream() returns a dummy page 1 result observable
        when(mockSearchPhotos.getStream(any(SearchPhotos.RequestValues.class))).thenReturn(Observable.just(dummySearchPhotosResponsePage1));

        // start entering query string
        homePresenter.enterSearchQuery(dummyQueryString);

        // submit
        homePresenter.submitSearchQuery(dummyQueryString);

        reset(mockView, mockSearchPhotos);

        // sync state
        homePresenter.syncState();

        // should update UI with results
        verify(mockView).showSearchResults(dummySearchPhotosResponsePage1.getPhotoItems());

        // should update UI with the current state
        verify(mockView).updateViewState(
                HomeContract.SearchViewState.SUBMITTED_QUERY,
                HomeContract.ContentViewState.RESULTS_DISPLAYED);
    }

    @Test
    public void destroy() throws Exception {
        homePresenter.destroy();

        // search filters should be cleared
        verify(spySearchFiltersViewState).reset();
    }

    @Test
    public void shouldResubmitQueryWhenSearchFiltersChangeIfDisplayingResults() {
        // given that mockSearchPhotos.getStream() returns a dummy page 1 result observable
        when(mockSearchPhotos.getStream(any(SearchPhotos.RequestValues.class))).thenReturn(Observable.just(dummySearchPhotosResponsePage1));

        // start entering query string
        homePresenter.enterSearchQuery(dummyQueryString);

        // submit
        homePresenter.submitSearchQuery(dummyQueryString);

        reset(mockSearchPhotos);

        // search filters change detected
        SearchFilters newSearchFilters = new SearchFilters(1492000000000L, 1495000000000L);
        spySearchFiltersViewState.update(newSearchFilters);

        // should get stream to re-search with current query
        verify(mockSearchPhotos).getStream(requestValuesCaptor.capture());

        // should have correct search filters in new query
        assertThat(requestValuesCaptor.getValue(), is(new SearchPhotos.RequestValues(
                1, numItemsPerPage, dummyQueryString,
                newSearchFilters.getMinUploadDateMillis(), newSearchFilters.getMaxUploadDateMillis()
        )));
    }

    @Test
    public void shouldNotResubmitQueryWhenSameSearchFiltersReceived() {
        // given that mockSearchPhotos.getStream() returns a dummy page 1 result observable
        when(mockSearchPhotos.getStream(any(SearchPhotos.RequestValues.class))).thenReturn(Observable.just(dummySearchPhotosResponsePage1));

        // start entering query string
        homePresenter.enterSearchQuery(dummyQueryString);

        // submit
        homePresenter.submitSearchQuery(dummyQueryString);

        reset(mockSearchPhotos);

        // same (default) search filters emitted
        SearchFilters newSearchFilters = new SearchFilters();
        spySearchFiltersViewState.update(newSearchFilters);

        // should NOT get stream to re-search with current query
        verify(mockSearchPhotos, never()).getStream(any(SearchPhotos.RequestValues.class));
    }

    @Test
    public void shouldNotResubmitQueryWhenSearchFiltersChangeIfDisplayingIntroMessage() {
        // search filters change detected
        SearchFilters newSearchFilters = new SearchFilters(1492000000000L, 1495000000000L);
        spySearchFiltersViewState.update(newSearchFilters);

        // should NOT get stream to re-search with current query
        verify(mockSearchPhotos, never()).getStream(any(SearchPhotos.RequestValues.class));
    }

    @Test
    public void shouldNotResubmitQueryWhenSearchFiltersChangeIfSearching() {
        // given that mockSearchPhotos.getStream() returns a dummy page 1 result observable with some delay
        when(mockSearchPhotos.getStream(any(SearchPhotos.RequestValues.class))).thenReturn(
                Observable.just(dummySearchPhotosResponsePage1).delay(5000, TimeUnit.MILLISECONDS)
        );

        // start entering query string
        homePresenter.enterSearchQuery(dummyQueryString);

        // submit
        homePresenter.submitSearchQuery(dummyQueryString);

        reset(mockSearchPhotos);

        // search filters change detected
        SearchFilters newSearchFilters = new SearchFilters(1492000000000L, 1495000000000L);
        spySearchFiltersViewState.update(newSearchFilters);

        // should NOT get stream to re-search with current query
        verify(mockSearchPhotos, never()).getStream(any(SearchPhotos.RequestValues.class));
    }
}