package io.ychescale9.flickrsearch.presentation.helper;

import org.junit.Before;
import org.junit.Test;

import java.util.concurrent.TimeUnit;

import io.reactivex.schedulers.TestScheduler;
import io.reactivex.subscribers.TestSubscriber;

/**
 * Created by yang on 24/4/17.
 * Unit tests for the implementation of {@link ViewState}
 */
public class ViewStateTest {

    ViewState<Model> viewState;

    @Before
    public void setUp() throws Exception {
        viewState = new ViewState<>();
    }

    @Test
    public void delayedEmission() throws Exception {
        int delay = 5000;
        TestScheduler testScheduler = new TestScheduler();
        TestSubscriber<Model> testSubscriber = viewState.get()
                .delay(delay, TimeUnit.MILLISECONDS, testScheduler).test();

        // emit 2 models
        Model model1 = new Model(1, 2);
        Model model2 = new Model(3, 4);
        viewState.update(model1);
        viewState.update(model2);

        // should NOT receive any items immediately after subscribing
        testSubscriber.assertNoValues();

        // advance time
        testScheduler.advanceTimeBy(delay, TimeUnit.MILLISECONDS);

        // should have received all emitted items
        testSubscriber.assertValues(model1, model2);
    }

    @Test
    public void receiveLastItemUponSubscription() throws Exception {
        // first subscriber
        TestSubscriber<Model> testSubscriber1 = viewState.get().test();

        // emits 2 models (no subscribers yet)
        Model model1 = new Model(1, 2);
        Model model2 = new Model(3, 4);
        viewState.update(model1);
        viewState.update(model2);

        // first subscriber should receive both items
        testSubscriber1.assertValues(model1, model2);

        // second subscriber
        TestSubscriber<Model> testSubscriber2 = viewState.get().test();

        // second subscriber should receive the last emitted item immediately after subscription
        testSubscriber2.assertValues(model2);
    }

    @Test
    public void multipleSubscribers() throws Exception {
        // first subscriber
        TestSubscriber<Model> testSubscriber1 = viewState.get().test();

        // emit 2 models
        Model model1 = new Model(1, 2);
        Model model2 = new Model(3, 4);
        viewState.update(model1);
        viewState.update(model2);

        // first subscriber should receive both items
        testSubscriber1.assertValues(model1, model2);

        // second subscriber
        TestSubscriber<Model> testSubscriber2 = viewState.get().test();

        // second subscriber should receive the last emitted item immediately after subscription
        testSubscriber2.assertValues(model2);

        // emit another model
        Model model3 = new Model(5, 6);
        viewState.update(model3);

        // first subscriber should receive all 3 models
        testSubscriber1.assertValues(model1, model2, model3);

        // second subscriber should receive last 2 models
        testSubscriber2.assertValues(model2, model3);
    }

    @Test
    public void unsubscribe() throws Exception {
        // first subscriber
        TestSubscriber<Model> testSubscriber1 = viewState.get().test();

        // second subscriber
        TestSubscriber<Model> testSubscriber2 = viewState.get().test();

        // emit 2 models
        Model model1 = new Model(1, 2);
        Model model2 = new Model(3, 4);
        viewState.update(model1);
        viewState.update(model2);

        // first subscriber unsubscribe
        testSubscriber1.dispose();

        // emit another model
        Model model3 = new Model(5, 6);
        viewState.update(model3);

        // first subscriber should NOT receive the new model
        testSubscriber1.assertValues(model1, model2);

        // second subscriber should receive all 3 models
        testSubscriber2.assertValues(model1, model2, model3);
    }

    @Test
    public void clear() throws Exception {
        // first subscriber
        TestSubscriber<Model> testSubscriber1 = viewState.get().test();

        // emit 2 models
        Model model1 = new Model(1, 2);
        Model model2 = new Model(3, 4);
        viewState.update(model1);
        viewState.update(model2);

        // first subscriber should have received all emitted items
        testSubscriber1.assertValues(model1, model2);

        // reset view state
        viewState.reset();

        // second subscriber
        TestSubscriber<Model> testSubscriber2 = viewState.get().test();

        // second subscriber should NOT receive any items as the flowable has been recreated
        testSubscriber2.assertNoValues();
    }

    private final class Model {
        int a;
        int b;

        Model(int a, int b) {
            this.a = a;
            this.b = b;
        }

        @Override
        public String toString() {
            return "Model{" +
                    "a=" + a +
                    ", b=" + b +
                    '}';
        }
    }

}