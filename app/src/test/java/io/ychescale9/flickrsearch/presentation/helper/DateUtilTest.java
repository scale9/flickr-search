package io.ychescale9.flickrsearch.presentation.helper;

import org.joda.time.DateTime;
import org.junit.Rule;
import org.junit.Test;

import io.ychescale9.flickrsearch.TimeZoneUtcRule;
import io.ychescale9.flickrsearch.util.Clock;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by yang on 21/4/17.
 * Unit tests for the implementation of {@link DateUtil}
 */
public class DateUtilTest {

    @Rule
    public TimeZoneUtcRule timeZoneUtcRule = new TimeZoneUtcRule();

    @Test
    public void formatDateTime() throws Exception {
        long timestamp = new DateTime(2017, 1, 1, 0, 0).getMillis();
        assertThat(DateUtil.formatDateTime(timestamp), is("January 1, 2017"));

        timestamp = new DateTime(2017, 6, 15, 0, 0).getMillis();
        assertThat(DateUtil.formatDateTime(timestamp), is("June 15, 2017"));

        timestamp = new DateTime(2017, 12, 31, 0, 0).getMillis();
        assertThat(DateUtil.formatDateTime(timestamp), is("December 31, 2017"));
    }

    @Test
    public void getDateTimeFromTimestampOrNow() {
        Clock mockClock = mock(Clock.class);
        long timestamp = 1497000000;

        assertThat(DateUtil.getDateTimeFromTimestampOrNow(timestamp, mockClock), is(new DateTime(timestamp)));

        // mock the current time
        long now = DateTime.now().getMillis();
        when(mockClock.now()).thenReturn(now);
        timestamp = 0;

        assertThat(DateUtil.getDateTimeFromTimestampOrNow(timestamp, mockClock), is(new DateTime(now)));
    }
}