package io.ychescale9.flickrsearch.data.api;

import io.ychescale9.flickrsearch.BuildConfig;

/**
 * Created by yang on 6/4/17.
 * Provides methods for getting prod api constant values
 */
public class ApiConstants extends BaseApiConstants {

    @Override
    public String getApiBaseUrl() {
        return BuildConfig.API_BASE_URL;
    }

    @Override
    public String getApiMethodSearchPhotos() {
        return BuildConfig.API_METHOD_SEARCH_PHOTOS;
    }

    @Override
    public int getConnectTimeOutMilliseconds() {
        return BuildConfig.API_CONNECT_TIMEOUT_MILLISECONDS;
    }

    @Override
    public int getReadTimeOutMilliseconds() {
        return BuildConfig.API_READ_TIMEOUT_MILLISECONDS;
    }

    @Override
    public int getWriteTimeOutMilliseconds() {
        return BuildConfig.API_WRITE_TIMEOUT_MILLISECONDS;
    }
}
