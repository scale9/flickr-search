package io.ychescale9.flickrsearch.data.api;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.ychescale9.flickrsearch.BuildConfig;
import io.ychescale9.flickrsearch.data.deserializer.SearchPhotosResponseDeserializer;
import io.ychescale9.flickrsearch.data.model.SearchPhotosResponse;
import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by yang on 6/4/17.
 */
@Module
public class ApiModule {

    @Provides
    @Singleton
    OkHttpClient provideOkHttpClient(ApiConstants apiConstants) {
        final OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.connectTimeout(apiConstants.getConnectTimeOutMilliseconds(), TimeUnit.MILLISECONDS)
                .readTimeout(apiConstants.getReadTimeOutMilliseconds(), TimeUnit.MILLISECONDS)
                .writeTimeout(apiConstants.getWriteTimeOutMilliseconds(), TimeUnit.MILLISECONDS);
        // append api key to every requests
        builder.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request original = chain.request();
                HttpUrl originalHttpUrl = original.url();

                // add common query parameters for all requests
                HttpUrl url = originalHttpUrl.newBuilder()
                        // add api key
                        .addQueryParameter("api_key", BuildConfig.API_KEY)
                        // add api response format
                        .addQueryParameter("format", BuildConfig.API_RESPONSE_FORMAT)
                        // don't include top-level jsonFlickrApi(...) in response
                        .addQueryParameter("nojsoncallback", "1")
                        .build();

                Request.Builder requestBuilder = original.newBuilder()
                        .url(url);

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });
        // add logging interceptor
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BASIC);
        builder.addInterceptor(loggingInterceptor);
        return builder.build();
    }

    @Provides
    @Singleton
    Retrofit provideRetrofit(OkHttpClient client,
                             ApiConstants apiConstants,
                             SearchPhotosResponseDeserializer searchPhotosResponseDeserializer) {
        // create Gson object using custom JsonDeserializers
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(SearchPhotosResponse.class, searchPhotosResponseDeserializer);
        Gson gson = gsonBuilder.create();

        return new Retrofit.Builder()
                .baseUrl(apiConstants.getApiBaseUrl())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
    }

    @Provides
    @Singleton
    FlickrSearchService provideFlickrSearchService(Retrofit retrofit) {
        return retrofit.create(FlickrSearchService.class);
    }
}
