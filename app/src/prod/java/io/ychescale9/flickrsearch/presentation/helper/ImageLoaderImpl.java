package io.ychescale9.flickrsearch.presentation.helper;

import android.content.Context;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

/**
 * Created by yang on 8/4/17.
 * Real implementation of {@link ImageLoader} with Picasso
 */
public class ImageLoaderImpl implements ImageLoader {

    @Override
    public void loadPhotoThumbnail(@NonNull Context context,
                                   int itemIndex,
                                   @NonNull String url,
                                   @NonNull ImageView target,
                                   @DrawableRes int placeholderResId) {
        Picasso.with(context).load(url)
                .fit()
                .placeholder(placeholderResId)
                .into(target);
    }
}
