package io.ychescale9.flickrsearch;

import android.util.Log;

import timber.log.Timber;

/**
 * Created by yang on 6/4/17.
 * An implementation of {@link Timber.Tree} for release builds.
 */
public class ReleaseTree extends Timber.Tree {

    @Override
    protected boolean isLoggable(String tag, int priority) {
        // only log WARN, ERROR, and WTF
        return priority == Log.WARN || priority == Log.ERROR || priority == Log.ASSERT;
    }

    @Override
    protected void log(int priority, String tag, String message, Throwable t) {
        // TODO: log exceptions with crash reporting library
    }
}
