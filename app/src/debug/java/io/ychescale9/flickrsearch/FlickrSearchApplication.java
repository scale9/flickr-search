package io.ychescale9.flickrsearch;

import timber.log.Timber;

/**
 * Created by yang on 6/4/17.
 */
public class FlickrSearchApplication extends BaseFlickrSearchApplication {

    @Override
    public void onCreate() {
        super.onCreate();

        // install the debug tree
        Timber.plant(new DebugTree());
    }
}
