package io.ychescale9.flickrsearch;

import timber.log.Timber;

/**
 * Created by yang on 6/4/17.
 * Custom Timber debug tree with line number in the tag.
 */
public class DebugTree extends Timber.DebugTree {

    @Override
    protected String createStackElementTag(StackTraceElement element) {
        return super.createStackElementTag(element) + ":" + element.getLineNumber();
    }
}
