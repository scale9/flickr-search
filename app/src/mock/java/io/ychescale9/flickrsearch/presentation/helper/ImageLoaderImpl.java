package io.ychescale9.flickrsearch.presentation.helper;

import android.content.Context;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.widget.ImageView;

import io.ychescale9.flickrsearch.R;

/**
 * Created by yang on 8/4/17.
 * Mock implementation of {@link ImageLoader} using local drawable resources.
 */
public class ImageLoaderImpl implements ImageLoader {

    @Override
    public void loadPhotoThumbnail(@NonNull Context context,
                                   int itemIndex,
                                   @NonNull String url,
                                   @NonNull ImageView target,
                                   @DrawableRes int placeholderResId) {
        int colorResId;
        if (itemIndex % 2 == 0) {
            colorResId = R.color.dummyPhoto1;
        } else {
            colorResId = R.color.dummyPhoto2;
        }
        target.setImageResource(colorResId);
    }
}
