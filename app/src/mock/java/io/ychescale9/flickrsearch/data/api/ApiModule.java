package io.ychescale9.flickrsearch.data.api;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.ychescale9.flickrsearch.data.deserializer.SearchPhotosResponseDeserializer;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.mock.BehaviorDelegate;
import retrofit2.mock.MockRetrofit;
import retrofit2.mock.NetworkBehavior;

/**
 * Created by yang on 6/4/17.
 */
@Module
public class ApiModule {

    @Provides
    @Singleton
    Retrofit provideRetrofit() {
        return new Retrofit.Builder()
                .baseUrl("http://example.com")
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
    }

    @Provides
    @Singleton
    FlickrSearchService provideFlickrSearchService(Retrofit retrofit,
                                                   SearchPhotosResponseDeserializer searchPhotosResponseDeserializer) {
        NetworkBehavior behavior = NetworkBehavior.create();
        // 1 second delay
        behavior.setDelay(1000, TimeUnit.MILLISECONDS);
        // no failure
        behavior.setFailurePercent(0);
        MockRetrofit mockRetrofit = new MockRetrofit.Builder(retrofit)
                .networkBehavior(behavior)
                .build();
        BehaviorDelegate<FlickrSearchService> delegate = mockRetrofit.create(FlickrSearchService.class);

        return new MockFlickrSearchService(delegate, searchPhotosResponseDeserializer);
    }
}
