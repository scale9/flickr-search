package io.ychescale9.flickrsearch.data.api;

/**
 * Created by yang on 6/4/17.
 * Provides methods for getting mock api constant values
 */
public class ApiConstants extends BaseApiConstants {

    @Override
    public String getApiBaseUrl() {
        return "http://example.com";
    }

    @Override
    public String getApiMethodSearchPhotos() {
        return "mock.photos.search";
    }

    @Override
    public String getPhotoThumbnailUrlFormat() {
        return "http://localhost/flickrsearch/%s/%s_%s_q.jpg";
    }

    @Override
    public String getPhotoPageUrlFormat() {
        return "http://localhost/flickrsearch/photos/%s/%s";
    }

    @Override
    public int getConnectTimeOutMilliseconds() {
        return 0;
    }

    @Override
    public int getReadTimeOutMilliseconds() {
        return 0;
    }

    @Override
    public int getWriteTimeOutMilliseconds() {
        return 0;
    }
}
