package io.ychescale9.flickrsearch.data.api;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import io.reactivex.Observable;
import io.ychescale9.flickrsearch.data.MockData;
import io.ychescale9.flickrsearch.data.deserializer.SearchPhotosResponseDeserializer;
import io.ychescale9.flickrsearch.data.model.SearchPhotosResponse;
import retrofit2.http.Query;
import retrofit2.mock.BehaviorDelegate;
import timber.log.Timber;

/**
 * Created by yang on 6/4/17.
 */
public class MockFlickrSearchService implements FlickrSearchService {

    private final BehaviorDelegate<FlickrSearchService> delegate;

    private final SearchPhotosResponseDeserializer searchPhotosResponseDeserializer;

    public MockFlickrSearchService(@NonNull BehaviorDelegate<FlickrSearchService> delegate,
                                   @NonNull SearchPhotosResponseDeserializer searchPhotosResponseDeserializer) {
        this.delegate = delegate;
        this.searchPhotosResponseDeserializer = searchPhotosResponseDeserializer;
    }

    @Override
    public Observable<SearchPhotosResponse> searchPhotos(
            @Query("method") @NonNull String method,
            @Query("page") int pageNumber,
            @Query("per_page") int itemsPerPage,
            @Query("text") @NonNull  String text,
            @Query("min_upload_date") @Nullable Long minUploadDate,
            @Query("max_upload_date") @Nullable Long maxUploadDate) {
            // create Gson object using custom JsonDeserializers
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(SearchPhotosResponse.class, searchPhotosResponseDeserializer);
        Gson gson = gsonBuilder.create();
        // return mock data for the corresponding page
        String mockPhotoResponse = pageNumber == 1 ? MockData.MOCK_PHOTOS_RESPONSE_PAGE_1_JSON : MockData.MOCK_PHOTOS_RESPONSE_PAGE_2_JSON;
        // convert the mock data from json to SearchPhotosResponse
        SearchPhotosResponse searchPhotosResponse = gson.fromJson(mockPhotoResponse, SearchPhotosResponse.class);

        Timber.d("Generated mock PhotoItems.");

        return delegate.returningResponse(searchPhotosResponse).searchPhotos(
                method, itemsPerPage, pageNumber, text, minUploadDate, maxUploadDate);
    }
}
