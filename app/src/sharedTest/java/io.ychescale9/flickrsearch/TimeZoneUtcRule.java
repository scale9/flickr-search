package io.ychescale9.flickrsearch;

import org.joda.time.DateTimeZone;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

/**
 * Created by yang on 11/12/16.
 * A test rule for overriding timezone with UTC temporarily
 * Note: this only works with joda time.
 */
public class TimeZoneUtcRule extends TestWatcher {

    private DateTimeZone originalDefaultDateTimeZone = DateTimeZone.getDefault();

    @Override
    protected void starting( Description description ) {
        DateTimeZone.setDefault(DateTimeZone.UTC);
    }

    @Override
    protected void finished( Description description ) {
        DateTimeZone.setDefault(originalDefaultDateTimeZone);
    }
}
