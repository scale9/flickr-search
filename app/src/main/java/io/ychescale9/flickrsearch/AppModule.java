package io.ychescale9.flickrsearch;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.ychescale9.flickrsearch.data.api.ApiConstants;
import io.ychescale9.flickrsearch.presentation.helper.ImageLoader;
import io.ychescale9.flickrsearch.presentation.helper.ImageLoaderImpl;
import io.ychescale9.flickrsearch.presentation.helper.PhotoUrlBuilder;
import io.ychescale9.flickrsearch.presentation.helper.PhotoUrlBuilderImpl;
import io.ychescale9.flickrsearch.util.Clock;
import io.ychescale9.flickrsearch.util.RealClock;
import io.ychescale9.flickrsearch.util.SchedulerProvider;

/**
 * Created by yang on 6/4/17.
 */
@Module
public class AppModule {

    @Provides
    @Singleton
    SchedulerProvider provideSchedulerProvider() {
        return new SchedulerProvider();
    }

    @Provides
    @Singleton
    ImageLoader provideImageLoader() {
        return new ImageLoaderImpl();
    }

    @Provides
    @Singleton
    PhotoUrlBuilder providePhotoUrlBuilder(ApiConstants apiConstants) {
        return new PhotoUrlBuilderImpl(
                apiConstants.getPhotoThumbnailUrlFormat(),
                apiConstants.getPhotoPageUrlFormat()
        );
    }

    @Provides
    @Singleton
    Clock provideClock() {
        return new RealClock();
    }
}
