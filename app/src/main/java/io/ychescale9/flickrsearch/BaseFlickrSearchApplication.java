package io.ychescale9.flickrsearch;

import android.app.Activity;
import android.app.Application;

import com.squareup.leakcanary.LeakCanary;

import net.danlew.android.joda.JodaTimeAndroid;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasActivityInjector;

/**
 * Created by yang on 6/4/17.
 */
public abstract class BaseFlickrSearchApplication extends Application implements HasActivityInjector {

    private AppComponent component;

    @Inject
    DispatchingAndroidInjector<Activity> dispatchingActivityInjector;

    @Override
    public void onCreate() {
        super.onCreate();
        // create and inject app component
        component = createComponent();
        component.inject(this);

        // initialize JodaTimeAndroid
        JodaTimeAndroid.init(this);

        // initialize leak detection
        LeakCanary.install(this);
    }

    @Override
    public AndroidInjector<Activity> activityInjector() {
        return dispatchingActivityInjector;
    }

    /**
     * Setup app-level Dagger component.
     */
    protected AppComponent createComponent() {
        return DaggerAppComponent.builder()
                .app(this)
                .build();
    }

    public AppComponent getComponent() {
        return component;
    }
}
