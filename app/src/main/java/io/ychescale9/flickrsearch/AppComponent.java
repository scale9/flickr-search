package io.ychescale9.flickrsearch;

import android.app.Application;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import io.ychescale9.flickrsearch.data.DataModule;
import io.ychescale9.flickrsearch.data.api.ApiModule;
import io.ychescale9.flickrsearch.data.repository.RepositoryModule;
import io.ychescale9.flickrsearch.presentation.ViewStateModule;
import io.ychescale9.flickrsearch.presentation.base.ActivityModule;

/**
 * Created by yang on 6/4/17.
 */
@Singleton
@Component(
        modules = {
                AppModule.class,
                ViewStateModule.class,
                ConstantsModule.class,
                DataModule.class,
                RepositoryModule.class,
                ApiModule.class,
                ActivityModule.class, // all activity sub-components
        }
)
public interface AppComponent {

    void inject(BaseFlickrSearchApplication application);

    @Component.Builder
    interface Builder {

        @BindsInstance
        Builder app(Application application);

        AppComponent build();
    }
}
