package io.ychescale9.flickrsearch.util;

import org.joda.time.DateTime;

/**
 * Created by yang on 21/4/17.
 */
public class RealClock implements Clock {

    @Override
    public long now() {
        return DateTime.now().getMillis();
    }
}
