package io.ychescale9.flickrsearch.util;

import android.support.annotation.Keep;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.ObservableTransformer;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by yang on 6/4/17.
 */
public class SchedulerProvider {

    @Keep
    public <T> ObservableTransformer<T, T> applySchedulers() {
        return new ObservableTransformer<T, T>() {
            @Override
            public ObservableSource<T> apply(Observable<T> upstream) {
                return upstream.subscribeOn(io())
                        .observeOn(ui());
            }
        };
    }

    /**
     * Subscribe on the Android main thread
     * @return
     */
    public Scheduler ui() {
        return AndroidSchedulers.mainThread();
    }

    /**
     * Subscribe on the io thread
     * @return
     */
    public Scheduler io() {
        return Schedulers.io();
    }

    public Scheduler computation() {
        return Schedulers.computation();
    }
}
