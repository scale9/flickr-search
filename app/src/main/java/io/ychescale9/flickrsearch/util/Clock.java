package io.ychescale9.flickrsearch.util;

/**
 * Created by yang on 21/4/17.
 */
public interface Clock {

    /**
     * Returns the current time in milliseconds UTC.
     * @return
     */
    long now();
}
