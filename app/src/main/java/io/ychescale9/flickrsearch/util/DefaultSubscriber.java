package io.ychescale9.flickrsearch.util;

import io.reactivex.subscribers.DisposableSubscriber;

/**
 * Created by yang on 25/4/17.
 * Helper class to be extended from if not all of the 3 methods need to be implemented.
 */
public class DefaultSubscriber<T> extends DisposableSubscriber<T> {

    @Override public void onNext(T t) {

    }

    @Override public void onError(Throwable e) {

    }

    @Override public void onComplete() {

    }
}