package io.ychescale9.flickrsearch.util;

import io.reactivex.observers.DisposableObserver;

/**
 * Created by yang on 7/4/17.
 * Helper class to be extended from if not all of the 3 methods need to be implemented.
 */
public class DefaultObserver<T> extends DisposableObserver<T> {

    @Override public void onNext(T t) {

    }

    @Override public void onError(Throwable e) {

    }

    @Override public void onComplete() {

    }
}