package io.ychescale9.flickrsearch.presentation.helper;

import android.support.annotation.NonNull;

/**
 * Created by yang on 8/4/17.
 * Real implementation of {@link PhotoUrlBuilder}
 */
public class PhotoUrlBuilderImpl implements PhotoUrlBuilder {

    private final String thumbnailUrlFormat;
    private final String originalUrlFormat;

    public PhotoUrlBuilderImpl(String thumbnailUrlFormat, String originalUrlFormat) {
        this.thumbnailUrlFormat = thumbnailUrlFormat;
        this.originalUrlFormat = originalUrlFormat;
    }

    @Override
    public String buildThumbnailUrl(int farmId, @NonNull String serverId, @NonNull String photoId, @NonNull String secret) {
        // build thumbnail url from farm, server, id and secret
        return String.format(thumbnailUrlFormat, farmId, serverId, photoId, secret);
    }

    @Override
    public String buildPageUrl(@NonNull String userId, @NonNull String photoId) {
        // build original url from farm, server, id and secret
        return String.format(originalUrlFormat, userId, photoId);
    }
}
