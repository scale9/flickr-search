package io.ychescale9.flickrsearch.presentation.searchfilters;

import dagger.Module;
import dagger.Provides;
import io.ychescale9.flickrsearch.presentation.helper.PresenterManager;
import io.ychescale9.flickrsearch.presentation.helper.ViewState;
import io.ychescale9.flickrsearch.util.SchedulerProvider;

/**
 * Created by yang on 18/4/17.
 */
@Module
public class SearchFiltersModule {

    @Provides
    SearchFiltersContract.View provideSearchFiltersView(SearchFiltersActivity activity) {
        return activity;
    }

    @Provides
    SearchFiltersContract.Presenter provideSearchFiltersPresenter(SearchFiltersContract.View searchFiltersView,
                                                                  ViewState<SearchFilters> searchFiltersViewState,
                                                                  SchedulerProvider schedulerProvider) {
        SearchFiltersContract.Presenter presenter;
        presenter = (SearchFiltersContract.Presenter) PresenterManager.getInstance().restorePresenter(searchFiltersView.getClass());
        if (presenter == null) {
            presenter = new SearchFiltersPresenter(
                    searchFiltersView,
                    searchFiltersViewState,
                    schedulerProvider
            );
        } else {
            presenter.setView(searchFiltersView);
        }
        return presenter;
    }
}
