package io.ychescale9.flickrsearch.presentation;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.ychescale9.flickrsearch.presentation.helper.ViewState;
import io.ychescale9.flickrsearch.presentation.searchfilters.SearchFilters;

/**
 * Created by yang on 17/4/17.
 * Application-level reactive view states.
 */
@Module
public class ViewStateModule {

    @Provides
    @Singleton
    ViewState<SearchFilters> provideSearchFiltersViewState() {
        return new ViewState<>();
    }
}
