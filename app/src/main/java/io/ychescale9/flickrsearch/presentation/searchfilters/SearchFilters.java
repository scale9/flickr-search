package io.ychescale9.flickrsearch.presentation.searchfilters;

/**
 * Created by yang on 18/4/17.
 * Model class representing the search filters.
 */
public class SearchFilters {

    private long minUploadDateMillis;
    private long maxUploadDateMillis;

    public SearchFilters() {
        this.minUploadDateMillis = 0;
        this.maxUploadDateMillis = 0;
    }

    public SearchFilters(long minUploadDateMillis, long maxUploadDate) {
        this.minUploadDateMillis = minUploadDateMillis;
        this.maxUploadDateMillis = maxUploadDate;
    }

    public long getMinUploadDateMillis() {
        return minUploadDateMillis;
    }

    public void setMinUploadDateMillis(long minUploadDateMillis) {
        this.minUploadDateMillis = minUploadDateMillis;
    }

    public long getMaxUploadDateMillis() {
        return maxUploadDateMillis;
    }

    public void setMaxUploadDateMillis(long maxUploadDateMillis) {
        this.maxUploadDateMillis = maxUploadDateMillis;
    }

    public void reset() {
        minUploadDateMillis = 0;
        maxUploadDateMillis = 0;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SearchFilters that = (SearchFilters) o;

        if (minUploadDateMillis != that.minUploadDateMillis) return false;
        return maxUploadDateMillis == that.maxUploadDateMillis;

    }

    @Override
    public int hashCode() {
        int result = (int) (minUploadDateMillis ^ (minUploadDateMillis >>> 32));
        result = 31 * result + (int) (maxUploadDateMillis ^ (maxUploadDateMillis >>> 32));
        return result;
    }

    @Override
    public String toString() {
        return "SearchFilters{" +
                "minUploadDateMillis=" + minUploadDateMillis +
                ", maxUploadDateMillis=" + maxUploadDateMillis +
                '}';
    }
}
