package io.ychescale9.flickrsearch.presentation.helper;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.support.annotation.ColorInt;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.graphics.drawable.VectorDrawableCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

public final class UiUtils {

    /**
     * Programmatically close soft keyboard
     * @param activity
     * @param focusedView
     */
	public static void hideKeyboard(Activity activity, View focusedView) {
        InputMethodManager inputManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow(focusedView.getWindowToken(), 0);
    }

    /**
     * Apply tinting to a vector drawable
     * @param context
     * @param resId
     * @param tint
     * @return
     */
    public static Drawable tintVectorDrawable(@NonNull Context context,
                                                    @DrawableRes int resId,
                                                    @Nullable Resources.Theme theme,
                                                    @ColorInt int tint) {
        Drawable drawable = VectorDrawableCompat.create(context.getResources(), resId, theme);
        assert drawable != null;
        final Drawable wrapped = DrawableCompat.wrap(drawable);
        drawable.mutate();
        DrawableCompat.setTint(wrapped, tint);
        return drawable;
    }
}
