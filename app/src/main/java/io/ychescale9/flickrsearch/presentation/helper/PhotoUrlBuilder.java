package io.ychescale9.flickrsearch.presentation.helper;

import android.support.annotation.NonNull;

/**
 * Created by yang on 8/4/17.
 */
public interface PhotoUrlBuilder {

    String buildThumbnailUrl(int farmId,
                 @NonNull String serverId,
                 @NonNull String id,
                 @NonNull String secret);

    String buildPageUrl(@NonNull String userId,
                        @NonNull String photoId);
}
