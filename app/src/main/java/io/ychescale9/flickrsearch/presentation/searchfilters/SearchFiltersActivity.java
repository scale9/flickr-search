package io.ychescale9.flickrsearch.presentation.searchfilters;

import android.app.DatePickerDialog;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TextView;

import org.joda.time.DateTime;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.ychescale9.flickrsearch.R;
import io.ychescale9.flickrsearch.presentation.base.BaseActivity;
import io.ychescale9.flickrsearch.presentation.helper.DateUtil;
import io.ychescale9.flickrsearch.presentation.helper.UiUtils;
import io.ychescale9.flickrsearch.util.Clock;
import timber.log.Timber;

/**
 * Created by yang on 18/4/17.
 */
public class SearchFiltersActivity extends BaseActivity implements SearchFiltersContract.View {

    @Inject
    SearchFiltersContract.Presenter presenter;

    @Inject
    Clock clock;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.text_view_min_upload_date)
    TextView minUploadDateTextView;

    @BindView(R.id.text_view_max_upload_date)
    TextView maxUploadDateTextView;

    private DatePickerDialog minUploadDateDialog;
    private DatePickerDialog maxUploadDateDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_search_filters);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);

        // tint and set navigation button icon
        Drawable drawable = UiUtils.tintVectorDrawable(
                this,
                R.drawable.ic_close_black_24dp,
                getTheme(),
                ContextCompat.getColor(this, R.color.colorAccent)
        );
        toolbar.setNavigationIcon(drawable);

        // navigate back when user clicks on the navigation button
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        // make sure views are in correct state after configuration changes
        presenter.syncState();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.search_filters, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_clear_filters:
                // reset the filters to default values
                presenter.clearFilters();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (minUploadDateDialog != null) {
            minUploadDateDialog.dismiss();
        }
        if (maxUploadDateDialog != null) {
            maxUploadDateDialog.dismiss();
        }
    }

    @Override
    public SearchFiltersContract.Presenter getPresenter() {
        return presenter;
    }
    
    @OnClick(R.id.button_apply_filters)
    public void applyFiltersButtonOnClick() {
        // apply filters and finish the activity
        presenter.applyFilters();
        finish();
    }

    @OnClick(R.id.button_min_upload_date_filter)
    public void minUploadDateFilterButtonOnClick() {
        presenter.startMinUploadDateSelection();
    }

    @OnClick(R.id.button_max_upload_date_filter)
    public void maxUploadDateFilterButtonOnClick() {
        presenter.startMaxUploadDateSelection();
    }

    @Override
    public void showMinUploadDateSelectionScreen(long selectedMinUploadDate, long maxDateAllowed) {
        DateTime selectedDate = DateUtil.getDateTimeFromTimestampOrNow(selectedMinUploadDate, clock);
        minUploadDateDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                Timber.d("Min upload date selected");
                // month in DatePicker starts at 0
                presenter.setMinUploadDate(new DateTime(year, month + 1, dayOfMonth, 0, 0).getMillis());
            }
        }, selectedDate.getYear(), selectedDate.getMonthOfYear() - 1, selectedDate.getDayOfMonth());

        // update the max date allowed for the min upload date picker
        if (maxDateAllowed > 0) {
            minUploadDateDialog.getDatePicker().setMaxDate(maxDateAllowed);
        }

        minUploadDateDialog.show();
    }

    @Override
    public void showMaxUploadDateSelectionScreen(long selectedMaxUploadDate, long minDateAllowed) {
        DateTime selectedDate = DateUtil.getDateTimeFromTimestampOrNow(selectedMaxUploadDate, clock);
        maxUploadDateDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                Timber.d("Max upload date selected");
                // month in DatePicker starts at 0
                presenter.setMaxUploadDate(new DateTime(year, month + 1, dayOfMonth, 0, 0).getMillis());
            }
        }, selectedDate.getYear(), selectedDate.getMonthOfYear() - 1, selectedDate.getDayOfMonth());

        // update the min date allowed for the max upload date picker
        if (minDateAllowed > 0) {
            maxUploadDateDialog.getDatePicker().setMinDate(minDateAllowed);
        }

        maxUploadDateDialog.show();
    }

    @Override
    public void showSelectedMinUploadDate(long minUploadDate) {
        // display formatted min upload date
        String formattedDate = DateUtil.formatDateTime(minUploadDate);
        minUploadDateTextView.setText(formattedDate);
    }

    @Override
    public void showSelectedMaxUploadDate(long maxUploadDate) {
        // display formatted max upload date
        String formattedDate = DateUtil.formatDateTime(maxUploadDate);
        maxUploadDateTextView.setText(formattedDate);
    }

    @Override
    public void showDefaultMinUploadDate() {
        minUploadDateTextView.setText(getString(R.string.prompt_message_min_upload_date_filter));
    }

    @Override
    public void showDefaultMaxUploadDate() {
        maxUploadDateTextView.setText(getString(R.string.prompt_message_max_upload_date_filter));
    }
}
