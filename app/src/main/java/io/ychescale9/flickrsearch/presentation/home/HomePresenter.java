package io.ychescale9.flickrsearch.presentation.home;

import android.support.annotation.NonNull;

import java.util.List;

import io.reactivex.disposables.CompositeDisposable;
import io.ychescale9.flickrsearch.data.helper.PaginationManager;
import io.ychescale9.flickrsearch.data.model.PhotoItem;
import io.ychescale9.flickrsearch.data.model.SearchPhotosResponse;
import io.ychescale9.flickrsearch.domain.SearchPhotos;
import io.ychescale9.flickrsearch.presentation.helper.ViewState;
import io.ychescale9.flickrsearch.presentation.searchfilters.SearchFilters;
import io.ychescale9.flickrsearch.util.DefaultObserver;
import io.ychescale9.flickrsearch.util.DefaultSubscriber;
import io.ychescale9.flickrsearch.util.SchedulerProvider;
import timber.log.Timber;

/**
 * Created by yang on 6/4/17.
 */
public class HomePresenter implements HomeContract.Presenter, PaginationManager.PaginationListener<PhotoItem> {

    private HomeContract.View view;

    @NonNull
    private final SearchPhotos searchPhotos;

    @NonNull
    private final PaginationManager<PhotoItem> paginationManager;

    private final int numberOfItemsPerPage;

    @NonNull
    private final ViewState<SearchFilters> searchFiltersViewState;

    @NonNull
    private SearchFilters currentSearchFilters;

    @NonNull
    private HomeContract.SearchViewState searchViewState = HomeContract.SearchViewState.INITIAL;

    @NonNull
    private HomeContract.ContentViewState contentViewState = HomeContract.ContentViewState.INTRO;

    private int loadingCount = 0;

    private final CompositeDisposable compositeDisposable = new CompositeDisposable();

    HomePresenter(@NonNull HomeContract.View view,
                  @NonNull SearchPhotos searchPhotos,
                  @NonNull PaginationManager<PhotoItem> paginationManager,
                  int numberOfItemsPerPage,
                  @NonNull ViewState<SearchFilters> searchFiltersViewState,
                  @NonNull SchedulerProvider schedulerProvider) {
        this.view = view;
        this.searchPhotos = searchPhotos;
        this.paginationManager = paginationManager;
        this.paginationManager.setPaginationListener(this);
        this.numberOfItemsPerPage = numberOfItemsPerPage;
        this.searchFiltersViewState = searchFiltersViewState;

        // initialize default current search filters
        currentSearchFilters = new SearchFilters();

        // subscribe to state changes in view state
        compositeDisposable.addAll(searchFiltersViewState.get()
                .observeOn(schedulerProvider.ui())
                .subscribeWith(new SearchFiltersChangeSubscriber())
        );
    }

    @Override
    public void enterSearchQuery(@NonNull String queryString) {
        // update search view state to ENTERING_QUERY
        if (searchViewState != HomeContract.SearchViewState.ENTERING_QUERY) {
            Timber.d("searchViewState -> ENTERING_QUERY");
            searchViewState = HomeContract.SearchViewState.ENTERING_QUERY;
            updateViewStates();
        }
    }

    @Override
    public void submitSearchQuery(@NonNull String queryString) {
        // submits the search query if query string is not blank
        if (!queryString.trim().isEmpty()) {
            // initialize pagination manager
            paginationManager.initialize(queryString);

            // update search view state to SUBMITTED_QUERY and content view state to SEARCHING
            if (searchViewState != HomeContract.SearchViewState.SUBMITTED_QUERY ||
                    contentViewState != HomeContract.ContentViewState.SEARCHING) {
                Timber.d("searchViewState -> SUBMITTED_QUERY");
                searchViewState = HomeContract.SearchViewState.SUBMITTED_QUERY;
                Timber.d("contentViewState -> SEARCHING");
                contentViewState = HomeContract.ContentViewState.SEARCHING;
                updateViewStates();
            }

            // search for the first page of results
            SearchPhotos.RequestValues requestValues = new SearchPhotos.RequestValues(
                    1, // start with the first page
                    numberOfItemsPerPage, // number of items per page
                    queryString,
                    currentSearchFilters.getMinUploadDateMillis(),
                    currentSearchFilters.getMaxUploadDateMillis()
            );

            compositeDisposable.add(
                    searchPhotos.getStream(requestValues)
                            .subscribeWith(new SearchPhotosObserver())
            );

            // increment loading ref counter
            loadingCount++;
        }
    }

    @Override
    public void loadNextPage() {
        SearchPhotos.RequestValues requestValues = new SearchPhotos.RequestValues(
                paginationManager.getLatestPage() + 1, // next page
                numberOfItemsPerPage, // number of items per page
                paginationManager.getQueryString(),
                currentSearchFilters.getMinUploadDateMillis(),
                currentSearchFilters.getMaxUploadDateMillis()
        );

        // increment loading ref counter
        loadingCount++;

        compositeDisposable.add(
                searchPhotos.getStream(requestValues)
                        .subscribeWith(new SearchPhotosObserver())
        );
    }

    @Override
    public void cancelSearch() {
        // reset search results
        paginationManager.clear();

        // reset loading ref counter
        loadingCount = 0;

        // update search view state to INITIAL and content view state to INTRO
        if (searchViewState != HomeContract.SearchViewState.INITIAL ||
                contentViewState != HomeContract.ContentViewState.INTRO) {
            Timber.d("searchViewState -> INITIAL");
            searchViewState = HomeContract.SearchViewState.INITIAL;
            Timber.d("contentViewState -> INTRO");
            contentViewState = HomeContract.ContentViewState.INTRO;
            updateViewStates();
        }
    }

    @Override
    public boolean isInSearchMode() {
        return searchViewState != HomeContract.SearchViewState.INITIAL;
    }

    @Override
    public boolean areAllPagesLoaded() {
        return paginationManager.getTotalNumPages() == 0
                || paginationManager.getLatestPage() == paginationManager.getTotalNumPages();
    }

    @Override
    public boolean isLoading() {
        return loadingCount > 0;
    }

    @Override
    public void setView(HomeContract.View view) {
        this.view = view;
    }

    @Override
    public void syncState() {
        // update the view with cached search results so we don't need to make another network call
        if (!paginationManager.getDataItems().isEmpty()) {
            view.showSearchResults(paginationManager.getDataItems());
        }

        updateViewStates();
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void destroy() {
        compositeDisposable.clear();

        // reset search filters
        searchFiltersViewState.reset();

        view = null;
    }

    @Override
    public void onNewPageAdded(@NonNull List<PhotoItem> addedItems) {
        // only append new results if user has NOT canceled search
        if (searchViewState != HomeContract.SearchViewState.INITIAL) {
            Timber.d("Adding new page of results to view");

            if (paginationManager.getLatestPage() == 1) {
                // show first page of search results
                view.showSearchResults(addedItems);
            } else {
                // show more search results
                view.showMoreSearchResults(addedItems);
            }
        }
    }

    @Override
    public void onInvalidNewPage() {
        Timber.e("Invalid page has been loaded. Not appending to search results.");
        // only show error if user has NOT canceled search
        if (searchViewState != HomeContract.SearchViewState.INITIAL) {
            view.showCannotSearchPhotosError();
        }
    }

    /**
     * Inform the UI the change of view states
     */
    private void updateViewStates() {
        view.updateViewState(searchViewState, contentViewState);
    }

    private final class SearchPhotosObserver extends DefaultObserver<SearchPhotosResponse> {

        @Override
        public void onNext(SearchPhotosResponse response) {
            // only render results if user has NOT canceled search
            if (searchViewState != HomeContract.SearchViewState.INITIAL) {
                // append results to pagination manager
                paginationManager.appendPage(
                        response.getPageNumber(),
                        response.getItemsPerPage(),
                        response.getTotalNumPages(),
                        response.getPhotoItems());

                // decrement loading ref counter
                loadingCount--;

                // update content view state to RESULTS_DISPLAYED
                if (contentViewState != HomeContract.ContentViewState.RESULTS_DISPLAYED) {
                    Timber.d("contentViewState -> RESULTS_DISPLAYED");
                    contentViewState = HomeContract.ContentViewState.RESULTS_DISPLAYED;
                    updateViewStates();
                }
            }
        }

        @Override
        public void onError(Throwable e) {
            Timber.w(e, "Error searching for photos.");
            // only show error if user has NOT canceled search
            if (searchViewState != HomeContract.SearchViewState.INITIAL) {
                // decrement loading ref counter
                loadingCount--;

                view.showCannotSearchPhotosError();

                // update content view state to RESULTS_DISPLAYED
                if (contentViewState != HomeContract.ContentViewState.RESULTS_DISPLAYED) {
                    Timber.d("contentViewState -> RESULTS_DISPLAYED");
                    contentViewState = HomeContract.ContentViewState.RESULTS_DISPLAYED;
                    updateViewStates();
                }
            }
        }
    }

    private final class SearchFiltersChangeSubscriber extends DefaultSubscriber<SearchFilters> {
        @Override
        public void onNext(SearchFilters searchFilters) {
            if (!currentSearchFilters.equals(searchFilters)) {
                Timber.d("SearchFilters change detected.");
                currentSearchFilters = searchFilters;
                if (contentViewState == HomeContract.ContentViewState.RESULTS_DISPLAYED) {
                    // re-search with current query
                    submitSearchQuery(paginationManager.getQueryString());
                }
            }
        }
    }
}
