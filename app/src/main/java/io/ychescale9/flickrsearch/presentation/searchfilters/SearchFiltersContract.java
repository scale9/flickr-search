package io.ychescale9.flickrsearch.presentation.searchfilters;

import io.ychescale9.flickrsearch.presentation.base.IPresenter;

/**
 * * Created by yang on 18/4/17.
 */
public interface SearchFiltersContract {

    interface View {

        void showMinUploadDateSelectionScreen(long selectedMinUploadDate, long maxDateAllowed);

        void showMaxUploadDateSelectionScreen(long selectedMaxUploadDate, long minDateAllowed);

        void showSelectedMinUploadDate(long selectedMinUploadDate);

        void showSelectedMaxUploadDate(long selectedMaxUploadDate);

        void showDefaultMinUploadDate();

        void showDefaultMaxUploadDate();
    }

    interface Presenter extends IPresenter<View> {

        void startMinUploadDateSelection();

        void startMaxUploadDateSelection();

        void setMinUploadDate(long minUploadDate);

        void setMaxUploadDate(long maxUploadDate);

        void applyFilters();

        void clearFilters();
    }
}
