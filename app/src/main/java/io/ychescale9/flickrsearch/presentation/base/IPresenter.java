package io.ychescale9.flickrsearch.presentation.base;

/**
 * Created by yang on 6/4/17.
 */
public interface IPresenter<T> {

    /**
     * Call this to update view instance associated with a presenter on orientation change.
     */
    void setView(T view);

    /**
     * Call this to synchronize the view state with the presenter e.g. after orientation change.
     */
    void syncState();

    /**
     * Call this in the view's (Activity or Fragment) onPause() method.
     */
    void pause();

    /**
     * Call this in the view's (Activity or Fragment) onResume() method.
     */
    void resume();

    /**
     * Call this in the view's (Activity or Fragment) onDestroy() method.
     */
    void destroy();

}
