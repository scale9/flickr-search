package io.ychescale9.flickrsearch.presentation.home;

import android.support.annotation.NonNull;

import java.util.List;

import io.ychescale9.flickrsearch.data.model.PhotoItem;
import io.ychescale9.flickrsearch.presentation.base.IPresenter;

/**
 * * Created by yang on 7/4/17.
 */
public interface HomeContract {

    interface View {

        void showSearchResults(@NonNull List<PhotoItem> photoItems);

        void showMoreSearchResults(@NonNull List<PhotoItem> newPhotoItems);

        void showCannotSearchPhotosError();

        void updateViewState(SearchViewState searchViewState, ContentViewState contentViewState);
    }

    interface Presenter extends IPresenter<View> {

        void enterSearchQuery(@NonNull String queryString);

        // presenter handles paging so no need to pass in paging info
        void submitSearchQuery(@NonNull String queryString);

        void loadNextPage();

        void cancelSearch();

        // whether user is entering search query or waiting for / viewing the search results
        boolean isInSearchMode();

        // whether all pages have been loaded for the current search query
        boolean areAllPagesLoaded();

        // whether we are currently loading data
        boolean isLoading();
    }

    /**
     * States for the search view
     */
    enum SearchViewState {
        INITIAL, ENTERING_QUERY, SUBMITTED_QUERY
    }

    /**
     * States for the content view
     */
    enum ContentViewState {
        INTRO, SEARCHING, RESULTS_DISPLAYED
    }
}
