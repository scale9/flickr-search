package io.ychescale9.flickrsearch.presentation.searchfilters;

import android.support.annotation.NonNull;

import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.ychescale9.flickrsearch.presentation.helper.ViewState;
import io.ychescale9.flickrsearch.util.DefaultSubscriber;
import io.ychescale9.flickrsearch.util.SchedulerProvider;
import timber.log.Timber;

/**
 * Created by yang on 18/4/17.
 */
public class SearchFiltersPresenter implements SearchFiltersContract.Presenter {

    private SearchFiltersContract.View view;

    @NonNull
    private final ViewState<SearchFilters> searchFiltersViewState;

    @NonNull
    private final SearchFilters currentSearchFilters;

    @NonNull
    private final SearchFilters newSearchFilters;

    private final Disposable searchFiltersDisposable;

    SearchFiltersPresenter(@NonNull SearchFiltersContract.View view,
                           @NonNull ViewState<SearchFilters> searchFiltersViewState,
                           @NonNull SchedulerProvider schedulerProvider) {
        this.view = view;
        this.searchFiltersViewState = searchFiltersViewState;

        // initialize current search filters (the search filters being applied)
        currentSearchFilters = new SearchFilters();

        // initialize new search filters (the search filters being modified)
        newSearchFilters = new SearchFilters();

        // subscribe to search filters to get the most recent SearchFilters (if available)
        searchFiltersDisposable = searchFiltersViewState.get()
                .observeOn(schedulerProvider.ui())
                .doAfterNext(new Consumer<SearchFilters>() {
                    @Override
                    public void accept(@io.reactivex.annotations.NonNull SearchFilters searchFilters) throws Exception {
                        Timber.d("Current SearchFilters received. Can unsubscribe now.");
                        searchFiltersDisposable.dispose();
                    }
                })
                .subscribeWith(new SearchFiltersChangeSubscriber());
    }

    @Override
    public void startMinUploadDateSelection() {
        view.showMinUploadDateSelectionScreen(
                newSearchFilters.getMinUploadDateMillis(),
                newSearchFilters.getMaxUploadDateMillis()
        );
    }

    @Override
    public void startMaxUploadDateSelection() {
        view.showMaxUploadDateSelectionScreen(
                newSearchFilters.getMaxUploadDateMillis(),
                newSearchFilters.getMinUploadDateMillis()
        );
    }

    @Override
    public void setMinUploadDate(long minUploadDate) {
        // update the min upload date for the new filters (not applied yet)
        newSearchFilters.setMinUploadDateMillis(minUploadDate);
        view.showSelectedMinUploadDate(minUploadDate);
    }

    @Override
    public void setMaxUploadDate(long maxUploadDate) {
        // update the max upload date for the new filters (not applied yet)
        newSearchFilters.setMaxUploadDateMillis(maxUploadDate);
        view.showSelectedMaxUploadDate(maxUploadDate);
    }

    @Override
    public void applyFilters() {
        // notify the change of search filters if the new filters differ from the current filters
        if (!currentSearchFilters.equals(newSearchFilters)) {
            currentSearchFilters.setMinUploadDateMillis(newSearchFilters.getMinUploadDateMillis());
            currentSearchFilters.setMaxUploadDateMillis(newSearchFilters.getMaxUploadDateMillis());
            searchFiltersViewState.update(newSearchFilters);
        }
    }

    @Override
    public void clearFilters() {
        // reset search filters to default values
        newSearchFilters.reset();
        view.showDefaultMinUploadDate();
        view.showDefaultMaxUploadDate();
    }

    @Override
    public void setView(SearchFiltersContract.View view) {
        this.view = view;
    }

    @Override
    public void syncState() {
        // restore state
        updateSelectedMinAndMaxUploadDates();
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void destroy() {
        searchFiltersDisposable.dispose();
        view = null;
    }

    private void updateSelectedMinAndMaxUploadDates() {
        if (newSearchFilters.getMinUploadDateMillis() > 0) {
            view.showSelectedMinUploadDate(newSearchFilters.getMinUploadDateMillis());
        } else {
            view.showDefaultMinUploadDate();
        }
        if (newSearchFilters.getMaxUploadDateMillis() > 0) {
            view.showSelectedMaxUploadDate(newSearchFilters.getMaxUploadDateMillis());
        } else {
            view.showDefaultMaxUploadDate();
        }
    }

    private final class SearchFiltersChangeSubscriber extends DefaultSubscriber<SearchFilters> {
        @Override
        public void onNext(SearchFilters searchFilters) {
            // Received currently applied SearchFilters
            // This won't be called again until during the lifetime of this presenter
            Timber.d("Current SearchFilters received.");
            // update the currentSearchFilters
            currentSearchFilters.setMinUploadDateMillis(searchFilters.getMinUploadDateMillis());
            currentSearchFilters.setMaxUploadDateMillis(searchFilters.getMaxUploadDateMillis());

            // show the currently selected min and max upload dates
            // if values are different from the newSearchFilters
            if (!currentSearchFilters.equals(newSearchFilters)) {
                // also update the newSearchFilters
                newSearchFilters.setMinUploadDateMillis(searchFilters.getMinUploadDateMillis());
                newSearchFilters.setMaxUploadDateMillis(searchFilters.getMaxUploadDateMillis());

                // update view with current search filters
                updateSelectedMinAndMaxUploadDates();
            }
        }
    }
}
