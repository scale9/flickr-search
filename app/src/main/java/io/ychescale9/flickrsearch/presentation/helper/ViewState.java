package io.ychescale9.flickrsearch.presentation.helper;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.FlowableEmitter;
import io.reactivex.FlowableOnSubscribe;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposables;

/**
 * Created by yang on 24/4/17.
 */
public class ViewState<T> {

    // a flowable which emits the last item emitted (if any) upon subscription
    private Flowable<T> flowable;

    interface ViewStateChangeListener<T> {
        void onUpdate(T t);
    }

    private ViewStateChangeListener<T> viewStateChangeListener;

    public void update(T t) {
        if (viewStateChangeListener != null) {
            viewStateChangeListener.onUpdate(t);
        }
    }

    public Flowable<T> get() {
        if (flowable == null) {
            flowable = Flowable.create(new FlowableOnSubscribe<T>() {
                @Override
                public void subscribe(@NonNull final FlowableEmitter<T> e) throws Exception {
                    viewStateChangeListener = new ViewStateChangeListener<T>() {
                        @Override
                        public void onUpdate(T t) {
                            e.onNext(t);
                        }
                    };
                    e.setDisposable(Disposables.fromRunnable(new Runnable() {
                        @Override
                        public void run() {
                            viewStateChangeListener = null;
                        }
                    }));
                }
            }, BackpressureStrategy.LATEST).replay(1).autoConnect();
        }
        return flowable;
    }

    public void reset() {
        flowable = null;
    }
}
