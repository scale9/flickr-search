package io.ychescale9.flickrsearch.presentation.helper;

import android.content.Context;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.widget.ImageView;

/**
 * Created by yang on 8/4/17.
 */
public interface ImageLoader {

    void loadPhotoThumbnail(
            @NonNull Context context,
            int itemIndex,
            @NonNull String url,
            @NonNull ImageView target,
            @DrawableRes int placeholderResId);
}
