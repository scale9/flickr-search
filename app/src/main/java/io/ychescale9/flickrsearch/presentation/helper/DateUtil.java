package io.ychescale9.flickrsearch.presentation.helper;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.Locale;

import io.ychescale9.flickrsearch.util.Clock;

/**
 * Created by yang on 21/4/17.
 */
public final class DateUtil {

    /**
     * Formats a timestamp into string e.g. July 1, 2017
     * @param timestamp
     * @return
     */
    public static String formatDateTime(long timestamp) {
        DateTimeFormatter formatter = DateTimeFormat.forPattern("MMMMM d, yyyy")
                .withLocale(Locale.ENGLISH);
        return new DateTime(timestamp).toString(formatter);
    }

    /**
     * Returns a DateTime representing the timestamp passed in if it's > 0,
     * otherwise return a DateTime representing now.
     * @param timestamp
     * @return
     */
    public static DateTime getDateTimeFromTimestampOrNow(long timestamp, Clock clock) {
        if (timestamp > 0) {
            return new DateTime(timestamp);
        } else {
            return new DateTime(clock.now());
        }
    }
}
