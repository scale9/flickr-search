package io.ychescale9.flickrsearch.presentation.home;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnEditorAction;
import butterknife.OnFocusChange;
import butterknife.OnTextChanged;
import io.ychescale9.flickrsearch.R;
import io.ychescale9.flickrsearch.data.model.PhotoItem;
import io.ychescale9.flickrsearch.presentation.base.BaseActivity;
import io.ychescale9.flickrsearch.presentation.base.OnInfiniteScrollListener;
import io.ychescale9.flickrsearch.presentation.helper.ImageLoader;
import io.ychescale9.flickrsearch.presentation.helper.PhotoUrlBuilder;
import io.ychescale9.flickrsearch.presentation.helper.UiUtils;
import io.ychescale9.flickrsearch.presentation.searchfilters.SearchFiltersActivity;
import timber.log.Timber;

import static io.ychescale9.flickrsearch.presentation.home.HomeContract.ContentViewState.RESULTS_DISPLAYED;

public class HomeActivity extends BaseActivity<HomeContract.Presenter> implements HomeContract.View {

    @Inject
    HomeContract.Presenter presenter;

    @Inject
    ImageLoader imageLoader;

    @Inject
    PhotoUrlBuilder photoUrlBuilder;

    @BindView(R.id.edit_text_search_photos)
    EditText searchPhotosEditText;

    @BindView(R.id.image_view_search)
    ImageView searchImageView;

    @BindView(R.id.button_cancel_search)
    ImageButton cancelButton;

    @BindView(R.id.button_clear)
    ImageButton clearButton;

    @BindView(R.id.button_search_filters)
    ImageButton searchFiltersButton;

    @BindView(R.id.text_view_intro_message)
    TextView introMessageTextView;

    @BindView(R.id.text_view_no_photos_found_message)
    TextView noResultsMessageTextView;

    @BindView(R.id.search_mode_overlay)
    View searchModeOverlay;

    @BindView(R.id.progress_bar_searching)
    ProgressBar searchingProgressBar;

    @BindView(R.id.recycler_view_photos)
    RecyclerView photosRecyclerView;

    // we are using the 150x150 thumbnails offered by the Flickr API
    private final static int MAX_THUMBNAIL_WIDTH_PIXELS = 150;

    private PhotosAdapter photosAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);

        // setup recycler view and its adapter
        final int spanCount = calculateGridLayoutSpanCount();
        photosAdapter = new PhotosAdapter(this, imageLoader, spanCount, photosActionListener);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, spanCount);
        photosRecyclerView.setAdapter(photosAdapter);
        gridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                return photosAdapter.getItemSpanSize(position);
            }
        });
        photosRecyclerView.setLayoutManager(gridLayoutManager);
        photosRecyclerView.setHasFixedSize(true);
        onInfiniteScrollListener = new PhotosOnInfiniteScrollListener(gridLayoutManager);
        photosRecyclerView.addOnScrollListener(onInfiniteScrollListener);

        // make sure views are in correct state after configuration changes
        presenter.syncState();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        photosAdapter = null;
        if (onInfiniteScrollListener != null) {
            photosRecyclerView.removeOnScrollListener(onInfiniteScrollListener);
        }
    }

    @Override
    public void onBackPressed() {
        if (presenter.isInSearchMode()) {
            // cancel search
            presenter.cancelSearch();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public HomeContract.Presenter getPresenter() {
        return presenter;
    }

    @OnFocusChange(R.id.edit_text_search_photos)
    public void searchPhotosEditTextOnClick(boolean hasFocus) {
        if (hasFocus) {
            // starting searching
            presenter.enterSearchQuery(searchPhotosEditText.getText().toString());
        }
    }

    @OnTextChanged(R.id.edit_text_search_photos)
    public void searchPhotosEditTextOnTextChanged(Editable editable) {
        if (isSearchPhotosEditTextEmpty()) {
            clearButton.setVisibility(View.GONE);
        } else {
            clearButton.setVisibility(View.VISIBLE);
        }
    }

    @OnEditorAction(R.id.edit_text_search_photos)
    public boolean searchPhotosEditTextOnEditorAction(int actionId) {
        boolean handled = false;
        if (actionId == EditorInfo.IME_ACTION_SEARCH) {
            // starting searching if edit text is not empty
            if (!isSearchPhotosEditTextEmpty()) {
                presenter.submitSearchQuery(searchPhotosEditText.getText().toString());
                handled = true;
            }
        }
        return handled;
    }

    @OnClick(R.id.button_cancel_search)
    public void cancelSearchButtonOnClick() {
        // cancel search
        presenter.cancelSearch();
    }

    @OnClick(R.id.button_clear)
    public void clearButtonOnClick() {
        // reset edit text
        searchPhotosEditText.getText().clear();
        // request focus for edit text
        searchPhotosEditText.requestFocus();
    }

    @OnClick(R.id.button_search_filters)
    public void searchFiltersButtonOnClick() {
        // start SearchFiltersActivity
        Intent intent = new Intent(this, SearchFiltersActivity.class);
        startActivity(intent);
    }

    /**
     * Action listener for the photos adapter
     */
    PhotosAdapter.ActionListener photosActionListener = new PhotosAdapter.ActionListener() {
        @Override
        public void onItemClick(@NonNull PhotoItem photoItem) {
            // launch the browser to display the full version of the photo
            String originalUrl = photoUrlBuilder.buildPageUrl(photoItem.getOwner(), photoItem.getId());
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(originalUrl));
            startActivity(intent);
        }
    };

    /**
     * Implementation of the OnInfiniteScrollListener for the Photos RecyclerView
     */
    PhotosOnInfiniteScrollListener onInfiniteScrollListener;

    private final class PhotosOnInfiniteScrollListener extends OnInfiniteScrollListener {

        PhotosOnInfiniteScrollListener(@NonNull LinearLayoutManager layoutManager) {
            super(layoutManager);
        }

        @Override
        public void onLoadMore() {
            photosAdapter.setIsLoadingMore(true);
            presenter.loadNextPage();
        }

        @Override
        public boolean areAllPagesLoaded() {
            return presenter.areAllPagesLoaded();
        }

        @Override
        public boolean isLoading() {
            return presenter.isLoading();
        }
    }

    @Override
    public void showSearchResults(@NonNull List<PhotoItem> photoItems) {
        if (photosAdapter != null) {
            // reset any existing items first
            photosAdapter.clear();

            if (photoItems.isEmpty()) {
                Timber.w("Search results is empty.");
            } else {
                // reset scroll position of search results
                photosRecyclerView.scrollToPosition(0);
                //  add the initial items
                photosAdapter.addItems(photoItems);
            }
        }
    }

    @Override
    public void showMoreSearchResults(@NonNull List<PhotoItem> newPhotoItems) {
        if (photosAdapter != null) {
            // add the new items
            photosAdapter.addItems(newPhotoItems);
            photosAdapter.setIsLoadingMore(false);
        }
    }

    @Override
    public void showCannotSearchPhotosError() {
        // show error message in a snackbar
        Snackbar.make(
                findViewById(R.id.root_view),
                getString(R.string.error_message_could_not_search_photos), Snackbar.LENGTH_LONG)
                .show();

        // disable infinite scrolling until user has scrolled up enough
        onInfiniteScrollListener.disableLoadingUntilScrollUp();
    }

    @Override
    public void updateViewState(HomeContract.SearchViewState searchViewState,
                                HomeContract.ContentViewState contentViewState) {
        switch (searchViewState) {
            case INITIAL:
                // show search icon
                searchImageView.setVisibility(View.VISIBLE);
                // hide keyboard
                UiUtils.hideKeyboard(this, searchPhotosEditText);
                // reset edit text
                searchPhotosEditText.getText().clear();
                // reset edit text focus
                searchPhotosEditText.clearFocus();
                // hide background overlay
                searchModeOverlay.setVisibility(View.GONE);
                // hide cancel button
                cancelButton.setVisibility(View.GONE);
                // remove results
                photosAdapter.clear();
                break;
            case ENTERING_QUERY:
                // show cancel button
                cancelButton.setVisibility(View.VISIBLE);
                // show background overlay
                searchModeOverlay.setVisibility(View.VISIBLE);
                // hide search icon
                searchImageView.setVisibility(View.GONE);
                break;
            case SUBMITTED_QUERY:
                // show cancel button
                cancelButton.setVisibility(View.VISIBLE);
                // hide keyboard
                UiUtils.hideKeyboard(this, searchPhotosEditText);
                // reset edit text focus
                searchPhotosEditText.clearFocus();
                // hide background overlay if content view state is RESULTS_DISPLAYED
                if (contentViewState == RESULTS_DISPLAYED) {
                    searchModeOverlay.setVisibility(View.GONE);
                } else {
                    searchModeOverlay.setVisibility(View.VISIBLE);
                }
                // hide search icon
                searchImageView.setVisibility(View.GONE);
                break;
        }

        switch (contentViewState) {
            case INTRO:
                // enable search edit text
                searchPhotosEditText.setEnabled(true);
                // show intro message
                introMessageTextView.setVisibility(View.VISIBLE);
                // hide results list
                photosRecyclerView.setVisibility(View.GONE);
                // hide no results message if results list is empty
                noResultsMessageTextView.setVisibility(View.GONE);
                // hide progress bar
                searchingProgressBar.setVisibility(View.GONE);
                break;
            case SEARCHING:
                searchPhotosEditText.setEnabled(false);
                // show progress bar
                searchingProgressBar.setVisibility(View.VISIBLE);
                // show results list and hide intro message if results list is NOT empty
                if (photosAdapter.getDataItemCount() > 0) {
                    photosRecyclerView.setVisibility(View.VISIBLE);
                    introMessageTextView.setVisibility(View.GONE);
                }
                break;
            case RESULTS_DISPLAYED:
                // enable search edit text
                searchPhotosEditText.setEnabled(true);
                // show results list
                photosRecyclerView.setVisibility(View.VISIBLE);
                // show no results message if results list is empty
                if (photosAdapter.getDataItemCount() == 0) {
                    noResultsMessageTextView.setVisibility(View.VISIBLE);
                } else {
                    noResultsMessageTextView.setVisibility(View.GONE);
                }
                // hide intro message
                introMessageTextView.setVisibility(View.GONE);
                // hide progress bar
                searchingProgressBar.setVisibility(View.GONE);
                break;
        }
    }

    private boolean isSearchPhotosEditTextEmpty() {
        return searchPhotosEditText.getText().toString().isEmpty();
    }

    /**
     * Calculates the number of columns the grid layout has based on the screen width and the maximum column width
     * @return
     */
    private int calculateGridLayoutSpanCount() {
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int layoutWidthPixels = metrics.widthPixels;
        float density = getResources().getDisplayMetrics().density;
        return (int) Math.ceil(layoutWidthPixels / MAX_THUMBNAIL_WIDTH_PIXELS / density);
    }
}
