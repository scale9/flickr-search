package io.ychescale9.flickrsearch.presentation.base;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import io.ychescale9.flickrsearch.presentation.home.HomeActivity;
import io.ychescale9.flickrsearch.presentation.home.HomeModule;
import io.ychescale9.flickrsearch.presentation.searchfilters.SearchFiltersActivity;
import io.ychescale9.flickrsearch.presentation.searchfilters.SearchFiltersModule;

/**
 * Created by yang on 29/4/17.
 */
@Module
public abstract class ActivityModule {

    @ViewScope
    @ContributesAndroidInjector(modules = HomeModule.class)
    abstract HomeActivity homeActivity();

    @ViewScope
    @ContributesAndroidInjector(modules = SearchFiltersModule.class)
    abstract SearchFiltersActivity searchFiltersActivity();
}
