package io.ychescale9.flickrsearch.presentation.home;

import android.content.Context;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.ychescale9.flickrsearch.R;
import io.ychescale9.flickrsearch.data.model.PhotoItem;
import io.ychescale9.flickrsearch.presentation.helper.ImageLoader;

/**
 * Created by yang on 8/4/17.
 */
public class PhotosAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    static final int TYPE_DATA_ITEM = 0;
    static final int TYPE_LOADING_MORE = -1;

    public interface ActionListener {
        void onItemClick(@NonNull PhotoItem photoItem);
    }

    private final Context context;
    private final ImageLoader imageLoader;
    private final ActionListener actionListener;

    private List<PhotoItem> photoItems;
    private final int spanCount;

    private boolean isLoadingMore = false;

    public PhotosAdapter(Context context,
                         ImageLoader imageLoader,
                         int spanCount,
                         ActionListener actionListener) {
        this.context = context;
        this.imageLoader = imageLoader;
        this.actionListener = actionListener;
        this.spanCount = spanCount;
        photoItems = new ArrayList<>();
    }

    public void addItems(@NonNull List<PhotoItem> newPhotoItems) {
        int positionStart = photoItems.size();
        photoItems.addAll(newPhotoItems);
        notifyItemRangeInserted(positionStart, newPhotoItems.size());
    }

    public void clear() {
        this.photoItems.clear();
        isLoadingMore = false;
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view;
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        switch (viewType) {
            case TYPE_DATA_ITEM:
                view = inflater.inflate(R.layout.item_photo, viewGroup, false);
                return new DataItemViewHolder(view);
            case TYPE_LOADING_MORE:
                view = inflater.inflate(R.layout.infinite_loading_view, viewGroup, false);
                return new LoadMoreViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (getItemViewType(position)) {
            case TYPE_DATA_ITEM:
                final DataItemViewHolder dataItemViewHolder = (DataItemViewHolder) holder;
                final PhotoItem photoItem = photoItems.get(position);

                String thumbnailUrl = photoItem.getThumbnailUrl();
                // use alternative colors for placeholder of the image views
                int placeholderResId = dataItemViewHolder.getAdapterPosition() % 2 == 0 ? R.color.photoPlaceholderDark : R.color.photoPlaceholderLight;
                imageLoader.loadPhotoThumbnail(
                        context,
                        dataItemViewHolder.getAdapterPosition(),
                        thumbnailUrl,
                        dataItemViewHolder.photoImageView,
                        placeholderResId);

                dataItemViewHolder.photoImageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (actionListener != null) {
                            actionListener.onItemClick(photoItem);
                        }
                    }
                });
                break;
            case TYPE_LOADING_MORE:
                final LoadMoreViewHolder loadMoreViewHolder = (LoadMoreViewHolder) holder;
                // show infinite progress bar only if there are already data items
                if (getDataItemCount() > 0 && isLoadingMore) {
                    loadMoreViewHolder.progressBar.setVisibility(View.VISIBLE);
                } else {
                    loadMoreViewHolder.progressBar.setVisibility(View.GONE);
                }
                break;
        }
    }

    @Override
    public int getItemViewType(int position) {
        return (position < getDataItemCount()) ? TYPE_DATA_ITEM : TYPE_LOADING_MORE;
    }

    @Override
    public int getItemCount() {
        // the size of data items plus the loading more progress bar
        return getDataItemCount() + 1;
    }

    /**
     * Return the actual number of data items (not including e.g. the LoadingMoreView at the bottom)
     * @return
     */
    public int getDataItemCount() {
        return photoItems.size();
    }

    /**
     * Handler used to delay notifying the change of an item.
     */
    private Handler itemChangedHandler = new Handler();

    /**
     * Set whether we are currently loading data
     * @param isLoadingMore
     */
    public void setIsLoadingMore(boolean isLoadingMore) {
        this.isLoadingMore = isLoadingMore;
        // update infinite loading view
        // postpone notifying this change as the recycler view might be computing layout
        // this fixed the IllegalStateException "Cannot call this method while RecyclerView is computing a layout or scrolling"
        // when scrolling down fast
        itemChangedHandler.post(new Runnable() {
            @Override
            public void run() {
                notifyItemChanged(getItemCount() - 1);
            }
        });
    }

        /**
         * Return the number of columns an item should occupy
         * @param position
         * @return
         */
    public int getItemSpanSize(int position) {
        switch (getItemViewType(position)) {
            case TYPE_LOADING_MORE:
                // loading more view should occupy the full width
                return spanCount;
            default:
                // regular data items should occupy 1 column
                return 1;
        }
    }

    static class DataItemViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.image_view_photo)
        ImageView photoImageView;

        DataItemViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }

    static class LoadMoreViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.progress_bar_infinite_loading)
        ProgressBar progressBar;

        LoadMoreViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }
}
