package io.ychescale9.flickrsearch.presentation.home;

import dagger.Module;
import dagger.Provides;
import io.ychescale9.flickrsearch.data.api.ApiConstants;
import io.ychescale9.flickrsearch.data.helper.PaginationManager;
import io.ychescale9.flickrsearch.data.model.PhotoItem;
import io.ychescale9.flickrsearch.data.repository.FlickrSearchRepository;
import io.ychescale9.flickrsearch.domain.SearchPhotos;
import io.ychescale9.flickrsearch.presentation.helper.PresenterManager;
import io.ychescale9.flickrsearch.presentation.helper.ViewState;
import io.ychescale9.flickrsearch.presentation.searchfilters.SearchFilters;
import io.ychescale9.flickrsearch.util.SchedulerProvider;

/**
 * Created by yang on 6/4/17.
 */
@Module
public class HomeModule {

    @Provides
    HomeContract.View provideHomeView(HomeActivity activity) {
        return activity;
    }

    @Provides
    HomeContract.Presenter provideHomePresenter(HomeContract.View homeView,
                                                SearchPhotos searchPhotos,
                                                ApiConstants apiConstants,
                                                PaginationManager<PhotoItem> paginationManager,
                                                ViewState<SearchFilters> searchFiltersViewState,
                                                SchedulerProvider schedulerProvider) {
        HomeContract.Presenter presenter;
        presenter = (HomeContract.Presenter) PresenterManager.getInstance().restorePresenter(homeView.getClass());
        if (presenter == null) {
            presenter = new HomePresenter(
                    homeView,
                    searchPhotos,
                    paginationManager,
                    apiConstants.getResponseItemsPerPage(),
                    searchFiltersViewState,
                    schedulerProvider
            );
        } else {
            presenter.setView(homeView);
        }
        return presenter;
    }

    @Provides
    SearchPhotos provideSearchPhotos(SchedulerProvider schedulerProvider,
                                     FlickrSearchRepository flickrSearchRepository) {
        return new SearchPhotos(
                schedulerProvider,
                flickrSearchRepository
        );
    }

    @Provides
    PaginationManager<PhotoItem> providePaginationManager() {
        return new PaginationManager<>();
    }
}
