/*
 * Copyright 2015 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.ychescale9.flickrsearch.presentation.base;

import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

/**
 * Created by yang on 9/4/17.
 * A scroll listener for RecyclerView to support infinite loading.
 */
public abstract class OnInfiniteScrollListener extends RecyclerView.OnScrollListener {

    // The minimum number of off-screen items remaining before we should loading more.
    private static final int VISIBLE_THRESHOLD = 5;

    private final LinearLayoutManager layoutManager;

    private boolean disabledLoading = false;

    public OnInfiniteScrollListener(@NonNull LinearLayoutManager layoutManager) {
        this.layoutManager = layoutManager;
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        if (dy == 0) {
            // re-enable loading once user once the recycler view has been repopulated
            disabledLoading = false;
        }

        final int visibleItemCount = recyclerView.getChildCount();
        final int totalItemCount = layoutManager.getItemCount();
        final int firstVisibleItem = layoutManager.findFirstVisibleItemPosition();


        if ((totalItemCount - visibleItemCount) <= (firstVisibleItem + VISIBLE_THRESHOLD)) {
            if (!disabledLoading && !isLoading() && !areAllPagesLoaded()) {
                onLoadMore();
            }
        }
        else if (dy < 0) {
            // re-enable loading once user has scrolled up enough
            disabledLoading = false;
        }
    }

    /**
     * Temporarily disable triggering onLoadMore()
     * until user has scrolled up and scrolled back down to where onLoadMore() would trigger normally
     * This can be used for preventing triggering of loading continuously after an error from loading more
     */
    public void disableLoadingUntilScrollUp() {
        disabledLoading = true;
    }

    public abstract void onLoadMore();

    public abstract boolean areAllPagesLoaded();

    public abstract boolean isLoading();
}
