package io.ychescale9.flickrsearch.data.deserializer;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

import io.ychescale9.flickrsearch.presentation.helper.PhotoUrlBuilder;
import io.ychescale9.flickrsearch.data.model.BaseResponse;
import io.ychescale9.flickrsearch.data.model.PhotoItem;
import io.ychescale9.flickrsearch.data.model.SearchPhotosResponse;

/**
 * Created by yang on 7/4/17.
 */
public class SearchPhotosResponseDeserializer implements JsonDeserializer<SearchPhotosResponse> {

    private final PhotoUrlBuilder photoUrlBuilder;

    public SearchPhotosResponseDeserializer(PhotoUrlBuilder photoUrlBuilder) {
        this.photoUrlBuilder = photoUrlBuilder;
    }

    @Override
    public SearchPhotosResponse deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        JsonObject jsonObject = json.getAsJsonObject();

        SearchPhotosResponse response;

        String status = jsonObject.get("stat").getAsString();
        // check if response is valid or has failed
        if (status.equals(BaseResponse.RESPONSE_STATUS_OK)) {
            JsonObject photosObject = jsonObject.get("photos").getAsJsonObject();
            int pageNumber = photosObject.get("page").getAsInt();
            int totalNumPages = photosObject.get("pages").getAsInt();
            int itemsPerPage = photosObject.get("perpage").getAsInt();
            int totalNumItems = photosObject.get("total").getAsInt();

            // deserialize list of PhotoItem using PhotoItemDeserializer
            JsonArray photosArray = photosObject.get("photo").getAsJsonArray();
            GsonBuilder gsonBuilder = new GsonBuilder();
            gsonBuilder.registerTypeAdapter(PhotoItem.class, new PhotoItemDeserializer(photoUrlBuilder));
            Gson gson = gsonBuilder.create();

            Type listType = new TypeToken<List<PhotoItem>>(){}.getType();
            List<PhotoItem> photoItems = gson.fromJson(photosArray, listType);
            response = new SearchPhotosResponse(pageNumber, totalNumPages, itemsPerPage, totalNumItems, photoItems);
        } else {
            int errorCode = jsonObject.get("code").getAsInt();
            String errorMessage = jsonObject.get("message").getAsString();
            response = new SearchPhotosResponse(errorCode, errorMessage);
        }

        return response;
    }
}
