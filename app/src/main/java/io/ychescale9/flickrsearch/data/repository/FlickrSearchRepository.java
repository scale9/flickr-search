package io.ychescale9.flickrsearch.data.repository;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import io.reactivex.Observable;
import io.ychescale9.flickrsearch.data.model.SearchPhotosResponse;

/**
 * Created by yang on 6/4/17.
 */
public interface FlickrSearchRepository {

    Observable<SearchPhotosResponse> searchPhotos(int pageNumber,
                                                  int itemsPerPage,
                                                  @NonNull String queryString,
                                                  @Nullable Long minUploadDate,
                                                  @Nullable Long maxUploadDate);
}
