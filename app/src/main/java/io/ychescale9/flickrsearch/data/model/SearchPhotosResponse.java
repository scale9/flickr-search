package io.ychescale9.flickrsearch.data.model;

import java.util.List;

/**
 * Created by yang on 6/4/17.
 * VO class representing a response of searching photos including some meta data such as paging information.
 * Values are immutable.
 */
public class SearchPhotosResponse extends BaseResponse {

    private final int pageNumber;
    private final int totalNumPages;
    private final int itemsPerPage;
    private final int totalNumItems;
    private final List<PhotoItem> photoItems;

    /**
     * Constructor for a valid response
     * @param pageNumber
     * @param totalNumPages
     * @param itemsPerPage
     * @param totalNumItems
     * @param photoItems
     */
    public SearchPhotosResponse(int pageNumber, int totalNumPages, int itemsPerPage, int totalNumItems,
                                List<PhotoItem> photoItems) {
        super(BaseResponse.RESPONSE_STATUS_OK, 0, null);
        this.pageNumber = pageNumber;
        this.totalNumPages = totalNumPages;
        this.itemsPerPage = itemsPerPage;
        this.totalNumItems = totalNumItems;
        this.photoItems = photoItems;
    }

    /**
     * Constructor for a failed response (with custom error info from the API)
     * @param errorCode
     * @param errorMessage
     */
    public SearchPhotosResponse(int errorCode, String errorMessage) {
        super(BaseResponse.RESPONSE_STATUS_FAIL, errorCode, errorMessage);
        this.pageNumber = 0;
        this.totalNumPages = 0;
        this.itemsPerPage = 0;
        this.totalNumItems = 0;
        this.photoItems = null;
    }

    public int getPageNumber() {
        return pageNumber;
    }

    public int getTotalNumPages() {
        return totalNumPages;
    }

    public int getItemsPerPage() {
        return itemsPerPage;
    }

    public int getTotalNumItems() {
        return totalNumItems;
    }

    public List<PhotoItem> getPhotoItems() {
        return photoItems;
    }
}
