package io.ychescale9.flickrsearch.data.api;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import io.reactivex.Observable;
import io.ychescale9.flickrsearch.data.model.SearchPhotosResponse;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by yang on 6/4/17.
 */
public interface FlickrSearchService {

    @GET("./")
    Observable<SearchPhotosResponse> searchPhotos(
            @Query("method") @NonNull String method,
            @Query("page") int pageNumber,
            @Query("per_page") int itemsPerPage,
            @Query("text") @NonNull String text,
            @Query("min_upload_date") @Nullable Long minUploadDate, // optional (can be null)
            @Query("max_upload_date") @Nullable Long maxUploadDate  // optional (can be null)
    );
}
