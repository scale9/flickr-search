package io.ychescale9.flickrsearch.data.repository;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.ychescale9.flickrsearch.data.api.ApiConstants;
import io.ychescale9.flickrsearch.data.api.FlickrSearchService;

/**
 * Created by yang on 6/4/17.
 */
@Module
public class RepositoryModule {

    @Provides
    @Singleton
    FlickrSearchRepository provideFlickrSearchRepository(FlickrSearchService flickrSearchService,
                                                         ApiConstants apiConstants) {
        return new FlickrSearchRepositoryImpl(flickrSearchService, apiConstants);
    }
}
