package io.ychescale9.flickrsearch.data.repository;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.ychescale9.flickrsearch.data.api.ApiConstants;
import io.ychescale9.flickrsearch.data.api.FlickrSearchService;
import io.ychescale9.flickrsearch.data.model.SearchPhotosResponse;
import timber.log.Timber;

/**
 * Created by yang on 6/4/17.
 */
public class FlickrSearchRepositoryImpl implements FlickrSearchRepository {

    private final FlickrSearchService flickrSearchService;
    private final ApiConstants apiConstants;

    public FlickrSearchRepositoryImpl(@NonNull FlickrSearchService flickrSearchService,
                                      @NonNull ApiConstants apiConstants) {
        this.flickrSearchService = flickrSearchService;
        this.apiConstants = apiConstants;
    }

    @Override
    public Observable<SearchPhotosResponse> searchPhotos(final int pageNumber,
                                                         final int itemsPerPage,
                                                         @NonNull final String queryString,
                                                         @Nullable final Long minUploadDate,
                                                         @Nullable final Long maxUploadDate) {
        return flickrSearchService.searchPhotos(
                apiConstants.getApiMethodSearchPhotos(),
                pageNumber, itemsPerPage, queryString,
                minUploadDate, maxUploadDate)
                .doOnSubscribe(new Consumer<Disposable>() {
                    @Override
                    public void accept(Disposable disposable) throws Exception {
                        Timber.d("Searching for photos from server: page=%s, perpage=%s, text=%s", pageNumber, itemsPerPage, queryString);
                    }
                })
                .map(new Function<SearchPhotosResponse, SearchPhotosResponse>() {
                    @Override
                    public SearchPhotosResponse apply(SearchPhotosResponse searchPhotosResponse) throws Exception {
                        // check if response is valid or has failed
                        if (searchPhotosResponse.isValid()) {
                            return searchPhotosResponse;
                        } else {
                            Timber.e("Response has error: %s:%s", searchPhotosResponse.getErrorCode(), searchPhotosResponse.getErrorMessage());
                            throw new Exception(searchPhotosResponse.getErrorMessage());
                        }
                    }
                })
                .doOnNext(new Consumer<SearchPhotosResponse>() {
                    @Override
                    public void accept(SearchPhotosResponse response) throws Exception {
                        Timber.d("Valid search photos response loaded from server.");
                    }
                })
                .doOnError(new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        Timber.e(throwable, "Error getting search results from server:");
                    }
                });
    }
}
