package io.ychescale9.flickrsearch.data.deserializer;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

import io.ychescale9.flickrsearch.presentation.helper.PhotoUrlBuilder;
import io.ychescale9.flickrsearch.data.model.PhotoItem;

/**
 * Created by yang on 7/4/17.
 */
public class PhotoItemDeserializer implements JsonDeserializer<PhotoItem> {

    private final PhotoUrlBuilder photoUrlBuilder;

    public PhotoItemDeserializer(PhotoUrlBuilder photoUrlBuilder) {
        this.photoUrlBuilder = photoUrlBuilder;
    }

    @Override
    public PhotoItem deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        JsonObject jsonObject = json.getAsJsonObject();

        String id = jsonObject.get("id").getAsString();
        String owner = jsonObject.get("owner").getAsString();
        String secret = jsonObject.get("secret").getAsString();
        String serverId = jsonObject.get("server").getAsString();
        int farmId = jsonObject.get("farm").getAsInt();
        String title = jsonObject.get("title").getAsString();
        int isPublic = jsonObject.get("ispublic").getAsInt();
        int isFriend = jsonObject.get("isfriend").getAsInt();
        int isFamily = jsonObject.get("isfamily").getAsInt();

        // build thumbnail url from farm, server, id and secret
        String thumbnailUrl = photoUrlBuilder.buildThumbnailUrl(farmId, serverId, id, secret);

        return new PhotoItem(id, owner, secret, serverId, farmId, title, isPublic, isFriend, isFamily, thumbnailUrl);
    }
}
