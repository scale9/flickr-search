package io.ychescale9.flickrsearch.data.model;

/**
 * Created by yang on 6/4/17.
 * VO class representing a photo.
 * Values are immutable.
 */
public class PhotoItem {

    private final String id;

    private final String owner;

    private final String secret;

    private final String serverId;

    private final int farmId;

    private final String title;

    private final int isPublic;

    private final int isFriend;

    private final int isFamily;

    // derived from farm, server, id and secret
    private transient final String thumbnailUrl;

    public PhotoItem(String id, String owner, String secret, String serverId, int farmId, String title,
                     int isPublic, int isFriend, int isFamily, String thumbnailUrl) {
        this.id = id;
        this.owner = owner;
        this.secret = secret;
        this.serverId = serverId;
        this.farmId = farmId;
        this.title = title;
        this.isPublic = isPublic;
        this.isFriend = isFriend;
        this.isFamily = isFamily;
        this.thumbnailUrl = thumbnailUrl;
    }

    public String getId() {
        return id;
    }

    public String getOwner() {
        return owner;
    }

    public String getSecret() {
        return secret;
    }

    public String getServerId() {
        return serverId;
    }

    public int getFarmId() {
        return farmId;
    }

    public String getTitle() {
        return title;
    }

    public int getIsPublic() {
        return isPublic;
    }

    public int getIsFriend() {
        return isFriend;
    }

    public int getIsFamily() {
        return isFamily;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }
}
