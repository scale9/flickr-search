package io.ychescale9.flickrsearch.data.api;

import io.ychescale9.flickrsearch.BuildConfig;

/**
 * Created by yang on 6/4/17.
 */
public abstract class BaseApiConstants {

    public abstract String getApiBaseUrl();

    public abstract String getApiMethodSearchPhotos();

    /**
     * Number of items per page to be included in a response
     * Note: this is fixed for now and may be dynamically calculated in the future
     * @return
     */
    public int getResponseItemsPerPage() {
        return BuildConfig.RESPONSE_ITEMS_PER_PAGE;
    }

    public String getPhotoThumbnailUrlFormat() {
        return BuildConfig.PHOTO_THUMBNAIL_URL_FORMAT;
    }

    public String getPhotoPageUrlFormat() {
        return BuildConfig.PHOTO_PAGE_URL_FORMAT;
    }

    public abstract int getConnectTimeOutMilliseconds();

    public abstract int getReadTimeOutMilliseconds();

    public abstract int getWriteTimeOutMilliseconds();
}
