package io.ychescale9.flickrsearch.data.model;

/**
 * Created by yang on 6/4/17.
 * Base class for a response with status of the response and error info if failed.
 * Values are immutable.
 */
public abstract class BaseResponse {

    public final static String RESPONSE_STATUS_OK = "ok";
    public final static String RESPONSE_STATUS_FAIL = "fail";

    private final String status;
    private final int errorCode;
    private final String errorMessage;

    public BaseResponse(String status, int errorCode, String errorMessage) {
        this.status = status;
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }

    public String getStatus() {
        return status;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    /**
     * Whether the response has valid results or has failed
     * @return
     */
    public boolean isValid() {
        return status != null && status.equals(RESPONSE_STATUS_OK);
    }
}
