package io.ychescale9.flickrsearch.data.helper;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by yang on 9/4/17.
 */
public class PaginationManager<T> {

    private PaginationListener<T> paginationListener;

    @NonNull
    private String queryString;

    private int totalNumPages;

    private int itemsPerPage;

    // NOTE: page starts at 1 not 0 i.e. page = 1 indicates first page
    private int latestPage;

    @NonNull
    private List<T> dataItems;

    public PaginationManager() {
        queryString = "";
        itemsPerPage = 0;
        totalNumPages = 0;
        latestPage = 0;
        dataItems = new ArrayList<>();
    }

    public void initialize(@NonNull String queryString) {
        clear();
        this.queryString = queryString;
    }

    public void appendPage(int page,
                           int itemsPerPage,
                           int totalNumPages,
                           @NonNull List<T> newDataItems) {
        boolean isValid = false;
        if (latestPage == 0) {
            // this is the first page being appended
            isValid = true;
            this.itemsPerPage = itemsPerPage;
        } else {
            // check that itemsPerPage is the same, the current page has not exceeded totalNumPages
            // and the loaded page is the next page
            if (newDataItems.size() > 0
                    && page == this.latestPage + 1
                    && this.itemsPerPage == itemsPerPage
                    && latestPage < totalNumPages) {
                isValid = true;
            }
        }

        // always update totalNumPages as this might change as pagination happens
        this.totalNumPages = totalNumPages;

        // append the new page of items to the current data items
        if (isValid) {
            latestPage = page;
            dataItems.addAll(newDataItems);
            if (paginationListener != null) {
                paginationListener.onNewPageAdded(newDataItems);
            }
        } else {
            if (paginationListener != null) {
                paginationListener.onInvalidNewPage();
            }
        }
    }

    public void clear() {
        queryString = "";
        itemsPerPage = 0;
        totalNumPages = 0;
        latestPage = 0;
        dataItems = new ArrayList<>();
    }

    public void setPaginationListener(@NonNull PaginationListener<T> paginationListener) {
        this.paginationListener = paginationListener;
    }

    @NonNull
    public String getQueryString() {
        return queryString;
    }

    public int getTotalNumPages() {
        return totalNumPages;
    }

    public int getItemsPerPage() {
        return itemsPerPage;
    }

    public int getLatestPage() {
        return latestPage;
    }

    @NonNull
    public List<T> getDataItems() {
        List<T> currentDataItems = new ArrayList<>();
        currentDataItems.addAll(dataItems);
        return currentDataItems;
    }

    public interface PaginationListener<T> {
        void onNewPageAdded(@NonNull List<T> addedItems);
        void onInvalidNewPage();
    }
}
