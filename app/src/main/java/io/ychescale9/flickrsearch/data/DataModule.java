package io.ychescale9.flickrsearch.data;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.ychescale9.flickrsearch.data.deserializer.SearchPhotosResponseDeserializer;
import io.ychescale9.flickrsearch.presentation.helper.PhotoUrlBuilder;

/**
 * Created by yang on 6/4/17.
 */
@Module
public class DataModule {

    @Provides
    @Singleton
    SearchPhotosResponseDeserializer provideSearchPhotosResponseDeserializer(PhotoUrlBuilder photoUrlBuilder) {
        return new SearchPhotosResponseDeserializer(photoUrlBuilder);
    }
}
