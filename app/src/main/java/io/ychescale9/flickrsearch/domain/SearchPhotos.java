package io.ychescale9.flickrsearch.domain;

import io.reactivex.Observable;
import io.ychescale9.flickrsearch.data.model.SearchPhotosResponse;
import io.ychescale9.flickrsearch.data.repository.FlickrSearchRepository;
import io.ychescale9.flickrsearch.util.SchedulerProvider;

/**
 * Created by yang on 7/4/17.
 */
public class SearchPhotos extends UseCase<SearchPhotos.RequestValues, SearchPhotosResponse> {

    private final FlickrSearchRepository flickrSearchRepository;

    public SearchPhotos(SchedulerProvider schedulerProvider, FlickrSearchRepository repository) {
        super(schedulerProvider);
        flickrSearchRepository = repository;
    }

    @Override
    protected Observable<SearchPhotosResponse> createUseCase() {
        // use null for minUploadDateMillis if <= 0, convert minUploadDate to second
        Long minUploadDate = getRequestValues().getMinUploadDateMillis() <= 0 ? null : getRequestValues().getMinUploadDateMillis() / 1000;
        // use null for maxUploadDateMillis if <= 0, convert maxUploadDate to second
        Long maxUploadDate = getRequestValues().getMaxUploadDateMillis() <= 0 ? null : getRequestValues().getMaxUploadDateMillis() / 1000;

        // Note that url encoding is performed at a lower level
        return flickrSearchRepository.searchPhotos(
                getRequestValues().getPageNumber(),
                getRequestValues().getItemsPerPage(),
                getRequestValues().getQueryString(),
                minUploadDate,
                maxUploadDate
        );
    }

    public static final class RequestValues implements UseCase.RequestValues {

        private final int pageNumber;
        private final int itemsPerPage;
        private final String queryString;
        private final long minUploadDateMillis;
        private final long maxUploadDateMillis;

        public RequestValues(int pageNumber,
                             int itemsPerPage, String queryString,
                             long minUploadDateMillis, long maxUploadDateMillis) {
            this.pageNumber = pageNumber;
            this.itemsPerPage = itemsPerPage;
            this.queryString = queryString;
            this.minUploadDateMillis = minUploadDateMillis;
            this.maxUploadDateMillis = maxUploadDateMillis;
        }

        String getQueryString() {
            return queryString;
        }

        int getPageNumber() {
            return pageNumber;
        }

        int getItemsPerPage() {
            return itemsPerPage;
        }

        long getMinUploadDateMillis() {
            return minUploadDateMillis;
        }

        long getMaxUploadDateMillis() {
            return maxUploadDateMillis;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            RequestValues that = (RequestValues) o;

            if (pageNumber != that.pageNumber) return false;
            if (itemsPerPage != that.itemsPerPage) return false;
            if (minUploadDateMillis != that.minUploadDateMillis) return false;
            if (maxUploadDateMillis != that.maxUploadDateMillis) return false;
            return queryString.equals(that.queryString);

        }

        @Override
        public int hashCode() {
            int result = pageNumber;
            result = 31 * result + itemsPerPage;
            result = 31 * result + queryString.hashCode();
            result = 31 * result + (int) (minUploadDateMillis ^ (minUploadDateMillis >>> 32));
            result = 31 * result + (int) (maxUploadDateMillis ^ (maxUploadDateMillis >>> 32));
            return result;
        }
    }
}
