package io.ychescale9.flickrsearch.domain;

import android.support.annotation.NonNull;

import io.reactivex.Observable;
import io.ychescale9.flickrsearch.util.SchedulerProvider;

/**
 * Created by yang on 7/4/17.
 */
public abstract class UseCase<Q extends UseCase.RequestValues, T> {

    private final SchedulerProvider schedulerProvider;

    private Q requestValues;

    UseCase(SchedulerProvider schedulerProvider) {
        this.schedulerProvider = schedulerProvider;
    }

    protected abstract Observable<T> createUseCase();

    /**
     * Return the created use case observable
     * @param requestValues
     * @return
     */
    public Observable<T> getStream(@NonNull Q requestValues) {
        // update request values for the next execution
        this.requestValues = requestValues;
        return createUseCase().compose(schedulerProvider.<T>applySchedulers());
    }

    public Q getRequestValues() {
        return requestValues;
    }

    /**
     * Request values passed in for a use case.
     */
    public interface RequestValues {
    }
}