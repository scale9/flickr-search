package io.ychescale9.flickrsearch;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.ychescale9.flickrsearch.data.api.ApiConstants;

/**
 * Created by yang on 6/4/17.
 */
@Module
public class ConstantsModule {

    @Provides
    @Singleton
    ApiConstants provideApiConstants() {
        return new ApiConstants();
    }
}