-keep class io.ychescale9.flickrsearch.** { *; }
-keepattributes *Annotation*
-keepattributes EnclosingMethod

# For enumeration classes, see http://proguard.sourceforge.net/manual/examples.html#enumerations
-keepclassmembers enum * {
    public static **[] values();
    public static ** valueOf(java.lang.String);
}

# Dagger Android
-dontwarn dagger.android.**

# Gson
-keepattributes Signature
-keep class sun.misc.Unsafe { *; }
-keep class io.ychescale9.flickrsearch.data.model.** { *; }

# OkHttp
-dontwarn okhttp3.**
-dontwarn okio.**

# Picasso
-dontwarn com.squareup.okhttp.**

# Retrofit2
-dontwarn retrofit2.**
-keep class retrofit2.** { *; }
-keepattributes Signature
-keepattributes Exceptions

-keepclasseswithmembers class * {
    @retrofit2.http.* <methods>;
}

# Espresso
-keep class android.support.test.espresso.IdlingResource { *; }

# LeakCanary
-dontwarn com.squareup.haha.guava.**
-dontwarn com.squareup.haha.perflib.**
-dontwarn com.squareup.haha.trove.**
-dontwarn com.squareup.leakcanary.**
-keep class com.squareup.haha.** { *; }
-keep class com.squareup.leakcanary.** { *; }
