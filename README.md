# Flickr Search #

## Introduction

This app uses the [Flickr API](https://www.flickr.com/services/api/) to provide basic photo searching capabilities.

## Product Flavors

This Android project has 2 product flavors:

* **mock** - app is driven by hardcoded dummy data and has no interactions with the network
* **prod** - app is driven by real data from the [Flickr API](https://www.flickr.com/services/api/)

## Running Tests

The project includes both unit tests and UI tests: 

* **Unit tests** - junit tests can be run with ./gradlew test
* **Android tests** - on-device / UI tests can be run with ./gradlew connectedAndroidTest

## Running the App

The app can be run or debugged from Android Studio with either the **mockDebug** or **prodDebug** build variant.
